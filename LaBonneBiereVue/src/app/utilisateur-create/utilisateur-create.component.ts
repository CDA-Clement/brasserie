import { Component, OnInit } from '@angular/core';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleDto } from '../model/role-dto';
import { Observable } from 'rxjs';
import { RoleService } from '../service/role.service';
import { AlertService } from '../service/alert.service';
import { ReponseDto } from '../model/reponse-dto';

@Component({
  selector: 'app-utilisateur-create',
  templateUrl: './utilisateur-create.component.html',
  styleUrls: ['./utilisateur-create.component.css']
})
export class UtilisateurCreateComponent implements OnInit {
  utilisateurs= new UtilisateurDto();
  roles: Observable<any>;
  utilisateur=new UtilisateurDto();
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  rep: ReponseDto;
  

  constructor(private roleService: RoleService, private route: ActivatedRoute, private utilisateurService: UtilisateurService, private router: Router, private alertService: AlertService) {
    this.utilisateur.role = new RoleDto();
  }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }
    this.roleService.getAll().subscribe(
      types => {
        this.roles = types;
      }
    );

  }

  onSubmitUtil() {
    if (this.utilisateur.nom == undefined ||
      this.utilisateur.prenom == undefined ||
      this.utilisateur.username == undefined ||
      this.utilisateur.dateAge == undefined ||
      this.utilisateur.role == undefined ||
      this.utilisateur.password == undefined ||
      this.utilisateur.adresseMail == undefined) {
      this.alertService.addPrimary('un ou plusieurs champs manquants')
    } else {
      this.utilisateurService.createUtilisateur(this.utilisateur)
        .subscribe(
          donnees => {
            this.rep = donnees;
            if(this.rep.msg==='ok'){
              this.utilisateurService.subjectMiseAJour.next(0)
              this.alertService.addSuccess("l'utilisateur " + this.utilisateur.username + " à bien été créé");
              (<HTMLFormElement>document.getElementById("form")).reset();

            }else if(this.rep.msg==='ADRESSE_MAIL'){
              this.alertService.addPrimary("adresse mail déjà existante")
            }else{
              this.alertService.addPrimary("username déjà existant")
            }
            
          },
          err => {
          }
        )
    }
  }
}
