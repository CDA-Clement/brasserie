import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VerificationService } from 'src/app/service/verification.service'
import { AlertService } from '../service/alert.service';
import { ReponseDto } from '../model/reponse-dto';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {
  id: string;
  token: string;
  verifi: ReponseDto;
  
  

  constructor(private alertService: AlertService,private router: Router,private route: ActivatedRoute,private verificationService:VerificationService) { }

  ngOnInit() {
    window.scrollTo(0,0)
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }
    this.route.paramMap.subscribe(res => {
      this.id = res.get('id');
      this.token=res.get('token')

      this.verif()
      });

  }

  verif(){
    this.verificationService.verification(this.id,this.token).subscribe(
      data=>{
        this.verifi=data
        if(this.verifi.msg=='validation'){
          this.router.navigateByUrl('/login')
          this.alertService.addPrimary("BIENVENUE, votre compte a été validé")

        }else if(this.verifi.msg=="expired"){
          this.router.navigateByUrl('/signup')
          this.alertService.addPrimary("Le lien de validation a expiré")
        }else if(this.verifi.msg=='validated'){
          this.router.navigateByUrl('/login')
          this.alertService.addPrimary("votre compte a deja été validé")
        }
        
      }
    )
  }
}


