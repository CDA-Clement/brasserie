import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { RoleDto } from '../model/role-dto';
import { Observable } from 'rxjs';
import { RoleService } from '../service/role.service';

@Component({
  selector: 'app-utilisateur-update',
  templateUrl: './utilisateur-update.component.html',
  styleUrls: ['./utilisateur-update.component.css']
})
export class UtilisateurUpdateComponent implements OnInit {
  idUserParam: any;
  idU: string;
  utilisateur: UtilisateurDto;
  roles: Observable<any>;

  constructor(private route: ActivatedRoute,private roleService: RoleService, private router: Router,
    private utilisateurService: UtilisateurService) { }

  ngOnInit() {
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }

    this.roleService.getAll().subscribe(
      types => {
        this.roles = types;
      }
    );
    this.utilisateur = new UtilisateurDto();
    this.utilisateur.role = new RoleDto();

    this.route.queryParams.subscribe(
      params => {
      this.idUserParam = params;
    });
    var strId=Object.values(this.idUserParam).toString()
    var id =parseInt(strId, 10)

    this.utilisateurService.getUtilisateur(id).subscribe(
      donnees => {
      this.utilisateurService.getUtilisateur(id)
        .subscribe(
          data => {
          this.utilisateur = data;
        },);
    });
  }


  onSubmit() {
    this.utilisateurService.updateUtilisateur(this.utilisateur)
    .subscribe(data => {
    },
    );
      this.router.navigate(['gestion/utilisateurs']);
  }

  
}



