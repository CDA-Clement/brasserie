import { Component, OnInit } from '@angular/core';

import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart, faPlus, faMinus, faInfoCircle, faTrash,faToggleOn,faToggleOff,  } from '@fortawesome/free-solid-svg-icons';
import { TypeDeBiereService } from 'src/app/service/type-de-biere.service';
import { TypeDeBiereDto } from 'src/app/model/type-de-biere-dto';
import { CommandeService } from 'src/app/service/commande.service';
import { CommandeDTO } from 'src/app/model/commande-dto';
import { AlertService } from 'src/app/service/alert.service';
import { PanierService } from 'src/app/service/panier.service';

import { environment } from 'src/environments/environment';
import { BiereDto } from '../model/biere-dto';
import { BiereService } from '../service/biere.service';
import { FactureService } from '../service/facture.service ';
import { Router } from '@angular/router';



@Component({
 
  selector: 'app-liste-de-biere',
  templateUrl: './liste-de-biere.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./liste-de-biere.component.css']
})
export class ListeDeBiereComponent implements OnInit {
  faEye=faToggleOn
  faEye1=faToggleOff;
  faInfoCircle = faInfoCircle;
  faPlus = faPlus;
  faMinus = faMinus;
  faTrash=faTrash;
  faShoppingCart = faShoppingCart;
  faSearch = faSearch;
  bieres= new Array<BiereDto>()
  bieres2=new Array<BiereDto>()
  bieres3=new Array<BiereDto>()
  recherche: string = "";
  touche = "";
  bool = false;
  boolTrack=false;
  biere: BiereDto;
  favoriteBeer=Array<String>();
  typeDebieres: TypeDeBiereDto[];
  filtre: Array<number>=new Array()
  filtre1: Array<number>=new Array()
  commande:CommandeDTO[];
  total:number=0;
  origine: string='boutique'
  user = localStorage.getItem("current_user")
  objJson :any;
  page:number=0
  url:String=environment.backSchema+"://"+environment.backServer
  prixMin:number
  prixMax:number
  qteMax: number
  qteMin: number
  alcMin: number
  alcMax: number
  sorting="rien"
  biereId: string;
  favBiere= new BiereDto();
  biereMomentId: number;
  biereMoment= new BiereDto();
  commande2: CommandeDTO;
  position: number;
  
  
  constructor(private router:Router, private factureService: FactureService,private panierService: PanierService,private biereService: BiereService, private typeDeBiereService: TypeDeBiereService, private commandeService: CommandeService, private alertService: AlertService) { }

  ngOnInit() {
  //  console.log(document.location.hash)
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }else{
      this.alertService.addPrimary('Veuillez saisir votre age')
      document.getElementById('id104').style.display = 'block';
    }  
    
    var local=localStorage.getItem("page");
    if(local!=null){
      this.page=parseInt(local)
    }
    this.filtre1.push(-1)
    window.scrollTo(0,0)
    document.getElementById('footerID').style.display="block";

    this.factureService.BiereMoment().subscribe(
      data=>{
        this.biereMomentId=data

        if(this.biereMomentId!=-1){
          this.biereService.getOne(this.biereMomentId).subscribe(
            data=>{
              this.biereMoment=data
              this.biereMoment.commande=0;
            }
          )
        }
      }
    )

   
    if(this.user!=null){
      this.objJson = JSON.parse(this.user);

      this.factureService.favoriteUserBeer(this.objJson.id).subscribe(
        data=>{
          this.favoriteBeer=data
          this.biereId=this.favoriteBeer[1].toString()
          if(this.biereId!='0'){
            this.biereService.getOne(parseInt(this.biereId)).subscribe(
              data=>{
                this.favBiere=data
                this.favBiere.commande=0;
              }
            )

          }
        }
      )
     

      this.biereService.getAll((this.page+1).toString()).subscribe(
        donnees => {
         
          this.bieres2 = donnees;
          
        }
      ); 
      
     
          this.commandeService.getCommande(this.objJson.nom).subscribe(
            data1=>{
              this.commande=data1;
                  this.biereService.getAll(this.page.toString()).subscribe(
                    donnees => {
                      this.bieres = donnees;
                      this.bieres.forEach(element => {
                        element.commande=0;
                      }
                      );                     
                      if(this.commande.length>0){
                        this.commande.forEach(element1 => {
                          this.biere= this.bieres.find(biere=>biere.nom===element1.nom)
                          if(this.biere!=undefined){
                            this.biere.commande=element1.commande
                            this.bieres.indexOf(this.biere)
                            this.bieres.splice(this.bieres.indexOf(this.biere),1,this.biere)
        
                          }
             
                  if(this.biereMoment.nom==element1.nom){
                    this.biereMoment.commande= element1.commande
                  }
                  if(this.favBiere.nom==element1.nom){
                    this.favBiere.commande= element1.commande
                  }
                    });

                  }
                  });
                 
                  
            }
          );

         
        


       
    }else{  
        this.biereService.getAll(this.page.toString()).subscribe(
            donnees => {
             
              this.bieres = donnees;
              this.bieres.forEach(element => {
                element.commande=0;
              }
              ); 
              
            }
          ); 
          this.biereService.getAll((this.page+1).toString()).subscribe(
            donnees => {
             
              this.bieres2 = donnees;
              
            }
          ); 
         
    }


    localStorage.setItem("origine", this.origine);
    
    
    
                  
              this.typeDeBiereService.getAll().subscribe(
                donnees => {
                  
                  this.typeDebieres = donnees;
                  this.remplacement();
                }
                );
                      
              }
              
remplacement() {
                var index=0
                this.typeDebieres.forEach(element => {
                  if (element.nom === "Tampon") {
                    this.typeDebieres.splice(index,1)
                  }
                  index++
                  
                });
                
            
}

track(event){
  this.position=parseInt(event.path[1].window.scrollY)
  if(this.objJson.id!=null){
    if(this.position>=299&&!this.boolTrack){
      this.boolTrack=true;
      document.getElementById("modal").style.display='block'
      document.getElementById("modal").animate([
        { transform: 'translatex(-300px) translatey(-200px)' }, 
        { transform: 'translatex(0px) translatey(0px)' }
      ], {
        duration: 400,
        
      })
    }else if(this.position<=299&&this.boolTrack){
      this.boolTrack=false;
      document.getElementById("modal").animate([
        { transform: 'translatex(0px) translatey(0px)' }, 
        { transform: 'translatex(-300px) translatey(-200px)' }
      ], {
        duration: 400,
        
      })
      
      setTimeout( () => { document.getElementById("modal").style.display='none' }, 400 );
      
    }

  }
}
input(event){
  document.getElementById("valider"+event).style.display='block'
  if(event==this.biereMoment.id){
    document.getElementById("valider2"+event).style.display='block'
  }
}
input2(event){
  document.getElementById("valider2"+event).style.display='block'
}
input3(event){
  document.getElementById("valider3"+event).style.display='block'
}
valider(event){
  this.flou()
  document.getElementById("valider"+event).style.display='none'
}
valider2(event){
  this.flou()
  document.getElementById("valider2"+event).style.display='none'
}
valider3(event){
  this.flou()
  document.getElementById("valider3"+event).style.display='none'
}
cardAnimate(event){
  document.getElementById("biereImgCard"+event).animate([
    { transform: 'scale(1)' }, 
    { transform: 'scale(5)' }
  ], {
    duration: 200,
    
  })
  
  setTimeout( () => { document.getElementById('modalTest').style.display='block' }, 150 );
  setTimeout( () => { this.router.navigateByUrl("detail/"+event) }, 200 );

  
}
test(event){
  document.getElementById("biereImg").animate([
    { transform: 'scale(1)' }, 
    { transform: 'scale(5)' }
  ], {
    duration: 200,
    
  })
  setTimeout( () => { document.getElementById('modalTest').style.display='block' }, 150 );
  setTimeout( () => { this.router.navigateByUrl("detail/"+event) }, 200 );

  
}
test2(event){
  document.getElementById("biereImg2").animate([
    { transform: 'scale(1)' }, 
    { transform: 'scale(5)' }
  ], {
    duration: 200,
    
  })
  setTimeout( () => { document.getElementById('modalTest').style.display='block' }, 150 );
  setTimeout( () => { this.router.navigateByUrl("detail/"+event) }, 200 );

  
}
test3(event){
  document.getElementById("biereImg3").animate([
    { transform: 'scale(1)' }, 
    { transform: 'scale(5)' }
  ], {
    duration: 200,
    
  })
  setTimeout( () => { document.getElementById('modalTest').style.display='block' }, 150 );
  setTimeout( () => { this.router.navigateByUrl("detail/"+event) }, 200 );

  
}

 

  findbyname() {
    var textarea =<HTMLInputElement> document.getElementById('search').firstChild
    textarea.addEventListener('keyup', (e) => {
      this.touche = e.key
    });
    if (this.recherche.length === 0 && this.touche === "Backspace" && this.bool === true) {
    } else {
      if (this.recherche.length === 0) {
        this.bool = true;
      } else {
        this.bool = false;
      }
      this.recherche=this.recherche.substring(0,1).toUpperCase()+this.recherche.substring(1).toLowerCase()
      this.biereService.findbynameClient(this.recherche,this.page).subscribe(
        donnees => {
          this.biereService.subjectMiseAJour.next(0)
          this.bieres = donnees;
        }
      );
    }
  }

  validerPanier(event){

this.panierService.create(this.objJson.nom,this.commande).subscribe(
  data=>{
  }
)
  }



emit(event){
  
  this.commandeService.getCommande(this.objJson.nom).subscribe(
    data1=>{
      this.commande=data1;
          this.biereService.getAllSansPage().subscribe(
            donnee => {
              this.bieres3 = donnee;
              this.bieres3.forEach(element => {
                element.commande=0;
              }
              );                     
              this.biereService.getAll(this.page.toString()).subscribe(
                donnees => {
                  this.bieres = donnees;
              if(this.commande.length>0){
                
                localStorage.setItem("commande",'1')
                this.commande.forEach(element1 => {
              this.bieres3.find(biere=>biere.nom===element1.nom).commande=element1.commande
                });

              }
              this.bieres.forEach(element=>{
                this.bieres3.forEach(element2=>{
                  if(element.id==element2.id){
                    element.commande=element2.commande
                  }
                  if(element2.id==this.favBiere.id){
                    this.favBiere.commande=element2.commande
                  }
                  if(element2.id==this.biereMoment.id){
                    this.biereMoment.commande=element2.commande
                  }

                })
                
              })
              this.total=event

              // if(this.sorting!='rien'||this.filtre.length>0){
                this.refresh()
              // }
          });
        });
         
         
        
    }
  );
}

  findByFilter(event:string) {
    if(event=="noAll"){
      document.getElementById("noAll").style.display='block'
      document.getElementById("all").style.display='none'
    }else if(event=="all"){
      this.filtre=[]
      document.getElementById("noAll").style.display='none'
      document.getElementById("all").style.display='block'
      this.typeDebieres.forEach(element=>{
        document.getElementById("No"+element.nom).style.display='block'
        document.getElementById(element.nom).style.display='none'
      })
      this.refresh()

    }else{
      if(event.startsWith("No")){
        
        document.getElementById(event).style.display='block'
        document.getElementById(event.substring(2,event.length)).style.display='none'

        this.typeDebieres.find(element=>{
          if(element.nom==event.substring(2,event.length)){
            
            if(this.filtre.includes(element.id)){
             var index1= this.filtre.indexOf(element.id)
              this.filtre.splice(index1,1) 
            }
          }

         })

         this.refresh()
      }else{
        document.getElementById("No"+event).style.display='none'
        document.getElementById(event).style.display='block'
        document.getElementById("noAll").style.display='block'
        document.getElementById("all").style.display='none'

        this.typeDebieres.find(element=>{
          if(element.nom==event){
           
            if(!this.filtre.includes(element.id)){
              this.filtre.push(element.id) 
            }
          }
         })
         this.refresh()
      }

   
  }
}


flou(){
  if(this.user!=null){
    document.getElementById('id02').style.webkitFilter = 'blur(17px)'
    var top= this.position+window.innerHeight/2.8
  
    document.getElementById('myModal1').style.top=top.toString()+'px'
    document.getElementById('myModal1').style.display='block'

  }
}

refresh(){
  document.getElementById("annexe").style.color='green'
  setTimeout(()=>{document.getElementById("annexe").style.color='rgb(255, 196, 0)'},1000)
  
  if(this.filtre.length==0){
    
    this.biereService.refreshList.subscribe(() => {
      this.biereService.findbytype(this.filtre1,this.page,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting) });
    this.biereService.findbytype(this.filtre1,this.page,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
      data=>{
        this.bieres=data
        if(this.bieres.length==0&&this.page>0){
          this.page--
          this.biereService.findbytype(this.filtre1,this.page,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
            data=>{
              this.bieres=data
            }
          )

        }
        if(this.user!=undefined){
          
          this.commandeService.getCommande(this.objJson.nom).subscribe(
            data1=>{
              this.commande=data1;
              if(this.commande.length>0){
                this.commande.forEach(element1 => {
                  this.biere= this.bieres.find(biere=>biere.nom===element1.nom)
                  if(this.biere==undefined){
                    document.getElementById('id02').style.webkitFilter = 'blur(0px)'
                    document.getElementById('myModal1').style.display='none' 
                    





                  }else{
                    this.biere.commande=element1.commande
                    this.bieres.indexOf(this.biere)
                    this.bieres.splice(this.bieres.indexOf(this.biere),1,this.biere)

                  }
     
          if(this.biereMoment.nom==element1.nom){
            this.biereMoment.commande= element1.commande
          }
          if(this.favBiere.nom==element1.nom){
            this.favBiere.commande= element1.commande
          }
            });
            document.getElementById('id02').style.webkitFilter = 'blur(0px)'
            document.getElementById('myModal1').style.display='none' 



          }else{
            document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
            document.getElementById('myModal1').style.display='none'
          }
        });

        }else{
          this.bieres.forEach(biere=>biere.commande=0)
          document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
          document.getElementById('myModal1').style.display='none'
        }
      }

    )  
    this.biereService.findbytype(this.filtre1,this.page+1,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
        data=>{
          this.bieres2=data
        }
      )  
  }else{
   
    this.biereService.refreshList.subscribe(() => {
    this.biereService.findbytype(this.filtre,this.page,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting) });
    this.biereService.findbytype(this.filtre,this.page,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
      data=>{
        this.bieres=data
        if(this.bieres.length==0&&this.page>0){
          this.page--
          this.biereService.findbytype(this.filtre1,this.page,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
            data=>{
              this.bieres=data
            }
          )

        }
        if(this.user!=undefined){
          
          this.commandeService.getCommande(this.objJson.nom).subscribe(
            data1=>{
              this.commande=data1;
              if(this.commande.length>0){
                this.commande.forEach(element1 => {
                  this.biere= this.bieres.find(biere=>biere.nom===element1.nom)
                  if(this.biere==undefined){
                    document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
                    document.getElementById('myModal1').style.display='none'
                  }else{
                    this.biere.commande=element1.commande
                    this.bieres.indexOf(this.biere)
                    this.bieres.splice(this.bieres.indexOf(this.biere),1,this.biere)

                  }
     
          if(this.biereMoment.nom==element1.nom){
            this.biereMoment.commande= element1.commande
          }
          if(this.favBiere.nom==element1.nom){
            this.favBiere.commande= element1.commande
          }
            });
            document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
            document.getElementById('myModal1').style.display='none'
          }else{
            document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
            document.getElementById('myModal1').style.display='none'
          }
        });

        }else{
          this.bieres.forEach(biere=>biere.commande=0)
          document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
          document.getElementById('myModal1').style.display='none'
        }
      }
    ) 
    this.biereService.findbytype(this.filtre1,this.page+1,true,false,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
      data=>{
        this.bieres2=data
      }
    )     

  }
}

sort(event){
  this.sorting=event
this.refresh()
}

sliderMin(event){
  this.prixMin=event.target.valueAsNumber
   if(event.target.valueAsNumber>this.prixMax){
     this.prixMax=event.target.valueAsNumber
   }
   this.refresh()
 }

 sliderMax(event){
  this.prixMax=(event.target.valueAsNumber)
  if(this.prixMax<this.prixMin){
    this.prixMin=this.prixMax
  }
  this.refresh()
}

sliderMinQte(event){
  this.qteMin=event.target.valueAsNumber
   if(event.target.valueAsNumber>this.qteMax){
     this.qteMax=event.target.valueAsNumber
   }
   this.refresh()
 }

 sliderMaxQte(event){
  this.qteMax=(event.target.valueAsNumber)
  if(this.qteMax<this.qteMin){
    this.qteMin=this.qteMax
  }
  this.refresh()
}

sliderMinAlc(event){
  this.alcMin=event.target.valueAsNumber
   if(event.target.valueAsNumber>this.alcMax){
     this.alcMax=event.target.valueAsNumber
   }
   this.refresh()
 }

 sliderMaxAlc(event){
  this.alcMax=(event.target.valueAsNumber)
  if(this.alcMax<this.alcMin){
    this.alcMin=this.alcMax
  }
  this.refresh()
}

minimumInput(event){
  this.prixMin=event.target.valueAsNumber
    if(this.prixMin>this.prixMax){
      this.prixMax=this.prixMin
    }
    this.refresh()
}

maximumInput(event){
  this.prixMax=event.target.valueAsNumber
  if(this.prixMax<this.prixMin){
    this.prixMin=this.prixMax
  }
  this.refresh()
}

minimumInputQte(event){
  this.qteMin=event.target.valueAsNumber
  if(this.qteMin>this.qteMax){
    this.qteMax=this.qteMin
  }
  this.refresh()
}

maximumInputQte(event){
this.qteMax=event.target.valueAsNumber
if(this.qteMax<this.qteMin){
  this.qteMin=this.qteMax
}
this.refresh()
}

minimumInputAlc(event){
this.alcMin=event.target.valueAsNumber
if(this.alcMin>this.alcMax){
  this.alcMax=this.alcMin
}
this.refresh()
}

maximumInputAlc(event){
this.alcMax=event.target.valueAsNumber
if(this.alcMax<this.alcMin){
this.alcMin=this.alcMax
}
this.refresh()
}

 pagination(event){
    window.scrollTo(0,180)
    if(event=="precedent"){
      if(this.page>0){
        
        this.page--
        localStorage.setItem("page", this.page.toString());
        this.refresh()
      }

    }else{
      
        this.page++
        localStorage.setItem("page", this.page.toString());
        this.refresh()
    }
  }


}

