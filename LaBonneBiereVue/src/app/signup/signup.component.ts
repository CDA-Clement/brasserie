import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { faUser, faLock, faEnvelope, faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { AlertService } from '../service/alert.service';
import { RoleDto } from '../model/role-dto';
import { ReponseDto } from '../model/reponse-dto';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  faEnvelope = faEnvelope;
  faCalendarAlt = faCalendarAlt;
  utilisateur= new UtilisateurDto();
  utilisateur2:object;
  submit:boolean = false;
  origin=''
  rep: ReponseDto;
  
  constructor(private utilisateurService: UtilisateurService, private router: Router, private alertService: AlertService) { }
    
    ngOnInit() {
      document.getElementById('margin').style.display = 'none';
      this.origin=localStorage.getItem('origine')
      window.scrollTo(0,0)
      document.getElementById('footerID').style.display="block";

      document.getElementById("general").style.webkitFilter = 'blur(0px)';
      
      this.submit=false;
    }

 modalite(){
   var bPass=true;
   var bAge=true;
  if (this.utilisateur.nom == undefined ||
    this.utilisateur.prenom == undefined ||
    this.utilisateur.username == undefined ||
    this.utilisateur.dateAge == undefined ||
    this.utilisateur.password == undefined ||
    this.utilisateur.adresseMail == undefined) {
    this.alertService.addPrimary('un ou plusieurs champs manquants')
  } else {
    var date=new Date(this.utilisateur.dateAge)
    if(!this.utilisateur.password.match(RegExp(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}/))){
      bPass=false
      this.alertService.addDanger('Validité du mot de passe incorrecte, 8 characteres AlphaNumériques et 1 charactere spécial')
      
    }
    if(new Date().getTime()-date.getTime()<567648000000){
      bAge=false
      this.alertService.addDanger('Votre age est inférieur à 18 ans, création de compte impossible')
    }

    if(bPass&&bAge){
      document.getElementById('myModal1').style.display='block'
    }

  }

 }
 cancel(){
  document.getElementById('myModal1').style.display='none'
 }
  signup() {
    
      document.getElementById("spinner").style.display="block"
      document.getElementById("id02").style.display="none"
      
        
        this.utilisateurService.createUtilisateurFree(this.utilisateur).subscribe(
          data => {
            this.rep=data
            if(this.rep.msg==='ok'){
              document.getElementById("spinner").style.display="none"
              this.alertService.addPrimary("email d'activation de compte envoyé à "+this.utilisateur.adresseMail)
              this.router.navigate(['/accueil']);
            }else if(this.rep.msg==='ADRESSE_MAIL'){
              document.getElementById("spinner").style.display="none"
              document.getElementById("myModal1").style.display="none"
              document.getElementById("id02").style.display="block"
              this.alertService.addPrimary("adresse mail déjà existante")
            }else{
              document.getElementById("spinner").style.display="none"
              document.getElementById("myModal1").style.display="none"
              document.getElementById("id02").style.display="block"
              this.alertService.addPrimary("username déjà existant")
            }
            
          },
          error =>{
            console.log("la")
            document.getElementById("spinner").style.display="none"
            document.getElementById("myModal1").style.display="none"
          });
       
    }

   
  
  

  spinnerClose() {
    this.alertService.addDanger("Oops une erreur est survenue lors de la création du compte")
    document.getElementById('spinner').style.display = 'none';
    document.getElementById('id02').style.display = 'block';
    
  }

  modalLoginClose() {
    document.getElementById("general").style.webkitFilter = 'blur(17px)';
    if(this.origin!=''){
      this.router.navigateByUrl(this.origin)
    }else{
      this.router.navigateByUrl('accueil')
    }
    
    
  }

}
