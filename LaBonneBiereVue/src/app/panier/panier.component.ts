import { Component, OnInit } from '@angular/core';
import { PanierService } from '../service/panier.service';
import { CommandeService } from '../service/commande.service';
import { CommandeDTO } from '../model/commande-dto';
import { faPlus, faMinus, faTrash,faCheck, faTimes, faBackward } from '@fortawesome/free-solid-svg-icons';
import { PanierDto } from '../model/panier-dto';
import { BiereService } from '../service/biere.service';
import { BiereDto } from '../model/biere-dto';
import { environment } from 'src/environments/environment';
import { AlertService } from 'src/app/service/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {
  commandes:CommandeDTO[]=[]
  objJson :any;
  user = localStorage.getItem("current_user");
  faPlus = faPlus;
  faMinus = faMinus;
  faTrash=faTrash;
  faCheck=faCheck;
  faTimes=faTimes;
  faBackward=faBackward;
  panier:PanierDto=new PanierDto();
  biere:BiereDto=new BiereDto()
  url:String=environment.backSchema+"://"+environment.backServer
  total=0;
  bool:Boolean;


  constructor(private router: Router,private biereService: BiereService,private panierService: PanierService,private commandeService: CommandeService,private alertService: AlertService) { }

  ngOnInit() {
    window.scrollTo(0,0)
    if (localStorage.getItem('current_user') == null) {
      this.router.navigateByUrl("/login")
    }
    this.objJson = JSON.parse(this.user);
this.commandeService.getCommande(this.objJson.nom).subscribe(
  data=>{
    this.commandes=data;
    this.commandes.forEach(element=>{
      this.total+=element.total
      var stringTo=this.total.toFixed(2);
      this.total=parseFloat(stringTo);
    })
  }
)

  }

  validerPanier(event){
    this.commandeService.getCommande(this.objJson.nom).subscribe(
      data=>{
        this.commandes=data;
        if(this.commandes.length==0){
          this.alertService.addPrimary("redirection vers la boutique panier expiré")
          this.router.navigateByUrl('/boutique')
        }else{
          document.getElementById('modal').style.display='block'
          document.getElementById('list').style.display='none'
          document.getElementById('button').style.webkitFilter = 'blur(2px)'
          var element = <HTMLInputElement> document.getElementById('panierbtn')
          element.disabled=true;

        }
      }
      )
  }

  confirmation(){
    var num=0
    this.commandeService.getCommande(this.objJson.nom).subscribe(
      data=>{
        this.commandes=data;
        this.commandes.forEach(element=>{
         if(element.commande==0){
           num++
         }
        })
        if(this.commandes.length>=1&&num<this.commandes.length){
          this.alertService.addPrimary("commande en cours de validation")
          document.getElementById('modal').style.display='none'
          document.getElementById('modal3').style.display='block'
          this.panierService.create(this.objJson.nom, this.commandes).subscribe(
            data=>{
              this.panier=data
              localStorage.setItem("panierConfirmed","1")
              document.getElementById('modal3').style.display='none'
              document.getElementById('modal1').style.display='block'
            }
          )
        }else{
          document.getElementById('modal').style.display='none'
          document.getElementById('modal2').style.display='block'
        }
      }
      )

  }

  annulation(){
    document.getElementById('modal').style.display='none'
    document.getElementById('modal2').style.display='none'
    document.getElementById('list').style.display='block'
    var element = <HTMLInputElement> document.getElementById('panierbtn')
    element.disabled=false;
    document.getElementById('button').style.webkitFilter = 'blur(0px)'
    
  }

  retourBoutique(){
    this.router.navigateByUrl('/boutique')
  }

  qttmoinsPanier(event){
    
    this.biereService.getOne(event).subscribe(
      data1=>{
        this.biere=data1;
        this.commandeService.getCommande(this.objJson.nom).subscribe(
          data=>{
            this.commandes=data;
            if(this.commandes.find(commande=>commande.idBiere===event).commande>0){
              this.biere.quantite=-1;
       this.commandeService.createCommande(this.biere,this.objJson.nom).subscribe(
         data=>{
           this.commandes=data;
           this.total=0
           this.commandes.forEach(element => {
            this.total+=element.prixUnitaire*element.commande
            var stringTo=this.total.toFixed(2);
            this.total=parseFloat(stringTo);
            var numb=0
            this.commandes.forEach(element=>{
              if(element.commande===0){
                numb++
              }
            })
            if(numb==this.commandes.length){
              this.bool=true
            }    
           });
    }
       )
            }
          }
        )
}
    )
}
qttplusPanier(event){
    
  this.biereService.getOne(event).subscribe(
    data1=>{
      this.biere=data1;
      this.commandeService.getCommande(this.objJson.nom).subscribe(
        data=>{
          this.commandes=data;
          if(this.commandes.find(commande=>commande.idBiere===event).commande>0){
            this.biere.quantite=+1;
            if(this.biere.quantite>0){
              this.commandeService.createCommande(this.biere,this.objJson.nom).subscribe(
                data=>{
                  this.commandes=data;
                  this.total=0
                  this.commandes.forEach(element => {
                   this.total+=element.prixUnitaire*element.commande
                   var stringTo=this.total.toFixed(2);
                   this.total=parseFloat(stringTo);  
                  });
           }
              )

            }
          }
        }
      )
}
  )
}
deleteCommande(event){
  this.biereService.getOne(event).subscribe(
    data1=>{
      this.biere=data1;
      this.commandeService.getCommande(this.objJson.nom).subscribe(
        data=>{
          this.commandes=data;
          var com=this.commandes.find(commande=>commande.idBiere===event).commande
          if(com>0){
           // this.biere.quantite-=com;
     this.commandeService.deleteCommande2(this.objJson.nom,event,com).subscribe(
       data=>{
         this.commandeService.getCommande(this.objJson.nom).subscribe(
           data1=>{
             this.commandes=data1
             this.total=0
         this.commandes.forEach(element => {
          this.total+=element.prixUnitaire*element.commande
          var stringTo=this.total.toFixed(2);
          this.total=parseFloat(stringTo);
          var numb=0
          this.commandes.forEach(element=>{
            if(element.commande===0){
              numb++
            }
          })
          if(numb==this.commandes.length){
            this.bool=true
          }  
         });
           }
         )
         
  }
     )
          }
        }
      )
}
  )
}
}
