import { Component, OnInit, HostListener, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { faShoppingCart,faPlus, faMinus, faTrash,faBars } from '@fortawesome/free-solid-svg-icons';
import { AlertDto } from '../model/alert-dto';
import { AlertService } from '../service/alert.service';
import { CommandeService } from '../service/commande.service';
import { CommandeDTO } from '../model/commande-dto';
import { BiereService } from '../service/biere.service';
import { BiereDto } from '../model/biere-dto';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./topnavbar.component.css'],
})
export class TopnavbarComponent implements OnInit {
  nbclic:number=0;
  isConnected: boolean;
  isAdmin: boolean;
  faTrash=faTrash
  faShoppingCart=faShoppingCart
  faHam=faBars
  faPlus=faPlus
  faMinus=faMinus
  position: number=0
  posBool: boolean=false
  posBool2: boolean;
  biere: BiereDto=new BiereDto()
  bieres=new Array <BiereDto>()
  count: number;
  alerts: AlertDto[];
  user = localStorage.getItem("current_user")
  objJson :any;
  commande:CommandeDTO[];
  @Input() total1:number=-1;
  items:number=0;
  @Input() page=0
  total:number=0;
 
  @Output()parent=new EventEmitter<any>()
  panierco: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  bool=false

 

  constructor(private router: Router, private commandeService: CommandeService,private biereService: BiereService, private authService: AuthService,private alertService: AlertService) {
  }

  ngOnInit() {
    if(this.user!=null){
      
      this.objJson = JSON.parse(this.user);
          this.commandeService.getCommande(this.objJson.nom).subscribe(
            data1=>{
              this.commande=data1;
              
                  this.biereService.getAllSansPage().subscribe(
                    donnees => {
                      
                      this.bieres = donnees;
                      
                      this.bieres.forEach(element => {
                        element.commande=0;
                      }
                      );                     
                      if(this.commande.length>0){
                        localStorage.setItem("commande",'1')
                        this.commande.forEach(element1 => {
                      this.total+=element1.total
                      var stringTo=this.total.toFixed(2);
                      this.total=parseFloat(stringTo);
                      this.items+=element1.commande
                      this.bieres.find(biere=>biere.nom===element1.nom).commande=element1.commande
                        });
                        this.parent.emit(this.total)

                      }
                  });
                 
                  if(this.commande.length>0){
           
                    if((new Date().getTime() + 24*60*60*1000)-parseInt(localStorage.getItem("cartTime"),10)>=1200000){
                    
                     this.cleanCart()
                    }
       
                }
            }
            
          );
       
    }else{  
        this.biereService.getAllSansPage().subscribe(
            donnees => {
             
              this.bieres = donnees;
              
            }
          ); 
         
    }
    this.alerts = [];
    this.alertService.alertSubject.subscribe(
      res => {
        this.alerts.push(res);
        setTimeout(() => {
          res.closed = true;
          this.alerts.splice(this.alerts.indexOf(res), 1);
        }, 2000);
      }
    );
    this.reloadData();
  }

   qttmoinsPanier(event){
    
    
     this.biereService.getOne(event).subscribe(
       data=>{
         this.biere=data
         this.commandeService.getCommande(this.objJson.nom).subscribe(
           data1=>{
             this.commande=data1
           
             this.biere.commande=this.commande.find(commande=>commande.idBiere===event).commande
            
           if(this.biere.commande>0){
             this.qttmoins(this.biere);
           }else{
             this.biere.quantite=0;
             this.commandeService.createCommande(this.biere,this.objJson.nom).subscribe(
               data=>{
                 this.commande=data;
                 this.items=0;
                this.commande.forEach(commande=>{
                   this.items+=commande.commande
                 })
                 if(this.commande.length==0){
                   document.getElementById('panierco').style.display='none'
                 }

               }
             )
           }
           }
         )
       }
   )
 }



 qttplusPanier(event){
   this.biereService.getOne(event).subscribe(
     data=>{
       this.biere=data
       this.commandeService.getCommande(this.objJson.nom).subscribe(
         data1=>{
           this.commande=data1
           this.biere.commande=this.commande.find(commande=>commande.idBiere===event).commande
         if(this.biere.quantite>0){
           this.qttplus(this.biere);
         }
         }
       )
     }
   )

 }

 valider(event) { 
  if(this.objJson!=null){
    if(localStorage.getItem("cartTime")==null){

      var temps :number=(new Date().getTime() + 24*60*60*1000)

      localStorage.setItem("cartTime",temps.toString())
      setTimeout(() => this.cleanCart(), 1200000);
    }


        this.biereService.getOne(event.id).subscribe(
          data1=>{
            this.biere=data1;
         if(this.biere.quantite>0){   

            this.commandeService.getCommande(this.objJson.nom).subscribe(
              data=>{
                this.commande=data
                this.biereService.getAllSansPage().subscribe(
                  donnees => {
                   
                    this.bieres = donnees;
                    this.bieres.find(biere => biere.id === event.id).commande=event.commande;
                    if(this.commande.length>0){
                      this.bool=true
                      if(this.commande.find(commande=>commande.idBiere===event.id)){
                        var c=this.commande.find(commande=>commande.idBiere===event.id).commande
                        if(c!=event.commande){
                          event.commande=event.commande-c
                        }else{
                          this.bool=false
                        }

                      }

                    }

                  if(event.commande<=this.biere.quantite){
                    this.biere.quantite=event.commande
                  }

                  if(this.commande.length==0||this.bool){
                    this.commandeService.createCommande(this.biere,this.objJson.nom).subscribe(
                      data=>{
                        this.commande=data;
                        this.total+=event.prix*event.commande;
                        var stringTo=this.total.toFixed(2);
                        this.total=parseFloat(stringTo);
                        this.items=0;
                        this.commande.forEach(commande=>{
                          this.items+=commande.commande
                        })
                        this.parent.emit(this.total)
                       }
                    );

                  }else{
                    this.parent.emit(this.total)
                  }

              }
              );
              }
            );
         }
        }
        );
     
  }else{
    this.alertService.addPrimary("veuillez vous connecter pour commander")
}
}


  qttplus(event) {
  
    this.nbclic++;
    if(this.objJson!=null){
      
      if(localStorage.getItem("cartTime")==null){

        var temps :number=(new Date().getTime() + 24*60*60*1000)

        localStorage.setItem("cartTime",temps.toString())
        setTimeout(() => this.cleanCart(), 1200000);
      }


          this.biereService.getOne(event.id).subscribe(
            data1=>{
              this.biere=data1;
           if(this.biere.quantite>0){   

              this.commandeService.getCommande(this.objJson.nom).subscribe(
                data=>{
                  this.commande=data
                  this.biereService.getAllSansPage().subscribe(
                    donnees => {
                     
                      this.bieres = donnees;
                  

                    this.bieres.find(biere => biere.id === event.id).commande++;
                     this.biere.quantite=1;

                  this.commandeService.createCommande(this.biere,this.objJson.nom).subscribe(
                    data=>{
                      this.commande=data;
                      this.total+=event.prix;
                      var stringTo=this.total.toFixed(2);
                      this.total=parseFloat(stringTo);
                      this.items=0;
                      this.commande.forEach(commande=>{
                        this.items+=commande.commande
                      })
                      this.parent.emit(this.total)
                     }
                  );
                }
                );
                }
              );
           }
           else{
            this.parent.emit(this.total)
         }
          }
          );
       
    }else{
      this.alertService.addPrimary("veuillez vous connecter pour commander")
    }
}

qttmoins(event) {
  
  if(this.objJson!=null){
    
    
    this.biereService.getOne(event.id).subscribe(
      data=>{
        this.biereService.getOne(event.id).subscribe(
          data1=>{
            this.biere=data1;
         if(event.commande>0){
          this.bieres.find(biere => biere.id === event.id).commande--;
           
           this.biere.quantite=-1;
           this.commandeService.createCommande(this.biere,this.objJson.nom).subscribe(
             data=>{
               this.commande=data;
               this.items=0;
               this.total-=event.prix;
               var stringTo=this.total.toFixed(2);
               this.total=parseFloat(stringTo);  
               this.commande.forEach(commande=>{
                 this.items+=commande.commande
              })  
              this.parent.emit(this.total)
              }
           );
         }
         else{
            this.parent.emit(this.total)
         }
        }
        );
      }
    );

  }else{
    this.alertService.addPrimary("veuillez vous connecter pour commander")
  }
}

cleanCart(){
  localStorage.removeItem('cartTime')
  this.bieres.forEach(element=>{
    element.commande=0;
    this.total=0;
  })
    this.commandeService.getCommande(this.objJson.nom).subscribe(
      data2=>{
        this.commande=data2
        var size=this.commande.length

        if(localStorage.getItem("panierConfirmed")==null){
         this.commande.forEach(element=>{
         this.commandeService.deleteCommande2(this.objJson.nom,element.idBiere.toString(),element.commande).subscribe(
           data1=>{
             this.commande=[];


             if(localStorage.getItem("commande")==='1'){

               localStorage.removeItem('commande')
               window.location.reload()
             }
            }
         )
          }
      
          )
          if(size>0){
            this.alertService.addPrimary("votre panier a été annulé temps expiré")
          }
   }else{
     localStorage.removeItem("panierConfirmed")
   }
}
  )
}

mouse(){
  if(!this.panierco){
      document.getElementById('panierco').style.display='block'
      this.panierco=true
  }else if(this.panierco){
    document.getElementById('panierco').style.display='none'
    this.panierco=false
  }
   
 
}




  

  track(event){
  
    this.position=parseInt(event.path[1].window.scrollY)
    var top = this.position+320  
    document.getElementById('panierco').style.top=top.toString()+'px'
    if(this.position>=180){
      top = this.position+100  
      document.getElementById('panierco').style.top=top.toString()+'px'
     this.posBool=true 
     document.getElementById("navbar").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("navbar").style.backgroundColor='rgba(255, 196, 0, 0.65)'
     document.getElementById("navbar").style.backgroundColor='rgba(255, 196, 0, 0.65)'
     document.getElementById("li").style.color='black'
     document.getElementById("li1").style.color='black'
     document.getElementById("li2").style.color='black'
     
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
      top = this.position+320 
      document.getElementById('panierco').style.top=top.toString()+'px'
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("navbar").style.boxShadow= "none"
       document.getElementById("navbar").style.height='auto'
       document.getElementById("navbar").style.backgroundColor='rgb(0,0,0)'
       document.getElementById("li").style.color='white'
       document.getElementById("li1").style.color='white'
       document.getElementById("li2").style.color='white'
      
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=0
     img.height=0 
     }
     
  }
  countM(){
    this.count++
    if(this.count>20){   
      this.posBool=false
    }
  }


  reloadData() {
    this.isConnected = this.authService.isConnected();
    if (this.authService.getCurrentUser()) {
      this.isAdmin = this.authService.getCurrentUser().role.nom === "Admin";
      
    }
    this.authService.subjectConnexion.subscribe(
      res => {
        this.isConnected = this.authService.isConnected();
        if (this.authService.getCurrentUser()) {
          this.isAdmin = this.authService.getCurrentUser().role.nom === "Admin";
          
        }

      }
    )
  }
  logout() {
    localStorage.clear();
    localStorage.setItem('age', '18');
    this.reloadData();

  }
}
