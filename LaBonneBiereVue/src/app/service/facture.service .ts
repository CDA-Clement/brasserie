import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class FactureService {
  
  factureUrlPeriod =  `${environment.backSchema}://${environment.backServer}/achatsPeriod/`;
  oneFactureUrl =  `${environment.backSchema}://${environment.backServer}/achat/`;
  factureUrl =  `${environment.backSchema}://${environment.backServer}/achats/`;
  facture1Url =  `${environment.backSchema}://${environment.backServer}/achats`;

  allUserInvoicesPeriod(id: String, dateBeginningMonth: String, dateEndMonth: String): Observable<any> {
    return this.http.get(this.facture1Url+"Period/"+id+"/"+dateBeginningMonth+"/"+dateEndMonth);
  }
  
  subjectMiseAJour = new Subject<number>();
  constructor(private http: HttpClient) { }

  allUserInvoices(id:String): Observable<any> {
    return this.http.get(this.factureUrl+id);
  }

  allUserInvoicesPerDate(id:String,date:String,date2:String): Observable<any> {
    return this.http.get(this.factureUrlPeriod+id+"/"+date+"/"+date2);
  }

  oneInvoice(idUser:String,id:String): Observable<any> {
    return this.http.get(this.oneFactureUrl+idUser+'/'+id);
  }

  oneInvoicePerPrice(idUser:String,total:number): Observable<any> {
    return this.http.get(this.oneFactureUrl+'price/'+idUser+'/'+total);
  }

  favoriteUserBeer(id:String): Observable<any> {
    return this.http.get(this.factureUrl+"biereFavorite/"+id);
  }
  BiereMoment(): Observable<any> {
    return this.http.get(this.factureUrl+"biereMoment");
  }

  downloadPDF(id:String,invoice:String) {
 return this.http.get(this.factureUrl+"downloadInvoice/"+id+'/'+invoice,{
   responseType : 'blob',
   headers:new HttpHeaders().append('Content-type','application/pdf')
 });
 }

}
