import { Injectable } from '@angular/core';
import { TypeDeBiereDto } from '../model/type-de-biere-dto';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypeDeBiereService {
  //typedebieresUrl= 'http://localhost:8080/typeDeBieres/';
  typedebieresUrl =  `${environment.backSchema}://${environment.backServer}/typeDeBieres/`;


  typedebieres: TypeDeBiereDto[];
  typedebiere: TypeDeBiereDto;

  subjectMiseAJour = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    
    return this.http.get(this.typedebieresUrl);
  }

  getOne(value: number): Observable<any> {
    
    return this.http.get(this.typedebieresUrl + value + '/');
  }
  createTypeDeBiere(typedebiere: TypeDeBiereDto): Observable<object> {
    return this.http.post(this.typedebieresUrl, typedebiere);
  }
  updateBiere(id: number, typedebiere: TypeDeBiereDto): Observable<object> {
    return this.http.put(this.typedebieresUrl, typedebiere);
  }
  deleteBiere(id: number): Observable<any> {
    return this.http.delete(this.typedebieresUrl + id + '/');
  }

  findbyfilter(filter: number): Observable<any> {
  
    return this.http.get(this.typedebieresUrl + filter + '/');
  }
}
