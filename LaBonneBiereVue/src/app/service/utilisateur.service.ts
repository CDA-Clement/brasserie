import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  
  private utilisateurUrl =  `${environment.backSchema}://${environment.backServer}/utilisateurs/`
  private utilisateurFreeUrl =  `${environment.backSchema}://${environment.backServer}/utilisateursFree/`
  //private utilisateurUrl = 'http://localhost:8080/utilisateurs/'
  //private utilisateurFreeUrl = 'http://localhost:8080/utilisateursFree/'
  
  utilisateurs: UtilisateurDto[];
  utilisateur: UtilisateurDto;
  
  subjectMiseAJour = new Subject<number>();
  
  chargementDetail = new Subject<number>();
  
  refreshList = new Subject<void>();
  
  constructor(private http: HttpClient) {
  }
  
  get refreshLaList() {
    return this.refreshList;
  }
  findbytype(page: number, active: boolean, noActive: boolean, role: number, sorting: string): Observable<any> {
    return this.http.get(this.utilisateurUrl+page+"/"+active+"/"+noActive+"/"+role+"/"+sorting);
  }
  findbyname(recherche: string, page: number): Observable<any> {
    if (recherche.length === 0) {
      return this.http.get(this.utilisateurUrl+"All/"+page);
    } else {
      return this.http.get(this.utilisateurUrl +"recherche/"+ recherche);

    }
  }

  getAll(page:number): Observable<any> {
   
    return this.http.get(this.utilisateurUrl+"All/"+page);
  }

  getUtilisateur(id: number): Observable<any> {
   
    return this.http.get(this.utilisateurUrl + id + '/');
  }
  createUtilisateur(utilisateur: UtilisateurDto): Observable<any> {
    return this.http.post(this.utilisateurUrl, utilisateur).pipe(tap(() => { this.refreshLaList.next() }));
  }
  
  createUtilisateurFree(utilisateurq: UtilisateurDto): Observable<any> {
    return this.http.post(this.utilisateurFreeUrl, utilisateurq)
  }

  updateUtilisateur(utilisateur: UtilisateurDto): Observable<any> {
   
    return this.http.put(this.utilisateurUrl, utilisateur)
  }
  deleteUtilisateur(id: number): Observable<any> {
    return this.http.delete(this.utilisateurUrl + id);
  }
  
}