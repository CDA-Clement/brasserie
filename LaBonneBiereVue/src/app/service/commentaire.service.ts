import { Injectable } from '@angular/core';
import { CommentaireDto } from '../model/commentaire-dto';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentaireService {
  private commentaireUrl =  `${environment.backSchema}://${environment.backServer}/commentaires/`
  private commentaire2Url =  `${environment.backSchema}://${environment.backServer}/commentairesByBiere/`
  //private commentaireUrl = 'http://localhost:8080/commentaires/'
  //private commentaire2Url = 'http://locahost:8080/commentairesByBiere/'

  commentaires: CommentaireDto[];
  commentaire: CommentaireDto;

  subjectMiseAJour = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
   
    return this.http.get(this.commentaireUrl);
  }
  getAllByBiere(id: number): Observable<any> {
    
    return this.http.get(this.commentaire2Url + id + '/');
  }
  getcommentaire(id: number): Observable<any> {
   
    return this.http.get(this.commentaireUrl + id + '/');
  }
  createcommentaire(id: number, commentaire: CommentaireDto): Observable<object> {
    return this.http.post(this.commentaireUrl + id + '/', commentaire);
  }
  updatecommentaire(id: number, commentaire: CommentaireDto): Observable<object> {
    return this.http.put(this.commentaireUrl, commentaire.id);
  }
  deletecommentaire(id: number): Observable<any> {
    return this.http.delete(this.commentaireUrl + id + '/');
  }
}