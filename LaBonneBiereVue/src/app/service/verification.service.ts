import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class VerificationService {
 mailUrl =  `${environment.backSchema}://${environment.backServer}/send/`;
  //tableUrl = 'http://localhost:8080/tables/';
  
  

  subjectMiseAJour = new Subject<number>();
  constructor(private http: HttpClient) { }

  verification(id:String,token:String): Observable<any> {
    
    return this.http.get(this.mailUrl+id+"/"+token);
  }
}
