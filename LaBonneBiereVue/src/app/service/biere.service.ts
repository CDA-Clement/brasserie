
import { BiereDto } from '../model/biere-dto';

import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BiereService {
 


   bieresUrl =  `${environment.backSchema}://${environment.backServer}/bieres/`;
   boutiqueURL =  `${environment.backSchema}://${environment.backServer}/boutique/`;
   bieresUrl1 =  `${environment.backSchema}://${environment.backServer}/biere/`;
   biereUrlType =  `${environment.backSchema}://${environment.backServer}/bieres/type/`;

  //bieresUrl= 'http://1localhost:8080/bieres/';
  //boutiqueURL= 'http://localhost:8080/boutique/';
  //bieresUrl1= 'http://localhost:8080/biere/';
  //biereUrlType='http://localhost:8080/bieres/type/';



  bieres: BiereDto[];
  biere: BiereDto;

  subjectMiseAJour = new Subject<number>();

  chargementDetail = new Subject<number>();

  refreshList = new Subject<void>();

  constructor(private http: HttpClient) {
  }

  
  findbynameClient(recherche: string,page:number):Observable<any> {
    if (recherche.length === 0) {
      return this.http.get(this.boutiqueURL+page+"/"+true);
    } else {
      return this.http.get(this.boutiqueURL+"Client/recherche/"+recherche);

    }
   
  }


  getAllSansPage(): Observable<any> {
 
    return this.http.get(this.boutiqueURL);
  }

  getAll(page:string): Observable<any> {
 
    return this.http.get(this.boutiqueURL+page+"/"+"True");
  }

  getAllAdmin(page:string): Observable<any> {
   
    return this.http.get(this.boutiqueURL+page+"/"+"All");
  }

  getAllNonActive(page:string): Observable<any> {
    
    return this.http.get(this.boutiqueURL+page+"/"+"False");
  }

  getOne(value: number): Observable<any> {
    return this.http.get(this.bieresUrl1 + value + '/');
  }
  getSupprimable(value: string): Observable<any> {
    return this.http.get(this.bieresUrl1 + "deletable/"+value + '/');
  }
  createBiere(biere: BiereDto): Observable<any> {
    return this.http.post(this.bieresUrl, biere);
    /* .pipe(tap(() => { this.refreshLaList.next() })); */
  }
  updateBiere(id: number, biere: BiereDto): Observable<object> {
    return this.http.put(this.bieresUrl, biere);
   // .pipe(tap(() => { this.refreshList.next() }));
  }
  deleteBiere(id: number): Observable<any> {
    return this.http.delete(this.bieresUrl + id + '/');
  }

  findbyname(recherche: string,page:number): Observable<any> {
    if (recherche.length === 0) {
      return this.http.get(this.boutiqueURL+page+"/"+true);
    } else {
      return this.http.get(this.bieresUrl +"recherche/"+ recherche);

    }
  }

  findbytype(filter: number[],page:number,active:boolean,noActive:boolean,prixMin:number,prixMax:number,qteMin:number, qteMax:number, alcMin:number ,alcMax:number,sorting:string): Observable<any> {
    if(prixMin==undefined){
      prixMin=0
    }
    if(prixMin!=undefined){
      if(prixMin.toString()=='NaN'){
        prixMin=0
      }
    }
    
    if(prixMax==undefined){
      prixMax=0
    }
    if(prixMax!=undefined){
      if(prixMax.toString()=='NaN'){
        prixMax=0
      }
    }
    if(qteMin==undefined){
      qteMin=0
    }

    if(qteMin!=undefined){
      if(qteMin.toString()=='NaN'){
        qteMin=0
      }
    }
    if(qteMax==undefined){
      qteMax=0
    }

    if(qteMax!=undefined){
      if(qteMax.toString()=='NaN'){
        qteMax=0
      }
    }

    if(alcMin==undefined){
      alcMin=0
    }

    if(alcMin!=undefined){
      if(alcMin.toString()=='NaN'){
        alcMin=0
      }
    }
    if(alcMax==undefined){
      alcMax=0
    }

    if(alcMax!=undefined){
      if(alcMax.toString()=='NaN'){
        alcMax=0
      }
    }
   
    return this.http.get(this.biereUrlType + filter + '/'+page+"/"+active+"/"+noActive+"/"+prixMin+"/"+prixMax+"/"+qteMin+"/"+qteMax+"/"+alcMin+"/"+alcMax+"/"+sorting);

  }


}
