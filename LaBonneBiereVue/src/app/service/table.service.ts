import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TableDto } from '../model/table-dto';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TableService {
  subjectMiseAJour = new Subject<number>();
  refreshList = new Subject<void>();
  tableUrl =  `${environment.backSchema}://${environment.backServer}/tables/`;
   //tableUrl = 'http://localhost:8080/tables/';
   rien: string = "reservation"
  constructor(private http: HttpClient) { }

  createTable(table: TableDto): Observable<any> { 
   return this.http.post(this.tableUrl,table)
  }

  findbytype( active: boolean, noActive: boolean, qteMin: number, sorting: string, resa:boolean,noResa:boolean): Observable<any> {
    if(qteMin==undefined){
      qteMin=0
    }
    return this.http.get(this.tableUrl+"type/"+active+"/"+noActive+"/"+qteMin+"/"+sorting+"/"+resa+"/"+noResa)
  }


  updateTable(table: TableDto): Observable<any> {
    return this.http.put(this.tableUrl+"active",table)
  }
  

  getAll(): Observable<any> {
   
    return this.http.get(this.tableUrl+"All");
  }
  getAllbyPage(page:number): Observable<any> {
   
    return this.http.get(this.tableUrl+"All/"+page);
  }

  updateResa(idUser: number, idTable: number, disponible: number, heureresa:number): Observable<any> {
    
    return this.http.put(this.tableUrl + idUser + '/' + idTable + '/' + disponible+'/'+heureresa, this.rien);
  }

  getOne(id: number): Observable<any> {
    return this.http.get(this.tableUrl + id)
  }

  delete(id: number): Observable<any> {
   
    return this.http.delete(this.tableUrl + id)

  }
  resetAll(idUser: number): Observable<any> {
    return this.http.put(this.tableUrl + idUser, this.rien);
  }

  getTableParUtilisateur(idUser:number): Observable<any>{
    return this.http.get(this.tableUrl + 'utilisateur/'+idUser);
  }

  findbyname(recherche: string): Observable<any> {
    if (recherche.length === 0) {
      return this.http.get(this.tableUrl+"All/");
    } else {
      return this.http.get(this.tableUrl +"recherche/"+ recherche);

    }
  }
}
