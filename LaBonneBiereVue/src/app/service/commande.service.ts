import { Injectable } from '@angular/core';
import { CommandeDTO } from '../model/commande-dto';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { BiereDto } from '../model/biere-dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  private commandeUrl =  `${environment.backSchema}://${environment.backServer}/commandes/`
  //private commandeUrl = 'http://localhost:8080/commandes/'
  commandes: CommandeDTO[];
  commande: CommandeDTO;

  subjectMiseAJour = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(this.commandeUrl);
  }


  getCommande(user: String): Observable<any> {
  
    return this.http.get(this.commandeUrl + user );
  }
  createCommande(biere: BiereDto, user: string): Observable<any> {


    return this.http.post(this.commandeUrl + user + '/', biere);
  }
  updateCommande(commande: CommandeDTO) {
    return this.http.put(this.commandeUrl, commande.utilisateur.username + '/' + commande.nom + '/' + commande.commande);
  }

  deleteCommande(id: number): Observable<any> {
    return this.http.delete(this.commandeUrl + id + '/');
  }

  deleteCommande2(idUser: number,biere:string,quantite:number): Observable<any> {
    return this.http.delete(this.commandeUrl + idUser + '/'+biere+'/'+quantite);
  }
}