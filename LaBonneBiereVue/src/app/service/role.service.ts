import { Injectable } from '@angular/core';
import { RoleDto } from '../model/role-dto';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  
  private roleUrl= `${environment.backSchema}://${environment.backServer}/roles/`
  //private roleUrl = 'http://localhost:8080/roles/'
  
  
  roles: RoleDto[];
  role: RoleDto;
  subjectMiseAJour = new Subject<number>();
  refreshList = new Subject<void>();
  
  constructor(private http: HttpClient) { }
  getAll(): Observable<any> {
    
    return this.http.get(this.roleUrl);
  }

delete(role: number) {
  return this.http.delete(this.roleUrl+role);
}
create(newRole: String): Observable<any> {
return this.http.post(this.roleUrl,newRole);
}
  getUtilisateur(id: number): Observable<any> {
    
    return this.http.get(this.roleUrl + id + '/');
  }


}
