import { Injectable } from '@angular/core';
import { PanierDto } from '../model/panier-dto';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { environment } from 'src/environments/environment';
import { CommandeDTO } from '../model/commande-dto';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  panierUrl =  `${environment.backSchema}://${environment.backServer}/paniers/`
  //panierUrl= 'http://localhost:8080/paniers/';

  paniers: PanierDto[];
  panier: PanierDto;

  subjectMiseAJour = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  getOne(utilisateur: UtilisateurDto): Observable<any> {
    
    return this.http.get(this.panierUrl + utilisateur.username);
  }

  create(UtilisateurDto:String,Commande:CommandeDTO[]):Observable<any>{
    return this.http.put(this.panierUrl+UtilisateurDto,Commande);
  }




}
