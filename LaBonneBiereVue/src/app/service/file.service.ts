import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';




@Injectable({ providedIn: 'root' })
export class FileService {
	private imageUrl= `${environment.backSchema}://${environment.backServer}/image/`
	private tableUrl= `${environment.backSchema}://${environment.backServer}/imageTable/`
	
	attachementList:any=[]
	constructor(private http: HttpClient) {}


	uploadFileTable(file: File, nom: String) {
		const formdata: FormData = new FormData();
		formdata.append('file', file);
		const req = new HttpRequest('POST',this.tableUrl+nom , formdata, {
			  reportProgress: true,
			  responseType: 'text'
		});
		return this.http.request(req);
		
	}

  uploadFile(file: File,file2: File,nomBiere:String): Observable<HttpEvent<{}>> {
		const formdata: FormData = new FormData();
		formdata.append('file', file);
		formdata.append('file', file2);
		const req = new HttpRequest('POST',this.imageUrl+nomBiere , formdata, {
			  reportProgress: true,
			  responseType: 'text'
		});
	
		return this.http.request(req);
   }

   modifFile(file: File,nomBiere:String): Observable<HttpEvent<{}>> {
	const formdata: FormData = new FormData();
	formdata.append('file', file);
	const req = new HttpRequest('PUT',this.imageUrl+nomBiere , formdata, {
		  reportProgress: true,
		  responseType: 'text'
	});

	return this.http.request(req);
}

	modifFileTable(file: File,nom:String): Observable<HttpEvent<{}>> {
		const formdata: FormData = new FormData();
		formdata.append('file', file);
		const req = new HttpRequest('PUT',this.tableUrl+nom , formdata, {
			  reportProgress: true,
			  responseType: 'text'
		});
	
		return this.http.request(req);
	}


}