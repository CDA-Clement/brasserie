import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class IpClientService {
  ipClientUrl =  `${environment.backSchema}://${environment.backServer}/client`;

  constructor(private http:HttpClient) { }

  public getIPAddress(){  
    return this.http.get(this.ipClientUrl);  
  }  

}
