import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FileService } from '../service/file.service';
import { AlertService } from '../service/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TableService } from '../service/table.service';
import { TableDto } from '../model/table-dto';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-table-create',
  templateUrl: './table-create.component.html',
  styleUrls: ['./table-create.component.css']
})
export class TableCreateComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  submitted = false;
 table=new TableDto()
  selectedFiles: FileList;
  currentFile: File;
  nom:String=""
  fichierSelection:boolean=false;
  @Output()parent=new EventEmitter<Boolean>()

  constructor( private route: ActivatedRoute, 
    private tableService: TableService, 
    private router: Router,
    private alertService: AlertService,
    private fileService:FileService, 
    ) { }

    ngOnInit() {
      if (this.user != null) {
        if(this.objJson.role.nom!='Admin'){
          this.router.navigateByUrl('/boutique') 
        }
         
      }else{
        this.router.navigateByUrl('/boutique')
      }
    
      if (localStorage.getItem('age') === '18') {
        document.getElementById("general").style.webkitFilter = 'blur(0px)'
      }
    

    }
    selectFile(event) {
      this.selectedFiles = event.target.files;
      this.fichierSelection=true;
    }
    
    upload() {
      
      this.currentFile = this.selectedFiles.item(0);
     
       this.fileService.uploadFileTable(this.currentFile,this.nom).subscribe(response => {
      
        if (response instanceof HttpResponse) {
       
         }	  
       });    
        
    }
  
    onSubmitUtil() {
      
      var confirmee:boolean
      
      this.nom=this.table.nom
      
      if(this.table.nom==undefined||
        this.table.capacite==undefined||
        this.table.description==undefined
       ){
          this.alertService.addPrimary("il manque un ou plusieurs champs")
        }else{
          this.table.nbResa=0
          this.table.reservationEnCours=false
          this.table.active=true
          this.table.heure=20.0
          if(!this.fichierSelection){
           
            if(window.confirm("etes vous sur d'ajouter un biere sans image")){
              confirmee=true
            }
            if(confirmee){
              
              this.tableService.createTable(this.table)
              .subscribe(
                donnees => {
                  this.tableService.subjectMiseAJour.next(0);
                  
                  this.alertService.addSuccess("la bière "+this.table.nom+" à bien été créée");
                  (<HTMLFormElement>document.getElementById("form")).reset();
                  
                },
                err=>{
                }
                
                )
                
              this.upload()
  
            }
            else{
              
            }
  
            }else{
              
              this.tableService.createTable(this.table)
              .subscribe(
                donnees => {
                  this.tableService.subjectMiseAJour.next(0);
                  this.alertService.addSuccess("la bière "+this.table.nom+" a bien été créée");
                  (<HTMLFormElement>document.getElementById("form")).reset();
                  this.parent.emit(true)
                  
                },
                err=>{
                }
                
                )
                this.upload()
          }
  
    }
    this.fichierSelection=false;
    
  }

}
