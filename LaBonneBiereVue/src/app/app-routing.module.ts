import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { SignupComponent } from './signup/signup.component';
import { ListeDUtilisateurComponent } from './liste-d-utilisateur/liste-d-utilisateur.component';
import { UtilisateurDetailComponent } from './utilisateur-detail/utilisateur-detail.component';
import { UtilisateurCreateComponent } from './utilisateur-create/utilisateur-create.component';
import { LoginComponent } from './login/login.component';
import { PanelAdminComponent } from './panel-admin/panel-admin.component';
import { TableComponent } from './table/table.component';
import { BiereAdminDetailComponent } from './biere-admin-detail/biere-admin-detail.component';
import { GestionMonCompteUtilisateurComponent } from './gestion-mon-compte-utilisateur/gestion-mon-compte-utilisateur.component';
import { GestionMonCompteDetailComponent } from './gestion-mon-compte-detail/gestion-mon-compte-detail.component';
import { GestionMonCompteEditPasswordComponent } from './gestion-mon-compte-edit-password/gestion-mon-compte-edit-password.component';
import { GestionMonCompteUpdateComponent } from './gestion-mon-compte-update/gestion-mon-compte-update.component';
import { BiereAdminComponent } from './biere-admin/biere-admin.component';
import { PanierComponent } from './panier/panier.component';
import { ValidationComponent } from './validation/validation.component';
import { FactureComponent } from './facture/facture.component';
import { RechercheFactureComponent } from './recherche-facture/recherche-facture.component';
import { TableAdminComponent } from './table-admin/table-admin.component';
import { TableAdminDetailComponent } from './table-admin-detail/table-admin-detail.component';
import { ListeDeBiereComponent } from './liste-de-biere/liste-de-biere.component';
import { BiereDetailComponent } from './biere-detail/biere-detail.component';
import { CoursComponent } from './cours/cours.component';
import { CoursAdminComponent } from './cours-admin/cours-admin.component';

const routes: Routes = [
  { path: '', redirectTo: 'accueil', pathMatch: 'full' },
  { path: 'accueil', component: AccueilComponent },
  { path: 'boutique', component: ListeDeBiereComponent },
  { path: 'mon-compte/factures/recherche', component: RechercheFactureComponent },
  { path: 'table', component: TableComponent },
  { path: 'detail/:id', component: BiereDetailComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'gestion/users', component: ListeDUtilisateurComponent },
  { path: 'gestion/users/detail', component: UtilisateurDetailComponent },
  { path: 'users/:id', component: UtilisateurDetailComponent },
  { path: 'gestion/cours', component: CoursAdminComponent },
  { path: 'validation/:id/:token', component: ValidationComponent },
  { path: 'create', component: UtilisateurCreateComponent },
  { path: 'login', component: LoginComponent },
  { path: 'gestion/bieres', component: BiereAdminComponent},
  { path: 'gestion/tables', component: TableAdminComponent},
  { path: 'gestion/tables/detail', component: TableAdminDetailComponent },
  { path: 'gestion', component: PanelAdminComponent}, 
  { path: 'gestion/bieres/detail', component: BiereAdminDetailComponent },
  { path: 'mon-compte', component: GestionMonCompteUtilisateurComponent },
  { path: 'mon-compte/detail', component: GestionMonCompteDetailComponent },
  { path: 'mon-compte/password', component: GestionMonCompteEditPasswordComponent },
  { path: 'mon-compte/factures', component: FactureComponent },
  { path: 'mon-compte/edit', component: GestionMonCompteUpdateComponent },
  { path: 'panier', component: PanierComponent },
  { path: 'cours', component: CoursComponent }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
