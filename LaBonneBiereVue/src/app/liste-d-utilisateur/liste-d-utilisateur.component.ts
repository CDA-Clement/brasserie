import { Component, OnInit, Input } from '@angular/core';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { faSignOutAlt,faHome,faUserShield,faTools,faToggleOn,faExclamationTriangle, faTimes, faCheck,faUser, faSearch, faToggleOff,faUserCog,faTh,faTimesCircle, faBeer, faChair, faChartLine,faChalkboardTeacher,faReply, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { AlertService } from '../service/alert.service';
import { environment } from 'src/environments/environment';
import { RoleService } from '../service/role.service';
import { RoleDto } from '../model/role-dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-d-utilisateur',
  templateUrl: './liste-d-utilisateur.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./liste-d-utilisateur.component.css']
})
export class ListeDUtilisateurComponent implements OnInit {
 
  faUser=faUserCog
  faSignOutAlt = faSignOutAlt;
  faBeer=faBeer
  faDoor=faReply;
  faDanger=faExclamationTriangle;
  faPlusCircle=faPlusCircle
  faDamier=faTh
  faClient=faUser
  faCheck=faCheck
  faSearch=faSearch
  faTimes=faTimes
  faEye=faToggleOn
  faEye1=faToggleOff;
  faHome=faHome
  faClose=faTimesCircle
  faTools=faTools
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faAdmin = faUserShield;
  faTable=faChair
  damierB: boolean=false
  position: number=0
  posBool: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  posBool2: boolean;
  count: number;
  createB: boolean=false
  child=false;
  page:number=0;
  active: boolean=false
  noActive: boolean=false
  dispoB: boolean=false
  noDispo: boolean=false
  role: number=0
  inputRole:number
  roles=new Array<RoleDto>()
  sorting="rien"
  touche = "";
  recherche: string = "";
  bool = false;
  inputPrix: number;
  utilisateurs= new Array<UtilisateurDto>();
  utilisateurs2= new Array<UtilisateurDto>();
  utilisateur= new UtilisateurDto();
  currentUser: any;
  newRole:String=undefined
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);


  constructor(private router: Router,private utilisateurService: UtilisateurService,private roleService: RoleService,private alertService: AlertService) { }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    
    this.utilisateurService.refreshLaList.subscribe(() => { this.reloadData() });
this.reloadData();
window.scrollTo(0,0)

    
    
  }

  reloadData() {
    this.utilisateurService.getAll(this.page).subscribe(
      co => {
        this.utilisateurs = co;
       
      }
    );

    this.utilisateurService.getAll(this.page+1).subscribe(
      co => {
        this.utilisateurs2 = co;
       
      }
    );

    this.roleService.getAll().subscribe(
      data=>{
        this.roles=data
      }
    )
   
  }

  track(event){
    
    this.position=parseInt(event.path[1].window.scrollY)
    var top:string=(this.position+50).toString()+"px"
      document.getElementById('myModal').style.top=top
      var top:string=(this.position+75).toString()+"px"
        document.getElementById('myModal1').style.top=top
      
  
   
    if(this.position>=180){
     this.posBool=true
     document.getElementById("top").style.backgroundImage= 'none'
      document.getElementById("top").style.display='block'
      document.getElementById("top").style.position='sticky'
      document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
   
     
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("top").style.boxShadow= "none"
       document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=350
     img.height=280
      
       
     }
     
  }
  countM(){
    this.count++
    if(this.count>20){
      
      this.posBool=false
  
    }
  }
  
  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true
  
    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }
  
  create(){
    if(this.createB==false){
      document.getElementById('myModal1').style.display='block'
      this.createB=true
  
    }else{
      document.getElementById('myModal1').style.display='none'
      this.createB=false
      if(this.child){
        window.location.reload()
      }
  
    }
  }
  
  nonActive(){
  
    if(!this.noActive){
            this.noActive=true
            this.active=false
            document.getElementById("noActOn").style.display='block'
            document.getElementById("noActOff").style.display='none'
            document.getElementById("actOn").style.display='none'
            document.getElementById("ActOff").style.display='block'
  
    }else{
            this.noActive=false
            document.getElementById("noActOn").style.display='none'
            document.getElementById("noActOff").style.display='block'
          }
          this.refresh()
  
  }
  
  activeB(){
      if(!this.active){
        this.active=true
        this.noActive=false
        document.getElementById("actOn").style.display='block'
        document.getElementById("ActOff").style.display='none'
        document.getElementById("noActOn").style.display='none'
        document.getElementById("noActOff").style.display='block'
     
     }else{
        this.active=false
        document.getElementById("actOn").style.display='none'
        document.getElementById("ActOff").style.display='block'
     
     }
     this.refresh()
     
  }
  
  admin(){
  
    if(!this.dispoB){
      this.dispoB=true
      this.noDispo=false
      this.role=2
      document.getElementById("DispoOn").style.display='block'
      document.getElementById("DispoOff").style.display='none'
      document.getElementById("noDispoOn").style.display='none'
      document.getElementById("noDispoOff").style.display='block'
  
  }else{
    this.role=0
      this.dispoB=false
      document.getElementById("DispoOn").style.display='none'
      document.getElementById("DispoOff").style.display='block'
  
  }
  this.refresh()
  }
  
  nonAdmin(){
  
        if(!this.noDispo){
                this.noDispo=true
                this.dispoB=false
                this.role=1
                document.getElementById("noDispoOn").style.display='block'
                document.getElementById("noDispoOff").style.display='none'
                document.getElementById("DispoOn").style.display='none'
                document.getElementById("DispoOff").style.display='block'
      
        }else{
          this.role=0
                this.noDispo=false
                document.getElementById("noDispoOn").style.display='none'
                document.getElementById("noDispoOff").style.display='block'
              }
              this.refresh()
      
  }
  activate(event){
      
          this.utilisateurService.getUtilisateur(event).subscribe(
            data=>{
              this.utilisateur=data
              this.utilisateur.actif=true
              this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe(
               data=>{
                 this.refresh()
                 
               }
              )
            }
          )
     
  }
  
  delete(event) {
    this.utilisateurService.getUtilisateur(event).subscribe(
      data=>{
        this.utilisateur=data
        this.utilisateur.actif=false
        this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe(
         data=>{
           this.refresh()
           
         }
        )
      }
    )
  }

  sort(event){
    this.sorting=event
  this.refresh()
  }
  
  roleF(event){
    
    this.currentUser=event
   
  
    this.utilisateurs.forEach(element=>{
      if(element.id!=event){
        document.getElementById("btn"+element.id).style.display='block'
        document.getElementById("inputQte"+element.id).style.display='none'
       
      }else{
        
      }
    })
      
          document.getElementById("btn"+event).style.display='none'
          document.getElementById("inputQte"+event).style.display='block'
   
  
    
  }

  inputRoleF(event){
    this.utilisateurService.getUtilisateur(this.currentUser).subscribe(
      data=>{
        this.utilisateur=data
        this.roles.forEach(element=>{
          if(element.id==event){
            this.utilisateur.role.id=element.id
            this.utilisateur.role.nom=event
          }
        })
        this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe(
          data=>{
            this.utilisateur=data
            this.refresh()
          }
        )
      }
    )
    
  }
  
  findbyname() {
    var textarea =<HTMLInputElement> document.getElementById('search').firstChild
    textarea.addEventListener('keyup', (e) => {
      this.touche = e.key
    });
    if (this.recherche.length === 0 && this.touche === "Backspace" && this.bool === true) {
    } else {
      if (this.recherche.length === 0) {
        this.bool = true;
      } else {
        this.bool = false;
      }
      this.utilisateurService.findbyname(this.recherche,this.page).subscribe(
        donnees => {
          this.utilisateurService.subjectMiseAJour.next(0)
          this.utilisateurs = donnees;
        }
      );
    }
  }
  
  
  // prix(event){
  
  //   this.utilisateurs.forEach(element=>{
  //     if(element.id!=event){
  //       document.getElementById("btnPrix"+element.id).style.display='block'
  //       document.getElementById("inputPrix"+element.id).style.display='none'
  //       document.getElementById("btn"+element.id).style.display='block'
  //       document.getElementById("inputQte"+element.id).style.display='none'
  //     }else{
  //       document.getElementById("btn"+element.id).style.display='block'
  //       document.getElementById("inputQte"+element.id).style.display='none'
  //     }
  //   })
  //     this.utilisateurService.getUtilisateur(event).subscribe(
  //       data=>{
  //         this.utilisateur=data
  //         this.inputPrix=this.utilisateur.nbResa
  //         document.getElementById("btnPrix"+event).style.display='none'
  //         document.getElementById("inputPrix"+event).style.display='block'
  //         var textarea = document.getElementById('inputPrix'+event)
  //     textarea.addEventListener('keyup', (e) => {
  //       if(e.key=="Enter"){
  //         this.utilisateur.nbResa=this.inputPrix
  //         this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe(
  //           data1=>{
  //             document.getElementById("btnPrix"+event).style.display='block'
  //             document.getElementById("inputPrix"+event).style.display='none'
  //               this.refresh()
  //           }
  //         ) 
  //        }
  //     });
  //      })  
  
    
  // }


  
  
      
  
  refresh(){
    this.roleService.refreshList.subscribe(() => {
      this.roleService.getAll() });
    this.roleService.getAll().subscribe(
      data=>{
        this.roles=data

      }
    )
      
  
      
       this.utilisateurService.refreshList.subscribe(() => {
         this.utilisateurService.findbytype(this.page,this.active,this.noActive, this.role, this.sorting) });
      this.utilisateurService.findbytype(this.page,this.active,this.noActive, this.role, this.sorting).subscribe(
         data=>{
           this.utilisateurs=data
          
          
           if(this.utilisateurs.length==0&&this.page>0){
            
             this.page--
             this.utilisateurService.findbytype(this.page,this.active,this.noActive, this.role, this.sorting).subscribe(
               data=>{
                 this.utilisateurs=data
                
               }
             )
  
           }
         }
  
       )  
       this.utilisateurService.findbytype(this.page+1,this.active,this.noActive, this.role, this.sorting).subscribe(
          data=>{
            this.utilisateurs2=data
           }
         )  
    
  
   
  }
  
  pagination(event){
    window.scrollTo(0,180)
    if(event=="precedent"){
      if(this.page>0){
        
        this.page--
        this.refresh()
      }
  
    }else{
      
        this.page++
        this.refresh()
    }
  
  
  }

  emit(event){
    
    this.child=event
  }

  creerRole(event){

    this.newRole=event.target.value
    if(event.key=="Enter"){
      this.validerRole()
    }

  }
  validerRole(){
    if(this.newRole!=undefined){
      this.roleService.create(this.newRole).subscribe(
        data=>{
          window.alert("le role "+this.newRole+" a bien été créé")
          this.newRole=undefined
          this.refresh()
    
        }
      )

    }else{
      window.alert("vous ne pouvez pas créer un role sans nom")
    }
    this.refresh()
  }

  supprimerRole(event){
    this.roles.find(element=>{
      if(element.id==event){
       if(window.confirm("Etes vous sur de supprimer le role "+element.nom)){
         this.roleService.delete(event).subscribe(
           data=>{
             this.refresh()
           }
         )

       }

      }
    })
  }

  // myFunction(id:number) {
  //   if (confirm("Êtes vous sur de vouloir supprimer l'utilisateur "+id)) {
  //     this.deleteUtilisateur(id)
  //   }
  // }

  // deleteUtilisateur(id: number) {
  //   this.utilisateurService.deleteUtilisateur(id)
  //     .subscribe(
  //      data => {
  //       console.log(data);
  //      this.reloadData();
  //      this.alertService.addSuccess("confirmation de la suppression de l'utilisateur "+id)
  //     },
  //     error => {console.log(error)
  //       this.alertService.addDanger("Ooops une erreur s'est produite")
  //     }
  //     );
    
  // }

  // getUtilisateur(id: number) {
  //   console.log(id);
  //   this.utilisateurService.chargementDetail.next(id);
  //   //this.router.navigate(['gestion', id]);
  // }

  // updateUtilisateur(id: number) {
  //   console.log(id);
  //   this.utilisateurService.chargementDetail.next(id);
  //   //this.router.navigate(['update', id]);

  // }
}