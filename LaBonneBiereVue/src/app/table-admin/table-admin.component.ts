import { Component, OnInit } from '@angular/core';
import { TableService } from '../service/table.service';
import { faSignOutAlt,faHome,faUserShield,faTools,faToggleOn,faCheck,faExclamationTriangle, faSearch, faToggleOff,faUserCog,faTh,faTimesCircle, faTimes,faBeer, faChair, faChartLine,faChalkboardTeacher,faReply, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { TableDto } from '../model/table-dto';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table-admin',
  templateUrl: './table-admin.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./table-admin.component.css']
})
export class TableAdminComponent implements OnInit {
  reset: any;
  table=new TableDto()
  tables:Array<TableDto>=new Array<TableDto>()
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);
  faUser=faUserCog
  faSignOutAlt = faSignOutAlt;
  faBeer=faBeer
  faCheck=faCheck
  faTimes=faTimes
  faDoor=faReply;
  faDanger=faExclamationTriangle;
  faPlusCircle=faPlusCircle
  faDamier=faTh
  faSearch=faSearch
  faEye=faToggleOn
  faEye1=faToggleOff;
  faHome=faHome
  faClose=faTimesCircle
  faTools=faTools
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUserShield = faUserShield;
  faTable=faChair
  damierB: boolean=false
  position: number=0
  posBool: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  posBool2: boolean;
  count: number;
  createB: boolean=false
  child=false;
  page:number=0;
  active: boolean=false
  noActive: boolean=false
  dispoB: boolean=false
  noDispo: boolean=false
  qteMin: number
  inputQte=0
  tables2: Array<TableDto>=new Array<TableDto>();
  sorting="rien"
  touche = "";
  recherche: string = "";
  bool = false;
  inputPrix: number;

  constructor(private router: Router,private tableService: TableService){}

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    window.scrollTo(0,0)
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }

    this.tableService.getAll().subscribe(
      data=>{
        this.tables=data
       
      }
    )

    
  }

  resetResa() {
    document.getElementById("CancelResa").style.display='none'
    document.getElementById("CancelResa2").style.display='block'
   
}

confirm(event){
  document.getElementById("CancelResa").style.display='block'
  document.getElementById("CancelResa2").style.display='none'
  if(event=='oui'){
    this.tableService.resetAll(this.objJson.id).subscribe(
      data => {
        this.reset = data;
        
        window.alert("Toutes les reservations ont été rénitialisées");
  
      }
    )

  }

}

track(event){
    
  this.position=parseInt(event.path[1].window.scrollY)
  var top:string=(this.position+50).toString()+"px"
    document.getElementById('myModal').style.top=top
    var top:string=(this.position+200).toString()+"px"
      document.getElementById('myModal1').style.top=top
    

 
  if(this.position>=180){
   this.posBool=true
   
    document.getElementById("top").style.display='block'
    document.getElementById("top").style.position='sticky'
    document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
   document.getElementById("top").style.height='auto'
   var img=<HTMLImageElement>document.getElementById("img")
   img.width=70
   img.height=60
 
   
  }
     if(this.posBool){
      this.countM()
     }
 
   if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
     this.posBool2=true
     this.posBool=true
     this.count=0
     document.getElementById("top").style.boxShadow= "none"
     document.getElementById("top").style.height='auto'
   var img=<HTMLImageElement>document.getElementById("img")
   img.width=350
   img.height=280
    
     
   }
   
}
countM(){
  this.count++
  if(this.count>20){
    
    this.posBool=false

  }
}

damier(event){
  
  if(this.damierB==false){
    document.getElementById('myModal').style.display='block'
    this.damierB=true

  }else{
    document.getElementById('myModal').style.display='none'
    this.damierB=false
  }
  
}

create(){
  if(this.createB==false){
    document.getElementById('myModal1').style.display='block'
    this.createB=true

  }else{
    document.getElementById('myModal1').style.display='none'
    this.createB=false
    if(this.child){
      window.location.reload()
    }

  }
}

nonActive(){

  if(!this.noActive){
          this.noActive=true
          this.active=false
          document.getElementById("noActOn").style.display='block'
          document.getElementById("noActOff").style.display='none'
          document.getElementById("actOn").style.display='none'
          document.getElementById("ActOff").style.display='block'

  }else{
          this.noActive=false
          document.getElementById("noActOn").style.display='none'
          document.getElementById("noActOff").style.display='block'
        }
        this.refresh()

}

activeB(){
    if(!this.active){
      this.active=true
      this.noActive=false
      document.getElementById("actOn").style.display='block'
      document.getElementById("ActOff").style.display='none'
      document.getElementById("noActOn").style.display='none'
      document.getElementById("noActOff").style.display='block'
   
   }else{
      this.active=false
      document.getElementById("actOn").style.display='none'
      document.getElementById("ActOff").style.display='block'
   
   }
   this.refresh()
   
}

dispo(){

  if(!this.dispoB){
    this.dispoB=true
    this.noDispo=false
    document.getElementById("DispoOn").style.display='block'
    document.getElementById("DispoOff").style.display='none'
    document.getElementById("noDispoOn").style.display='none'
    document.getElementById("noDispoOff").style.display='block'

}else{
    this.dispoB=false
    document.getElementById("DispoOn").style.display='none'
    document.getElementById("DispoOff").style.display='block'

}
this.refresh()
}

nonDispo(){

      if(!this.noDispo){
              this.noDispo=true
              this.dispoB=false
              document.getElementById("noDispoOn").style.display='block'
              document.getElementById("noDispoOff").style.display='none'
              document.getElementById("DispoOn").style.display='none'
              document.getElementById("DispoOff").style.display='block'
    
      }else{
              this.noDispo=false
              document.getElementById("noDispoOn").style.display='none'
              document.getElementById("noDispoOff").style.display='block'
            }
            this.refresh()
    
}
activate(event){
    
        this.tableService.getOne(event).subscribe(
          data=>{
            this.table=data
            this.table.active=true
            this.tableService.updateTable(this.table).subscribe(
             data=>{
               this.refresh()
               
             }
            )
          }
        )
   
}

delete(event) {
  this.tableService.getOne(event).subscribe(
    data=>{
      this.table=data
      this.table.active=false
      this.tableService.updateTable(this.table).subscribe(
       data=>{
         this.refresh()
         
       }
      )
    }
  )
}

minimumInputQte(event){
  this.qteMin=event.target.valueAsNumber
  if(this.minimumInputQte!=Number){
    this.qteMin=0
    
  }
  this.refresh()
}

sliderMinQte(event){
  this.qteMin=event.target.valueAsNumber
   this.refresh()
 }

 sort(event){
  this.sorting=event
this.refresh()
}

quantite(event){
 

  this.tables.forEach(element=>{
    if(element.id!=event){
      document.getElementById("btn"+element.id).style.display='block'
      document.getElementById("inputQte"+element.id).style.display='none'
      document.getElementById("btnPrix"+element.id).style.display='block'
      document.getElementById("inputPrix"+element.id).style.display='none'
    }else{
      document.getElementById("btnPrix"+element.id).style.display='block'
      document.getElementById("inputPrix"+element.id).style.display='none'
    }
  })
    this.tableService.getOne(event).subscribe(
      data=>{
        this.table=data
        this.inputQte=this.table.capacite
        document.getElementById("btn"+event).style.display='none'
        document.getElementById("inputQte"+event).style.display='block'
        var textarea = document.getElementById('inputQte'+event)
    textarea.addEventListener('keyup', (e) => {
      if(e.key=="Enter"){
        this.table.capacite=this.inputQte
        this.tableService.updateTable(this.table).subscribe(
          data=>{
            document.getElementById("btn"+event).style.display='block'
            document.getElementById("inputQte"+event).style.display='none'
              this.refresh()            
          }
        ) 
       }
    });
     })  

  
}

findbyname() {
  var textarea =<HTMLInputElement> document.getElementById('search').firstChild
  textarea.addEventListener('keyup', (e) => {
    this.touche = e.key
  });
  if (this.recherche.length === 0 && this.touche === "Backspace" && this.bool === true) {
  } else {
    if (this.recherche.length === 0) {
      this.bool = true;
    } else {
      this.bool = false;
    }
    this.tableService.findbyname(this.recherche).subscribe(
      donnees => {
        this.tableService.subjectMiseAJour.next(0)
        this.tables = donnees;
      }
    );
  }
}


prix(event){

  this.tables.forEach(element=>{
    if(element.id!=event){
      document.getElementById("btnPrix"+element.id).style.display='block'
      document.getElementById("inputPrix"+element.id).style.display='none'
      document.getElementById("btn"+element.id).style.display='block'
      document.getElementById("inputQte"+element.id).style.display='none'
    }else{
      document.getElementById("btn"+element.id).style.display='block'
      document.getElementById("inputQte"+element.id).style.display='none'
    }
  })
    this.tableService.getOne(event).subscribe(
      data=>{
        this.table=data
        this.inputPrix=this.table.nbResa
        document.getElementById("btnPrix"+event).style.display='none'
        document.getElementById("inputPrix"+event).style.display='block'
        var textarea = document.getElementById('inputPrix'+event)
    textarea.addEventListener('keyup', (e) => {
      if(e.key=="Enter"){
        this.table.nbResa=this.inputPrix
        this.tableService.updateTable(this.table).subscribe(
          data1=>{
            document.getElementById("btnPrix"+event).style.display='block'
            document.getElementById("inputPrix"+event).style.display='none'
              this.refresh()
          }
        ) 
       }
    });
     })  

  
}

    

refresh(){
    

    
     this.tableService.refreshList.subscribe(() => {
       this.tableService.findbytype(this.active,this.noActive, this.qteMin, this.sorting, this.dispoB, this.noDispo) });
    this.tableService.findbytype(this.active,this.noActive, this.qteMin, this.sorting, this.dispoB, this.noDispo).subscribe(
       data=>{
         this.tables=data       
       }

     )   
}



emit(event){
    
  this.child=event
}

}
