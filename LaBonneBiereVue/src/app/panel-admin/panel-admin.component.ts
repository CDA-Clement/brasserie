import { Component, OnInit } from '@angular/core';
import { TableService } from '../service/table.service';
import { faSignOutAlt,faHome,faUserShield,faTools,faUserCog,faTh, faBeer, faChair, faChartLine,faChalkboardTeacher,faReply } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-admin',
  templateUrl: './panel-admin.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./panel-admin.component.css']
})
export class PanelAdminComponent implements OnInit {
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);
  reset: any;
  faUser=faUserCog
  faSignOutAlt = faSignOutAlt;
  faBeer=faBeer
  faDoor=faReply;
  faDamier=faTh
  faHome=faHome
  faTools=faTools
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUserShield = faUserShield;
  faTable=faChair
  damierB: boolean=false
  position: number=0
  posBool: boolean=false
  posBool2: boolean;
  count: number;


  constructor(private router: Router,private tableService: TableService) { }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
      window.scrollTo(0,0)
    }

  }

  public resetResa() {
        this.tableService.resetAll(this.objJson.id).subscribe(
          data => {
            this.reset = data;
          }
        )
  }

  track(event){
    
    this.position=parseInt(event.path[1].window.scrollY)
    var top:string=(this.position+50).toString()+"px"
      document.getElementById('myModal').style.top=top
      

   
    if(this.position>=180){
     this.posBool=true
     document.getElementById("top").style.backgroundImage= 'none'
      document.getElementById("top").style.display='block'
      document.getElementById("top").style.position='sticky'
      document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
   
     
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("top").style.boxShadow= "none"
       document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=350
     img.height=280
      
       
     }
     
  }
  countM(){
    this.count++
    if(this.count>20){
      
      this.posBool=false

    }
  }

  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true

    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }
}
