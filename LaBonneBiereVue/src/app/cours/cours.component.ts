import { Component, OnInit } from '@angular/core';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.css']
})
export class CoursComponent implements OnInit {

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }else{
      this.alertService.addPrimary('Veuillez saisir votre age')
      document.getElementById('id104').style.display = 'block';
    }
  }

}
