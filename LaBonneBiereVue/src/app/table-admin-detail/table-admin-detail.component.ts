import { Component, OnInit } from '@angular/core';
import { faTh,faSearch, faTrash,faBeer,faHome, faEdit, faPlus,faReply,faUserCog, faChair, faChartLine,faChalkboardTeacher, faTools} from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';
import { FileService } from '../service/file.service';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { TableDto } from '../model/table-dto';
import { TableService } from '../service/table.service';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';

@Component({
  selector: 'app-table-admin-detail',
  templateUrl: './table-admin-detail.component.html',
  host: {'(window:scroll)': 'track($event)','(window:mousemove)': 'move($event)'},
  styleUrls: ['./table-admin-detail.component.css']
})
export class TableAdminDetailComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  utilisateur=new UtilisateurDto()
  reservant=new UtilisateurDto()
  table=new TableDto()
  idBiereParam: any;
  idB: string;
  faReply=faReply
  faDamier=faTh
  faEdit = faEdit;
  faPlus=faPlus
  faTools=faTools
  faHome=faHome
  faTrash = faTrash;
  faBeer=faBeer
  faSearch = faSearch;
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUser = faUserCog;
  faTable=faChair
  position: number=0
  posBool: boolean=false
  posBool2: boolean;
  count: number;
  damierB: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  selectedFiles: FileList;
  currentFile: File;
  nom=""
  moveB: boolean=false
  supprimable=false;
  resaAdmin: boolean=false

  constructor(private route: ActivatedRoute, private router: Router,private tableService: TableService, private utilisateurService: UtilisateurService, private fileService:FileService) { }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    window.scrollTo(0,0)
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }
    this.route.queryParams
      .subscribe(params => {
        this.idBiereParam = params;
        
      });
    var strId = Object.values(this.idBiereParam).toString()
    var id = parseInt(strId, 10)    
        this.tableService.getOne(id)
          .subscribe(data => {
            this.table = data;
            this.nom=this.table.nom
            if(this.table.reservationEnCours){
              this.resaAdmin=true
              this.utilisateurService.getUtilisateur(this.table.utilisateur.id).subscribe(
                data=>{
                  this.reservant=data
                }
              )
            }
          }, 
          );
          window.scrollTo(0, 0)

          this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
            data=>{
              this.utilisateur=data
            }
          )

         
      
  }

  track(event){
    
    this.position=parseInt(event.path[1].window.scrollY)
    var top:string=(this.position+50).toString()+"px"
      document.getElementById('myModal').style.top=top

   
    if(this.position>=80){
     this.posBool=true
     document.getElementById("top").style.backgroundImage= 'none'
      document.getElementById("top").style.display='block'
      document.getElementById("top").style.position='sticky'
      document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
   
     
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("top").style.boxShadow= "none"
       document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=350
     img.height=280
       
       
     }
     
  }

  move(event){
    if(!this.moveB){
      document.getElementById('button').addEventListener('click', () => {
        document.getElementById('fileInput').click()
      })

      
      this.moveB=true
    }

     
  }

  countM(){
    this.count++
    if(this.count>5){
      
      this.posBool=false

    }
  }
  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true

    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }

  edit(event){
    if(event=='nom'){
      document.getElementById('inputNom').style.display='block'
      document.getElementById('idN').style.display='none'

    }
    if(event=='disponibilite'){
      if(this.table.reservationEnCours){
        this.table.reservationEnCours=false
        this.table.utilisateur=null
        this.resaAdmin=false
      }else if(!this.table.reservationEnCours){
        this.table.reservationEnCours=true
        this.table.utilisateur=this.utilisateur
        this.resaAdmin=true
      }
      this.refresh()

    }

    if(event=='description'){
      document.getElementById('inputDescription').style.display='block'
      document.getElementById('idD').style.display='none'

    }

    if(event=='capacite'){
      document.getElementById('inputDegre').style.display='block'
      document.getElementById('idDe').style.display='none'

    }

    if(event=='reservation'){
      document.getElementById('inputQuantite').style.display='block'
      document.getElementById('idQ').style.display='none'

    }
    
  }

  inputNom(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputNom').style.display='none'
      
      document.getElementById('idN').style.display='block'
      document.getElementById('idN').style.marginTop="2%"
        this.refresh()
         }
  }

  inputDescription(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputDescription').style.display='none'
      
      document.getElementById('idD').style.display='block'
      document.getElementById('idD').style.marginTop="2%"
        this.refresh()
         }
  }

  inputDegre(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputDegre').style.display='none'
      
      document.getElementById('idDe').style.display='block'
      document.getElementById('idDe').style.marginTop="2%"
        this.refresh()
         }
  }

 

  inputQuantite(event){
    if(event.key=="Enter"){
      document.getElementById('inputQuantite').style.display='none'
      document.getElementById('idQ').style.display='block'
      document.getElementById('idQ').style.marginTop="2%"
        this.refresh()
         }
  }

  activer(event){
    if(event=="desactiver"){
      this.table.active=false
    }else{
      this.table.active=true
    }
    this.refresh()
  }

  refresh(){
    this.tableService.refreshList.subscribe(() => {
      this.tableService.updateTable(this.table) });
    this.tableService.updateTable(this.table).subscribe(
      data=>{
        this.tableService.getOne(this.table.id)
  }
    )
}

selectFile(event) {
  this.selectedFiles = event.target.files;
  this.currentFile = this.selectedFiles.item(0);
   this.fileService.modifFileTable(this.currentFile,this.nom).subscribe(response => {
		
     if (response instanceof HttpResponse) {	  
      }
      window.location.reload()
    });    
  
}

supprimer(){
  this.tableService.delete(this.table.id).subscribe(
    data=>{
      this.router.navigateByUrl("/gestion/tables")
    }
  )
}

}
