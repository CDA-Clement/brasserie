import { Component, OnInit, Input } from '@angular/core';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gestion-mon-compte-detail',
  templateUrl: './gestion-mon-compte-detail.component.html',
  styleUrls: ['./gestion-mon-compte-detail.component.css']
})
export class GestionMonCompteDetailComponent implements OnInit {
  @Input() idParent;
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  utilisateur: UtilisateurDto;
  age: number;

  constructor(private router: Router,private utilisateurService: UtilisateurService) { }

  ngOnInit() {
    window.scrollTo(0,0)
    if (localStorage.getItem('current_user') == null) {
      this.router.navigateByUrl("/login")
    }
    

    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      util => {
        this.utilisateur = util;
        var timeDiff = Date.now() - new Date(this.utilisateur.dateAge).getTime();
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

      }
    );
  }
  }
