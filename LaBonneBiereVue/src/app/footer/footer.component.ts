import { Component, OnInit } from '@angular/core';
import { faInstagram, faFacebookSquare, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';
import { faCopyright } from '@fortawesome/free-regular-svg-icons'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  faInstagram = faInstagram;
  faFacebook = faFacebookSquare;
  faTwitter = faTwitterSquare;
  faCopyright = faCopyright;

  constructor() { }

  ngOnInit() {
  }

}
