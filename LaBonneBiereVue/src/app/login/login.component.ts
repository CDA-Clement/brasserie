import { Component, OnInit, Inject } from '@angular/core';
import { UtilisateurAuthDto } from '../model/utilisateur-auth-dto';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faUser = faUser;
  faLock = faLock;
  user: UtilisateurAuthDto;
  direction: String;
  

  constructor(private authService: AuthService, private router: Router, private alertService: AlertService) { }

  ngOnInit() {
    document.getElementById('margin').style.display = 'none';
    document.getElementById('id104').style.display = 'none';
    document.getElementById("general").style.webkitFilter = 'blur(0px)';
    window.scrollTo(0,0)
    document.getElementById('footerID').style.display="block"; 
    this.direction = localStorage.getItem('origine');

    
    this.user = new UtilisateurAuthDto();
    this.user.username = '';
    this.user.password = '';
    

  }

  login() {
    this.authService.login(this.user).subscribe(res => {
      if (res) {
        localStorage.setItem("loginTime",new Date().getHours().toString())
        this.alertService.addSuccess('bienvenue '+this.user.username);
        localStorage.setItem("age", '18');
        // login ok
        if(this.direction!=undefined){
          this.router.navigateByUrl('/' + this.direction)

        }else{
          this.router.navigateByUrl('/accueil')
        }

      }
    });
  }

  modalLoginClose() {
    document.getElementById("general").style.webkitFilter = 'blur(17px)';
    if(this.direction!=undefined){
      this.router.navigateByUrl('/' + this.direction)

    }else{
      this.router.navigateByUrl('/accueil')
    }
  }
  signUp(){
    this.router.navigateByUrl('/signup')
  }
}