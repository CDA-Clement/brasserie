import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { faCircle, faShoppingCart, faPlus, faMinus, faQuoteRight, faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

import { CommandeService } from 'src/app/service/commande.service';
import { CommandeDTO } from 'src/app/model/commande-dto';
import { UtilisateurService } from 'src/app/service/utilisateur.service';
import { UtilisateurDto } from 'src/app/model/utilisateur-dto';
import { AlertService } from 'src/app/service/alert.service';
import { BiereDto } from '../model/biere-dto';
import { BiereService } from '../service/biere.service';
import { TypeDeBiereDto } from '../model/type-de-biere-dto';
import { element } from 'protractor';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-biere-detail',
  templateUrl: './biere-detail.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./biere-detail.component.css']
})
export class BiereDetailComponent implements OnInit {
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);
  faCircle = faCircle;
  faShoppingCart = faShoppingCart;
  biere: BiereDto;
  bieres: BiereDto[];
  typeDeBiere: TypeDeBiereDto
  id: any;
  commande: CommandeDTO[];
  temp: BiereDto;
  quant: number=0;
  faPlus = faPlus;
  faMinus = faMinus;
  total:number=0;
  nbclic:number=0;
  url:String=environment.backSchema+"://"+environment.backServer
  position: number;
  chevronRight=faChevronRight
  chevronLeft=faChevronLeft
  faQuote=faQuoteRight;
  logoB=false;
  bool=false
  origine: string='detail';
  utilisateur: UtilisateurDto= new UtilisateurDto();
  constructor(private utilisateurService:UtilisateurService,private route: ActivatedRoute, private biereService: BiereService, private commandeService: CommandeService,private alertService: AlertService) {
    this.biere = new BiereDto();
    this.biere.typeDeBiereDto = new TypeDeBiereDto();
    this.quant = 0;
    
  }

  ngOnInit() {
    window.scrollTo(0,0)
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }else{
      document.getElementById('id104').style.display = 'block';
    }  
    window.scrollTo(0,0)
    
    document.getElementById('footerID').style.display="block";

    if(this.objJson != null){
      this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
        data1 => {
          
          this.utilisateurService.getUtilisateur(this.objJson.id)
          .subscribe(data => {
            
            this.utilisateur = data;
          });
        });
      }
      this.route.paramMap.subscribe(res => {
        this.id = res.get('id');
        localStorage.setItem("origine", this.origine+'/'+this.id);
        
        this.biereService.getOne(this.id).subscribe(
          donnees => {
            this.biere = donnees;
          })
        })
        if(this.objJson!=null){
          this.commandeService.getCommande(this.objJson.nom).subscribe(
            data1=>{
              this.commande=data1;
              this.route.paramMap.subscribe(res => {
                this.id = res.get('id');
                this.biereService.refreshList.subscribe(() => {
                  this.biereService.getOne(this.id) });
                this.biereService.getOne(this.id).subscribe(
                  donnees => {
                    this.biere = donnees;
      
                    if(this.commande.length>0){
                      this.commande.forEach(element1 => {
                        if(this.biere.nom===element1.nom){
                          this.biere.commande=element1.commande
                        }
                    
                      });
      
                    }
                    
          
                  }
                  );
                });
                 
                document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
                document.getElementById('myModal1').style.display='none'
            }
          );
        }
     
  }

  logo(){
    if(!this.logoB){
      this.logoB=true
    }else{
      this.logoB=false;
    }

  }

  input(event){
    document.getElementById('validerModal').style.backgroundColor='green'
    document.getElementById('valider').style.backgroundColor='green'
    
  }
   

  valider(event){
    this.flou()
    document.getElementById("validerModal").style.backgroundColor='rgb(255, 196, 0)'
    document.getElementById("valider").style.backgroundColor='rgb(255, 196, 0)'
  }
  


  emit(event){
  if(this.objJson!=null){
   
    this.commandeService.getCommande(this.objJson.nom).subscribe(
      data1=>{
        this.commande=data1;
        this.route.paramMap.subscribe(res => {
          this.id = res.get('id');
          this.biereService.refreshList.subscribe(() => {
            this.biereService.getOne(this.id) });
          this.biereService.getOne(this.id).subscribe(
            donnees => {
              this.biere = donnees;

              if(this.commande.length>0){
                localStorage.setItem("commande",'1')
                this.commande.forEach(element1 => {
                  if(this.biere.nom===element1.nom){
                    this.biere.commande=element1.commande
                  }
              
                });

              }
              
    
            }
            );
          });
           
          document.getElementById('id02').style.webkitFilter = 'blur(0px)' 
          document.getElementById('myModal1').style.display='none'
      }
    );

  }else{
    
    this.alertService.addPrimary('veuillez vous connecter pour commander')
  }
  }

  track(event){
    
    
    this.position=parseInt(event.path[1].window.scrollY)
    if(this.position>=199&&!this.bool){
      this.bool=true;
      document.getElementById("info").style.display="none"
      document.getElementById("modal").style.display='block'
    }else if(this.position<=450&&this.bool){
      this.bool=false;
      document.getElementById("modal").style.display='none'
      document.getElementById("info").style.display="block"
    }
  }

  flou(){
    if(this.user!=null){
      document.getElementById('id02').style.webkitFilter = 'blur(17px)'
      var top= this.position+window.innerHeight/2.8
    
      document.getElementById('myModal1').style.top=top.toString()+'px'
      document.getElementById('myModal1').style.display='block'
  
    }
  }


}
