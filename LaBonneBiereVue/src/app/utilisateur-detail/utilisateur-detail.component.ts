import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { UtilisateurService } from '../service/utilisateur.service';
import { RoleDto } from '../model/role-dto';
import { RoleService } from '../service/role.service';
import { faTh,faSearch, faTrash,faBeer, faEdit,faHome, faPlus,faReply, faUserShield, faUser,faUserCog,faUnlockAlt, faChair, faChartLine,faChalkboardTeacher, faTools} from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-utilisateur-detail',
  templateUrl: './utilisateur-detail.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./utilisateur-detail.component.css']
})
export class UtilisateurDetailComponent implements OnInit {
  utilisateur= new UtilisateurDto()
  role: RoleDto= new RoleDto()
  idUserParam: any;
  idU: string;
  faEdit = faEdit;
  faReply=faReply
  faDamier=faTh
  faPlus=faPlus
  faPass=faUnlockAlt
  faTools=faTools
  faHome=faHome
  faTrash = faTrash;
  faBeer=faBeer
  faSearch = faSearch;
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUser = faUserCog;
  faAdmin = faUserShield;
  faUser1=faUser
  faTable=faChair
  position: number=0
  posBool: boolean=false
  posBool2: boolean;
  count: number;
  damierB: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  nom=""
  moveB: boolean=false
  supprimable=false;
  dateAge: string;
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);
 

  constructor(private route: ActivatedRoute, private router: Router,
    private utilisateurService: UtilisateurService, private roleService: RoleService) { }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    window.scrollTo(0,0)
    this.utilisateur = new UtilisateurDto();
    this.utilisateur.role = new RoleDto();
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }

    this.route.queryParams
    .subscribe(params => {
     
      
      this.idUserParam = params;
      
    });
    var strId=Object.values(this.idUserParam).toString()
    var id =parseInt(strId, 10)
      this.utilisateurService.getUtilisateur(id)
        .subscribe(data => {
          this.utilisateur = data;
          this.dateAge=this.utilisateur.dateAge.toString().substring(0,10)
        },);
   
  }

  track(event){
    
    this.position=parseInt(event.path[1].window.scrollY)
    var top:string=(this.position+50).toString()+"px"
      document.getElementById('myModal').style.top=top

   
    if(this.position>=80){
     this.posBool=true
     document.getElementById("top").style.backgroundImage= 'none'
      document.getElementById("top").style.display='block'
      document.getElementById("top").style.position='sticky'
      document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
   
     
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("top").style.boxShadow= "none"
       document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=350
     img.height=280
       
       
     }
     
  }


  countM(){
    this.count++
    if(this.count>5){
      
      this.posBool=false

    }
  }
  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true

    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }

  edit(event){
    if(event=='nom'){
      document.getElementById('inputNom').style.display='block'
      document.getElementById('idN').style.display='none'

    }

    if(event=='mail'){
      document.getElementById('inputMail').style.display='block'
      document.getElementById('idM').style.display='none'

    }
    

    if(event=='description'){
      document.getElementById('inputDescription').style.display='block'
      document.getElementById('idD').style.display='none'

    }

    if(event=='capacite'){
      document.getElementById('inputDegre').style.display='block'
      document.getElementById('idDe').style.display='none'

    }

    if(event=='reservation'){
      document.getElementById('inputQuantite').style.display='block'
      document.getElementById('idQ').style.display='none'

    }
    
  }

  inputNom(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputNom').style.display='none'
      
      document.getElementById('idN').style.display='block'
      document.getElementById('idN').style.marginTop="2%"
        this.refresh()
         }
  }

  inputMail(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputMail').style.display='none'
      
      document.getElementById('idM').style.display='block'
      document.getElementById('idM').style.marginTop="2%"
        this.refresh()
         }
  }

  inputDescription(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputDescription').style.display='none'
      
      document.getElementById('idD').style.display='block'
      document.getElementById('idD').style.marginTop="2%"
        this.refresh()
         }
  }

  inputDegre(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputDegre').style.display='none'
      
      document.getElementById('idDe').style.display='block'
      document.getElementById('idDe').style.marginTop="2%"
        this.refresh()
         }
  }

 

  inputQuantite(event){
    if(event.key=="Enter"){
      document.getElementById('inputQuantite').style.display='none'
      document.getElementById('idQ').style.display='block'
      document.getElementById('idQ').style.marginTop="2%"
        this.refresh()
         }
  }

  activer(event){
    if(event=="desactiver"){
      this.utilisateur.actif=false
    }else{
      this.utilisateur.actif=true
    }
    this.refresh()
  }

  resetPassword(){
    if(window.confirm("Etes vous sur de vouloir reset le mot de passe")){
      this.utilisateur.password="temporaire"
      this.refresh()    

    }
   
  }

  refresh(){
    this.utilisateurService.refreshList.subscribe(() => {
      this.utilisateurService.updateUtilisateur(this.utilisateur) });
    this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe(
      data=>{
        this.utilisateurService.getUtilisateur(this.utilisateur.id).subscribe(
          data=>{
            this.utilisateur=data
            this.dateAge=this.utilisateur.dateAge.toString().substring(0,10)
          }
        )
        
  }
    )
}



supprimer(){
  this.utilisateurService.deleteUtilisateur(this.utilisateur.id).subscribe(
    data=>{
      this.router.navigateByUrl("/gestion/users")
    }
  )
}
}