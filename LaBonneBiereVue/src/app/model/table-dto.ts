import { UtilisateurDto } from './utilisateur-dto';

export class TableDto {
    id: number;
    nom: string;
    reservationEnCours: boolean;
    utilisateur: UtilisateurDto;
    capacite: number;
    heure: number;
    description: string;
    active:boolean;
    nbResa:number;

    constructor(id?: number, nom?: string, reservationEnCours?: boolean, utilisateur?: UtilisateurDto, capacite?: number, description?: string, heure?: number, active?:boolean, nbResa?:number) {
        this.id = id;
        this.nom = nom;
        this.reservationEnCours = reservationEnCours;
        this.utilisateur = utilisateur;
        this.capacite = capacite;
        this.heure = heure;
        this.description = description
        this.active=active
        this.nbResa=nbResa
    }
}
