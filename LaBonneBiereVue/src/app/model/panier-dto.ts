import { BiereDto } from './biere-dto';


export class PanierDto {

    id: number;
    nom: string;
    listDeBiere: Array<BiereDto>;

    constructor(id?: number, nom?: string, listDeBiere?: Array<BiereDto>) {
        this.id = id;
        this.nom = nom;
        this.listDeBiere = listDeBiere;
    }
}
