import { TypeDeBiereDto } from './type-de-biere-dto';


export class BiereDto {
    id: number;
    nom: string;
    description: string;
    degre: number;
    prix: number;
    quantite: number;
    typeDeBiereDto: TypeDeBiereDto;
    commande:number;
    active:Boolean;
    prixTotal:number;
    

    constructor(id?: number, nom?: string, description?: string, degre?: number, prix?: number, quantite?: number, typeDeBiere?: TypeDeBiereDto,active?:Boolean) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.degre = degre;
        this.prix = prix;
        this.quantite = quantite;
        this.typeDeBiereDto = typeDeBiere;
        this.commande=0;
        this.active=active
        this.prixTotal=0;
        

    }
}
