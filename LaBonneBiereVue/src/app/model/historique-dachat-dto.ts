import { BiereDto } from './biere-dto';


export class HistoriqueDAchatDto {
    id: number;
    idUser:number;
    dateAchat: Date;
    prixTotal: number;
    biere:String;
    idBiere:number;
    typeDebiere:String;
    listDeBiere: Array<BiereDto>;
    dateAchatString: string;

    constructor(id?: number,idUser?:number, dateAchat?: Date, prixTotal?: number,biere?:String, idBiere?:number,typeDebiere?:String, listDeBiere?: Array<BiereDto>) {
        this.id = id;
        this.idUser=idUser;
        this.dateAchat = dateAchat;
        this.dateAchatString=""
        this.prixTotal = prixTotal;
        this.biere=biere;
        this.idBiere=idBiere;
        this.typeDebiere=typeDebiere;
        this.listDeBiere=listDeBiere;
    }
}
