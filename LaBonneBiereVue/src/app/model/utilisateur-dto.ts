import { PanierDto } from './panier-dto';
import { HistoriqueDAchatDto } from './historique-dachat-dto';
import { RoleDto } from './role-dto';

export class UtilisateurDto {
    id: number;
    nom: string;
    prenom: string;
    username: string;
    password: string;
    adresseMail: string;
    dateAge: Date;
    panier: PanierDto;
    role: RoleDto;
    listHistoriqueAchat: HistoriqueDAchatDto;
    reservation: boolean;
    actif: boolean;

    constructor(id?: number, nom?: string, prenom?: string, username?: string,
        password?: string, adresseMail?: string, dateAge?: Date, panier?: PanierDto,
        role?: RoleDto, listHistoriqueAchat?: HistoriqueDAchatDto, reservation?: boolean, actif?: boolean) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.adresseMail = adresseMail;
        this.dateAge = dateAge;
        this.panier = panier;
        this.role = role;
        this.listHistoriqueAchat = listHistoriqueAchat;
        this.reservation = reservation;
        this.actif = actif;

    }
}
