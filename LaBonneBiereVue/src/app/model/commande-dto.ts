import { UtilisateurDto } from './utilisateur-dto';

export class CommandeDTO {
	id: number;
	nom: string;
	total: number;
	commande: number;
	utilisateur: UtilisateurDto;
	idBiere:number;
	prixUnitaire:number;
	constructor(id?: number, nom?: string, total?: number, commande?: number, utilisateur?: UtilisateurDto,idBiere?:number,prixUnitaire?:number) {
		this.id = id;
		this.nom = nom;
		this.total = total;
		this.commande = commande;
		this.utilisateur = utilisateur;
		this.idBiere=idBiere;
		this.prixUnitaire=prixUnitaire;
	}
}
