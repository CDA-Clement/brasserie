export class AlertDto {
    type: string;
    message: string;
    closed: boolean;

    constructor() { }
}