import { BiereDto } from './biere-dto';

export class CommentaireDto {
    id: number;
    nom: string;
    message: string;
    biere: BiereDto;
    date: Date;
    constructor(id?: number, nom?: string, message?: string, biere?: BiereDto, date?: Date) {
        this.id = id;
        this.nom = nom;
        this.message = message;
        this.biere = biere;
        this.date = date;
    }
}
