import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { BiereDto } from '../model/biere-dto';
import { Router, ActivatedRoute } from '@angular/router';
import { BiereService } from '../service/biere.service';
import { TypeDeBiereDto } from '../model/type-de-biere-dto';
import { TypeDeBiereService } from '../service/type-de-biere.service';
import { Observable } from 'rxjs';
import { AlertService } from '../service/alert.service';
import { FileService } from '../service/file.service';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-biere-create',
  templateUrl: './biere-create.component.html',
  styleUrls: ['./biere-create.component.css']
})
export class BiereCreateComponent implements OnInit {
  biere: BiereDto = new BiereDto();
  submitted = false;
  typedebieres: Observable<any>;
  selectedFiles: FileList;
  currentFile: File;
  currentFile2: File;
  nom:String=""
  fichierSelection:boolean=false;
  @Output()parent=new EventEmitter<Boolean>()
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  

  constructor(private typedebiereService: TypeDeBiereService, 
    private route: ActivatedRoute, 
    private biereService: BiereService, 
    private router: Router,
    private alertService: AlertService,
    private fileService:FileService) {
    this.biere.typeDeBiereDto = new TypeDeBiereDto();
    
  }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }
    this.typedebiereService.getAll().subscribe(
      types => {
        this.typedebieres = types;
      }
    );
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.fichierSelection=true;
  }
  
  upload() {
    
    this.currentFile = this.selectedFiles.item(0);
    this.currentFile2 = this.selectedFiles.item(1);
     this.fileService.uploadFile(this.currentFile,this.currentFile2,this.nom).subscribe(response => {
		
      if (response instanceof HttpResponse) {
		 
       }	  
     });    
      
  }

  onSubmitUtil() {
    
    var confirmee:boolean
    
    this.nom=this.biere.nom
    
    if(this.biere.nom==undefined||
      this.biere.degre==undefined||
      this.biere.description==undefined||
      this.biere.prix==undefined||
      this.biere.quantite==undefined||
      this.biere.typeDeBiereDto==undefined){
        this.alertService.addPrimary("il manque un ou plusieurs champs")
      }else{
        if(!this.fichierSelection){
         
          if(window.confirm("etes vous sur d'ajouter un biere sans image")){
            confirmee=true
          }
          if(confirmee){
            
            this.biereService.createBiere(this.biere)
            .subscribe(
              donnees => {
                this.biereService.subjectMiseAJour.next(0);
                
                this.alertService.addSuccess("la bière "+this.biere.nom+" à bien été créée");
                (<HTMLFormElement>document.getElementById("form")).reset();
                
              },
              err=>{
                
              }
              
              )
              
            this.upload()

          }
          else{
            
          }

          }else{
            
            this.biereService.createBiere(this.biere)
            .subscribe(
              donnees => {
                this.biereService.subjectMiseAJour.next(0);
                this.alertService.addSuccess("la bière "+this.biere.nom+" a bien été créée");
                (<HTMLFormElement>document.getElementById("form")).reset();
                this.parent.emit(true)
                
              },
              err=>{
                
              }
              
              )
              this.upload()
        }

  }
  this.fichierSelection=false;
  
}



}
