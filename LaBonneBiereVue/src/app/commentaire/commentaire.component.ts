import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import { CommentaireDto } from 'src/app/model/commentaire-dto';
import { BiereDto } from 'src/app/model/biere-dto';
import { TypeDeBiereDto } from 'src/app/model/type-de-biere-dto';
import { CommentaireService } from 'src/app/service/commentaire.service';

@Component({
  selector: 'app-commentaire',
  templateUrl: './commentaire.component.html',
  styleUrls: ['./commentaire.component.css']
})
export class CommentaireComponent implements OnInit {
  commentaireText: any;
  commentaires= new Array<CommentaireDto>();
  commentaire: any;
  id;
  biere: BiereDto;
  typeDebiere: TypeDeBiereDto;
  isConnected: boolean;
  isAdmin: boolean;
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  faTrash = faTrash;
  faEdit = faEdit;

  constructor(private router: Router, private authService: AuthService, private route: ActivatedRoute, private commentaireService: CommentaireService) {
    this.biere = new BiereDto();
    this.biere.typeDeBiereDto = new TypeDeBiereDto();
    this.commentaire = new CommentaireDto();
  }

  ngOnInit() {
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }
    this.reloadData();
    this.route.paramMap.subscribe(res => {
      this.id = res.get('id');
      this.commentaireService.getAllByBiere(this.id).subscribe(
        res => {
          this.commentaires = res;
        }
      )
    });


  }
  reloadData() {
    this.isConnected = this.authService.isConnected();
    if (this.authService.getCurrentUser()) {
      this.isAdmin = this.authService.getCurrentUser().role.nom === "Admin";
    }
    this.authService.subjectConnexion.subscribe(
      res => {
        this.isConnected = this.authService.isConnected();
        if (this.authService.getCurrentUser()) {
          this.isAdmin = this.authService.getCurrentUser().role.nom === "Admin";
        }

      }
    )
  }

  delete(idCom: number) {
    this.commentaireService.deletecommentaire(idCom)
      .subscribe(
        data => {
          this.reloadData();
          this.ngOnInit();
        },
        );
  }

  addComment() {
    this.commentaire.nom = this.objJson.nom;
    this.commentaire.message = this.commentaireText;
    this.commentaire.biere = this.biere.id;
    this.commentaire.date = new Date();
    this.commentaireService.createcommentaire(this.id, this.commentaire).subscribe(() => {
      this.commentaireText=""
      this.reloadData();
      this.ngOnInit();
    })
  }

}