import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { TopnavbarComponent } from './topnavbar/topnavbar.component';
import { AccueilComponent } from './accueil/accueil.component';
import { SignupComponent } from './signup/signup.component';
import { ListeDUtilisateurComponent } from './liste-d-utilisateur/liste-d-utilisateur.component';
import { ListeDeBiereComponent } from './liste-de-biere/liste-de-biere.component';
import { UtilisateurDetailComponent } from './utilisateur-detail/utilisateur-detail.component';
import { UtilisateurUpdateComponent } from './utilisateur-update/utilisateur-update.component';
import { UtilisateurCreateComponent } from './utilisateur-create/utilisateur-create.component';
import { LoginComponent } from './login/login.component';
import { PanelAdminComponent } from './panel-admin/panel-admin.component';
import { BiereAdminComponent } from './biere-admin/biere-admin.component';
import { BiereCreateComponent } from './biere-create/biere-create.component';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { BiereAdminDetailComponent } from './biere-admin-detail/biere-admin-detail.component';
import { ModalageComponent } from './modalage/modalage.component';

import { GestionMonCompteUtilisateurComponent } from './gestion-mon-compte-utilisateur/gestion-mon-compte-utilisateur.component';
import { TableComponent } from './table/table.component';
import { GestionMonCompteDetailComponent } from './gestion-mon-compte-detail/gestion-mon-compte-detail.component';
import { GestionMonCompteUpdateComponent } from './gestion-mon-compte-update/gestion-mon-compte-update.component';
import { GestionMonCompteEditPasswordComponent } from './gestion-mon-compte-edit-password/gestion-mon-compte-edit-password.component';

import { AlertComponent } from './alert/alert.component';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { PanierComponent } from './panier/panier.component';
import { ValidationComponent } from './validation/validation.component';
import { FactureComponent } from './facture/facture.component';
import { RechercheFactureComponent } from './recherche-facture/recherche-facture.component';
import { TableAdminComponent } from './table-admin/table-admin.component';
import { TableAdminDetailComponent } from './table-admin-detail/table-admin-detail.component';
import { TableCreateComponent } from './table-create/table-create.component';
import { BiereDetailComponent } from './biere-detail/biere-detail.component';
import { CommentaireComponent } from './commentaire/commentaire.component';
import { CoursComponent } from './cours/cours.component';
import { CoursAdminComponent } from './cours-admin/cours-admin.component';




@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    TopnavbarComponent,
    AccueilComponent,
    FactureComponent,
    SignupComponent,
    ListeDUtilisateurComponent,
    UtilisateurDetailComponent,
    UtilisateurUpdateComponent,
    UtilisateurCreateComponent,
    LoginComponent,
    PanelAdminComponent,
    BiereAdminComponent,
    BiereDetailComponent,
    BiereCreateComponent,
    ListeDeBiereComponent,
    BiereAdminDetailComponent,
    ModalageComponent,
    GestionMonCompteUtilisateurComponent,
    TableComponent,
    GestionMonCompteDetailComponent,
    GestionMonCompteUpdateComponent,
    GestionMonCompteEditPasswordComponent,
    AlertComponent,
    PanierComponent,
    CommentaireComponent,
    ValidationComponent,
    FactureComponent,
    RechercheFactureComponent,
    TableAdminComponent,
    TableAdminDetailComponent,
    TableCreateComponent,
    CoursComponent,
    CoursAdminComponent,

    
    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    JwtModule.forRoot({
      config: {
        // pour injecter le token dans toutes les requetes
        tokenGetter: function tokenGetter() {
          
          return localStorage.getItem('access_token');
        },
        // inject le token pour tous ces chemin
        whitelistedDomains: [ environment.backServer],
        // n'injecte pas le token pour ce chemin
        blacklistedRoutes: [ `${environment.backSchema}://${environment.backServer}/auth/login`
        ]
      }
    }),
    NgbAlertModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent],
})
export class AppModule { }
