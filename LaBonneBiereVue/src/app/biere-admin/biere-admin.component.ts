import { Component, OnInit, Input } from '@angular/core';
import { BiereService } from '../service/biere.service';
import { BiereDto } from '../model/biere-dto';
import { Router } from '@angular/router';
import { faInfoCircle, faPlusCircle, faMinus,faTools, faHome,faTh,faSearch,faTimesCircle, faTrash,faBeer, faEdit, faPlus, faToggleOn,faToggleOff, faCheckCircle, faReply,faUserCog, faChair, faChartLine,faChalkboardTeacher} from '@fortawesome/free-solid-svg-icons';
import { TypeDeBiereDto } from '../model/type-de-biere-dto';
import { TypeDeBiereService } from '../service/type-de-biere.service';
import { environment } from 'src/environments/environment';



@Component({
  selector: 'app-biere-admin',
  templateUrl: './biere-admin.component.html',
  styleUrls: ['./biere-admin.component.css'],
  host: {'(window:scroll)': 'track($event)'}
})
export class BiereAdminComponent implements OnInit {
  faInfoCircle = faInfoCircle;
  faPlusCircle = faPlusCircle;
  faPlus=faPlus
  faDamier=faTh
  faMinus = faMinus;
  faClose=faTimesCircle
  faTrash = faTrash;
  faReply=faReply
  faTools=faTools
  faEdit = faEdit;
  faCheck=faCheckCircle
  faEye=faToggleOn
  faEye1=faToggleOff;
  faBeer=faBeer
  faHome=faHome
  faSearch = faSearch;
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUser = faUserCog;
  faTable=faChair
  bieres: Array<BiereDto>=new Array<BiereDto>();
  recherche: string = "";
  touche = "";
  bool = false;
  biere: BiereDto;
  typeDebieres: Array<TypeDeBiereDto>;
  filtre: Array<number>=new Array()
  page:number=0;
  url:String=environment.backSchema+"://"+environment.backServer
  quant: number;
  origine: string='gestion/bieres'
  inputQte=0
  inputPrix: number;
  active: boolean=false
  noActive: boolean=false
  prixMin:number
  prixMax:number
  filtre1: Array<number>=new Array()
  qteMax: number
  qteMin: number
  alcMin: number
  alcMax: number
  posBool: boolean=false
  posBool2: boolean;
  count: number;
  sorting="rien"
  damierB: boolean=false
  createB: boolean=false
  position: number=0
  bieres2: Array<BiereDto>=new Array<BiereDto>();
  child=false;
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
 





  constructor(private biereService: BiereService, private router: Router, private typeDeBiereService: TypeDeBiereService) { }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    this.filtre1.push(-1)
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }else{
      document.getElementById('id104').style.display = 'block';
    }
    this.biereService.refreshList.subscribe(() => { this.reloadData() });
    this.reloadData();

        this.typeDeBiereService.getAll().subscribe(
          donnees => {
            this.typeDebieres = donnees;
            this.remplacement()
            
          }
        );

        window.scrollTo(0, 0)
  }

  remplacement() {
    var index=0
    this.typeDebieres.forEach(element => {
      if (element.nom === "Tampon") {
        this.typeDebieres.splice(index,1)
      }
      index++
      
    });
    

}

  

  findbyname() {
    var textarea =<HTMLInputElement> document.getElementById('search').firstChild
    textarea.addEventListener('keyup', (e) => {
      this.touche = e.key
    });
    if (this.recherche.length === 0 && this.touche === "Backspace" && this.bool === true) {
    } else {
      if (this.recherche.length === 0) {
        this.bool = true;
      } else {
        this.bool = false;
      }
      this.recherche=this.recherche.substring(0,1).toUpperCase()+this.recherche.substring(1).toLowerCase()
      this.biereService.findbyname(this.recherche.substring(0),this.page).subscribe(
        donnees => {
          this.biereService.subjectMiseAJour.next(0)
          this.bieres = donnees;
        }
      );
    }
  }

  findByFilter(event:string) {
      if(event=="noAll"){
        document.getElementById("noAll").style.display='block'
        document.getElementById("all").style.display='none'
      }else if(event=="all"){
        this.filtre=[]
        document.getElementById("noAll").style.display='none'
        document.getElementById("all").style.display='block'
        this.typeDebieres.forEach(element=>{
          document.getElementById("No"+element.nom).style.display='block'
          document.getElementById(element.nom).style.display='none'
        })
        this.refresh()

      }else{
        if(event.startsWith("No")){
          
          document.getElementById(event).style.display='block'
          document.getElementById(event.substring(2,event.length)).style.display='none'

          this.typeDebieres.find(element=>{
            if(element.nom==event.substring(2,event.length)){
              
              if(this.filtre.includes(element.id)){
               var index1= this.filtre.indexOf(element.id)
                this.filtre.splice(index1,1) 
              }
            }
  
           })

           this.refresh()
        }else{
          document.getElementById("No"+event).style.display='none'
          document.getElementById(event).style.display='block'
          document.getElementById("noAll").style.display='block'
          document.getElementById("all").style.display='none'
  
          this.typeDebieres.find(element=>{
            if(element.nom==event){
             
              if(!this.filtre.includes(element.id)){
                this.filtre.push(element.id) 
              }
            }
           })
           this.refresh()
        }
  
     
    }
  }


  reloadData() {
    this.biereService.getAllAdmin(this.page.toString()).subscribe(
      donnees => {
        this.bieres = donnees; 
      }
    );
    this.biereService.findbytype(this.filtre1,this.page+1,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
      data=>{
        this.bieres2=data
      }
    )  
  }

  delete(event) {
    this.biereService.getOne(event).subscribe(
      data=>{
        this.biere=data
        this.biere.active=false
        this.biereService.updateBiere(event,this.biere).subscribe(
         data=>{
           this.refresh()
           
         }
        )
      }
    )
  }

  getBiere(id: number) {
    this.biereService.chargementDetail.next(id);
    //this.router.navigate(['detailAdmin', id]);
  }

  updateBiere(id: number) {
    this.biereService.chargementDetail.next(id);
    //this.router.navigate(['updateBiere', id]);

  }

  quantite(event){

    this.bieres.forEach(element=>{
      if(element.id!=event){
        document.getElementById("btn"+element.id).style.display='block'
        document.getElementById("inputQte"+element.id).style.display='none'
        document.getElementById("btnPrix"+element.id).style.display='block'
        document.getElementById("inputPrix"+element.id).style.display='none'
      }else{
        document.getElementById("btnPrix"+element.id).style.display='block'
        document.getElementById("inputPrix"+element.id).style.display='none'
      }
    })
      this.biereService.getOne(event).subscribe(
        data=>{
          this.biere=data
          this.inputQte=this.biere.quantite
          document.getElementById("btn"+event).style.display='none'
          document.getElementById("inputQte"+event).style.display='block'
          var textarea = document.getElementById('inputQte'+event)
      textarea.addEventListener('keyup', (e) => {
        if(e.key=="Enter"){
          this.biere.quantite=this.inputQte
          this.biereService.updateBiere(event,this.biere).subscribe(
            data=>{
              document.getElementById("btn"+event).style.display='block'
              document.getElementById("inputQte"+event).style.display='none'
                this.refresh()            
            }
          ) 
         }
      });
       })  

    
  }

  prix(event){

    this.bieres.forEach(element=>{
      if(element.id!=event){
        document.getElementById("btnPrix"+element.id).style.display='block'
        document.getElementById("inputPrix"+element.id).style.display='none'
        document.getElementById("btn"+element.id).style.display='block'
        document.getElementById("inputQte"+element.id).style.display='none'
      }else{
        document.getElementById("btn"+element.id).style.display='block'
        document.getElementById("inputQte"+element.id).style.display='none'
      }
    })
      this.biereService.getOne(event).subscribe(
        data=>{
          this.biere=data
          this.inputPrix=this.biere.prix
          document.getElementById("btnPrix"+event).style.display='none'
          document.getElementById("inputPrix"+event).style.display='block'
          var textarea = document.getElementById('inputPrix'+event)
      textarea.addEventListener('keyup', (e) => {
        if(e.key=="Enter"){
          this.biere.prix=this.inputPrix
          this.biereService.updateBiere(event,this.biere).subscribe(
            data1=>{
              document.getElementById("btnPrix"+event).style.display='block'
              document.getElementById("inputPrix"+event).style.display='none'
                this.refresh()
            }
          ) 
         }
      });
       })  

    
  }

  nonActive(){

  if(!this.noActive){
          this.noActive=true
          this.active=false
          document.getElementById("noActOn").style.display='block'
          document.getElementById("noActOff").style.display='none'
          document.getElementById("actOn").style.display='none'
          document.getElementById("ActOff").style.display='block'

  }else{
          this.noActive=false
          document.getElementById("noActOn").style.display='none'
          document.getElementById("noActOff").style.display='block'
        }
        this.refresh()

   }

   activeB(){

    if(!this.active){
            this.active=true
            this.noActive=false
            document.getElementById("actOn").style.display='block'
            document.getElementById("ActOff").style.display='none'
            document.getElementById("noActOn").style.display='none'
            document.getElementById("noActOff").style.display='block'

    }else{
            this.active=false
            document.getElementById("actOn").style.display='none'
            document.getElementById("ActOff").style.display='block'

    }
    this.refresh()
     }


   activate(event){
    
     this.biereService.getOne(event).subscribe(
       data=>{
         this.biere=data
         this.biere.active=true
         this.biereService.updateBiere(event,this.biere).subscribe(
          data=>{
            this.refresh()
            
          }
         )
       }
     )

   }

   sliderMin(event){
    this.prixMin=event.target.valueAsNumber
     if(event.target.valueAsNumber>this.prixMax){
       this.prixMax=event.target.valueAsNumber
     }
     this.refresh()
   }

   sliderMax(event){
    this.prixMax=(event.target.valueAsNumber)
    if(this.prixMax<this.prixMin){
      this.prixMin=this.prixMax
    }
    this.refresh()
  }

  sliderMinQte(event){
    this.qteMin=event.target.valueAsNumber
     if(event.target.valueAsNumber>this.qteMax){
       this.qteMax=event.target.valueAsNumber
     }
     this.refresh()
   }

   sliderMaxQte(event){
    this.qteMax=(event.target.valueAsNumber)
    if(this.qteMax<this.qteMin){
      this.qteMin=this.qteMax
    }
    this.refresh()
  }

  sliderMinAlc(event){
    this.alcMin=event.target.valueAsNumber
     if(event.target.valueAsNumber>this.alcMax){
       this.alcMax=event.target.valueAsNumber
     }
     this.refresh()
   }

   sliderMaxAlc(event){
    this.alcMax=(event.target.valueAsNumber)
    if(this.alcMax<this.alcMin){
      this.alcMin=this.alcMax
    }
    this.refresh()
  }

  minimumInput(event){
    this.prixMin=event.target.valueAsNumber
      if(this.prixMin>this.prixMax){
        this.prixMax=this.prixMin
      }
      this.refresh()
      }

  maximumInput(event){
    this.prixMax=event.target.valueAsNumber
    if(this.prixMax<this.prixMin){
      this.prixMin=this.prixMax
    }
    this.refresh()
  }

  minimumInputQte(event){
    this.qteMin=event.target.valueAsNumber
    if(this.qteMin>this.qteMax){
      this.qteMax=this.qteMin
    }
    this.refresh()
    }

maximumInputQte(event){
  this.qteMax=event.target.valueAsNumber
  if(this.qteMax<this.qteMin){
    this.qteMin=this.qteMax
  }
  this.refresh()
}

minimumInputAlc(event){
  this.alcMin=event.target.valueAsNumber
  if(this.alcMin>this.alcMax){
    this.alcMax=this.alcMin
  }
  this.refresh()
  }

maximumInputAlc(event){
  this.alcMax=event.target.valueAsNumber
if(this.alcMax<this.alcMin){
  this.alcMin=this.alcMax
}
this.refresh()
}

  refresh(){
    
    if(this.filtre.length==0){
      
      this.biereService.refreshList.subscribe(() => {
        this.biereService.findbytype(this.filtre1,this.page,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting) });
      this.biereService.findbytype(this.filtre1,this.page,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
        data=>{
          this.bieres=data
          if(this.bieres.length==0&&this.page>0){
            this.page--
            this.biereService.findbytype(this.filtre1,this.page,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
              data=>{
                this.bieres=data
              }
            )

          }
        }

      )  
      this.biereService.findbytype(this.filtre1,this.page+1,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
          data=>{
            this.bieres2=data
          }
        )  
    }else{
     
      this.biereService.refreshList.subscribe(() => {
      this.biereService.findbytype(this.filtre,this.page,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting) });
      this.biereService.findbytype(this.filtre,this.page,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
        data=>{
          this.bieres=data
          if(this.bieres.length==0&&this.page>0){
            this.page--
            this.biereService.findbytype(this.filtre1,this.page,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
              data=>{
                this.bieres=data
              }
            )

          }
        }
      ) 
      this.biereService.findbytype(this.filtre1,this.page+1,this.active,this.noActive,this.prixMin,this.prixMax, this.qteMin, this.qteMax, this.alcMin, this.alcMax, this.sorting).subscribe(
        data=>{
          this.bieres2=data
        }
      )     

    }
  }

   sort(event){
     this.sorting=event
  this.refresh()
  }

  track(event){
    
    this.position=parseInt(event.path[1].window.scrollY)
    var top:string=(this.position+50).toString()+"px"
      document.getElementById('myModal').style.top=top
      var top:string=(this.position+200).toString()+"px"
      document.getElementById('myModal1').style.top=top


   
    if(this.position>=180){
     this.posBool=true
     document.getElementById("top").style.backgroundImage= 'none'
      document.getElementById("top").style.display='block'
      document.getElementById("top").style.position='sticky'
      document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
     document.getElementById("iCreate").style.fontSize='5vw'
     
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("top").style.boxShadow= "none"
       document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=350
     img.height=280
       document.getElementById("iCreate").style.fontSize='4vw'
       
     }
     
  }
  countM(){
    this.count++
    if(this.count>20){
      
      this.posBool=false

    }
  }

  pagination(event){
    window.scrollTo(0,180)
    if(event=="precedent"){
      if(this.page>0){
        
        this.page--
        this.refresh()
      }

    }else{
      
        this.page++
        this.refresh()
    }


  }

  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true

    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }
  emit(event){
    
    this.child=event
  }

  create(){
    if(this.createB==false){
      document.getElementById('myModal1').style.display='block'
      this.createB=true

    }else{
      document.getElementById('myModal1').style.display='none'
      this.createB=false
      if(this.child){
        window.location.reload()
      }

    }
  }
  
}

