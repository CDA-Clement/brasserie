import { Component, OnInit } from '@angular/core';
import { faInfoCircle, faPlusCircle, faMinus,faTools, faHome,faTh,faSearch,faTimesCircle, faTrash,faBeer, faEdit, faPlus, faToggleOn,faToggleOff, faCheckCircle, faReply,faUserCog, faChair, faChartLine,faChalkboardTeacher} from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cours-admin',
  templateUrl: './cours-admin.component.html',
  styleUrls: ['./cours-admin.component.css']
})
export class CoursAdminComponent implements OnInit {
  faInfoCircle = faInfoCircle;
  faPlusCircle = faPlusCircle;
  faPlus=faPlus
  faDamier=faTh
  faMinus = faMinus;
  faClose=faTimesCircle
  faTrash = faTrash;
  faReply=faReply
  faTools=faTools
  faEdit = faEdit;
  faCheck=faCheckCircle
  faEye=faToggleOn
  faEye1=faToggleOff;
  faBeer=faBeer
  faHome=faHome
  faSearch = faSearch;
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUser = faUserCog;
  faTable=faChair
  damierB: boolean=false
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);

  constructor(private router: Router) { }

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }else{
      document.getElementById('id104').style.display = 'block';
    }
  }

  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true

    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }

}
