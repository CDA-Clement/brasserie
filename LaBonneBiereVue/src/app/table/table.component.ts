import { Component, OnInit } from '@angular/core';
import { TableDto } from '../model/table-dto';
import { TableService } from '../service/table.service';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { faCamera, faTimes, faCheck, faQuoteRight } from '@fortawesome/free-solid-svg-icons';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { AlertService } from '../service/alert.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  host: {'(window:scroll)': 'track($event)'},
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  tables= new Array<TableDto>();
  idTableSelectionnee: number;
  faTimes=faTimes;
  faCheck=faCheck;
  nomTableSelectionnee: string;
  dispoTableSelectionnee: string;
  user = localStorage.getItem("current_user")
  objJson = JSON.parse(this.user);
  faAngleRight = faAngleRight;
  faAngleLeft = faAngleLeft;
  faCamera = faCamera;
  numero: number = 1;
  utilisateur: UtilisateurDto= new UtilisateurDto();
  origine: string = 'table';
  rien: boolean=true;
  table= new TableDto();
  page:number=0
  position: number=0
  heureResa:number
  posBool: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  posBool2: boolean;
  count: number;
  faQuote=faQuoteRight


  constructor(private router: Router,private tableService: TableService, private utilisateurService: UtilisateurService,private alertService: AlertService) {
    this.table.utilisateur=new UtilisateurDto()
    
    

  }
  ngOnInit() {
    window.scrollTo(0,0)
    document.getElementById('myModal').style.top="550px"
    localStorage.setItem("origine", this.origine);
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }else{
      this.alertService.addPrimary('Veuillez saisir votre age')
      document.getElementById("id104").style.display='block'
    }
      if (this.objJson != null) {
  
        this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
          data => {
            this.utilisateur = data;
          }
        );
  
      } else {
        this.rien=false;
        this.utilisateur = new UtilisateurDto();
        
      }
    
    this.tableService.getAllbyPage(this.page).subscribe(
      data => {
        this.table = data;
      }
    );

    this.tableService.findbytype(true,false,0,"rien",false,false).subscribe(
      data=>{
        this.tables=data
      }
    )
  
}

track(event){
    
  this.position=parseInt(event.path[1].window.scrollY)
  var top:string=(this.position+150).toString()+"px"
    document.getElementById('myModal').style.top=top
    
   
}
countM(){
  this.count++
  if(this.count>20){
    
    this.posBool=false

  }
}

heure(event:number){
this.heureResa=event
document.getElementById("confirmationHeure").style.display='block'
}

confirmer(event){
  if(event=="yes"){
    this.tableService.updateResa(this.objJson.id, this.table.id, 1,this.heureResa).subscribe(
      data=>{
        this.alertService.addPrimary("réservation confirmée")
        this.tableService.refreshList.subscribe(() => {
          this.tableService.getAllbyPage(this.page) });
       this.tableService.getAllbyPage(this.page).subscribe(
          data=>{
            this.table=data    
          }
   
        )     
    }
      )
   
  }
  document.getElementById("confirmationHeure").style.display='none'
  document.getElementById("myModal").style.display='none'


}

closeModal(){
  document.getElementById("myModal").style.display='none'
}


  public nextSlide(e) {
   this.page++
   if(this.page>this.tables.length-1){
     this.page=0
   }
   this.tableService.getAllbyPage(this.page).subscribe(
     data=>{
       this.table=data
     }
   )

  }

  public previousSlide(e) {
    this.page--
   if(this.page<0){
     this.page=this.tables.length-1
   }
   this.tableService.getAllbyPage(this.page).subscribe(
     data=>{
       this.table=data 
     }
   )

  }



  annuler() {
    
    this.tableService.updateResa(this.objJson.id, this.table.id, 0,20).subscribe(
      data => {
        this.utilisateur = data;
        this.alertService.addAnnulation("réservation annulée")
        this.ngOnInit();
      }
    )
  }

  reserver(){
    if (this.objJson != null) {
      document.getElementById('myModal').style.display='block'
    }
    else{
      this.alertService.addPrimary("Veuillez vous connecter pour reserver une table")
    }
  }

}  
