import { Component, OnInit } from '@angular/core';
import { faFileInvoice, faCalendarAlt, faReply, faEuroSign, faCalendarDay, faShoppingBasket, faArchive, faWarehouse, faCalendarWeek, faBeer, faVenusMars, faSearch, faDownload } from '@fortawesome/free-solid-svg-icons';
import { faOrcid } from '@fortawesome/free-brands-svg-icons';
import { FactureService } from '../service/facture.service ';
import { BiereDto } from '../model/biere-dto';
import { HistoriqueDAchatDto } from '../model/historique-dachat-dto';
import { formatDate } from '@angular/common';
import { environment } from 'src/environments/environment';
import {saveAs} from 'file-saver'
import { Router } from '@angular/router';



@Component({
  selector: 'app-facture',
  templateUrl: './facture.component.html',
  styleUrls: ['./facture.component.css']
})
export class FactureComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  faInvoice=faFileInvoice;
  faCalendar=faCalendarAlt;
  faCalendarDay=faCalendarDay;
  faCalendar1=faCalendarWeek;
  faEuro=faEuroSign;
  faPanier=faShoppingBasket;
  faArchive=faArchive;
  faId=faOrcid;
  faReply=faReply;
  faWarehouse=faWarehouse;
  faBeer=faBeer;
  faSearch=faSearch;
  faDownload=faDownload;
  faGender=faVenusMars
  bieres:BiereDto[]
  allFactures: HistoriqueDAchatDto[];
  allFacturesMonth: HistoriqueDAchatDto[];
  total:number=0
  dateEndMonth:String;
  dateBeginningMonth:String;
  totalMonth: number=0
  panierMoyen: number=0;
  lastInvoice: HistoriqueDAchatDto=new HistoriqueDAchatDto()
  allFacturesYear: HistoriqueDAchatDto[];
  totalYear: number=0
  dateBeginningYear: string;
  dateEndYear: string;
  favoriteBeer=Array<String>();
  url:String=environment.backSchema+"://"+environment.backServer;
  biereName: String;
  biereId: String;
  lastInvoiceBool: any;



  constructor(private router: Router,private factureService: FactureService) { }

  ngOnInit() {
    window.scrollTo(0, 0)
   var date=formatDate(new Date(), 'yyyy/MM/dd', 'en');
    document.getElementById('footerID').style.display="block";

    if (localStorage.getItem('current_user') == null) {
      this.router.navigateByUrl("/login")
    }
    var currentMonth=parseInt(date.substring(5,7))
    
    if(currentMonth==1||currentMonth==3||currentMonth==5||currentMonth==7||currentMonth==8||currentMonth==10||currentMonth==12){
      this.dateEndMonth=date.substring(0,8)+"31-23:59:59"
      this.dateEndMonth= this.dateEndMonth.replace(new RegExp('/', 'g'),".")
    }
    else if(currentMonth==4||currentMonth==6||currentMonth==9||currentMonth==11){
      this.dateEndMonth=date.substring(0,8)+"30-23:59:59"
      this.dateEndMonth= this.dateEndMonth.replace(new RegExp('/', 'g'),".")
    }else{
      this.dateEndMonth=date.substring(0,8)+"29-23:59:59"
      this.dateEndMonth=this.dateEndMonth.replace(new RegExp('/', 'g'),".")
    }
    this.dateBeginningMonth=date.substring(0,8)+"01-00:00:00"
    this.dateBeginningMonth=this.dateBeginningMonth.replace(new RegExp('/', 'g'),".")
    

    this.factureService.allUserInvoicesPeriod(this.objJson.id,this.dateBeginningMonth,this.dateEndMonth).subscribe(
      data=>{
        this.allFacturesMonth=data
        this.allFacturesMonth.forEach(element=>{
          
          this.totalMonth+=element.prixTotal
        }
        )
          var stringTo=this.totalMonth.toFixed(2);
          this.totalMonth=parseFloat(stringTo);

      }
    )

    this.dateBeginningYear=date.substring(0,4)+".01.01-00:00:00"
    this.dateEndYear=date.substring(0,4)+".12.31-23:59:59"
    this.factureService.allUserInvoicesPeriod(this.objJson.id,this.dateBeginningYear,this.dateEndYear).subscribe(
      data=>{
        this.allFacturesYear=data
        this.allFacturesYear.forEach(element=>{
          this.totalYear+=element.prixTotal
        }
        )
          var stringTo=this.totalYear.toFixed(2);
          this.totalYear=parseFloat(stringTo);

      }
    )

    this.factureService.allUserInvoices(this.objJson.id).subscribe(
      data=>{
        this.allFactures=data
        this.allFactures.forEach(element=>{
          this.total+=element.prixTotal
        }
        )
        var stringTo=this.total.toFixed(2);
        this.total=parseFloat(stringTo);
        if(this.total>0){
          this.panierMoyen=this.total/this.allFactures.length
          stringTo=this.panierMoyen.toFixed(2);
          this.panierMoyen=parseFloat(stringTo);
          this.lastInvoice=this.allFactures[this.allFactures.length-1]
          this.lastInvoice.dateAchatString=this.lastInvoice.dateAchat.toString()
          this.lastInvoice.dateAchatString= this.lastInvoice.dateAchatString.substring(0,19)
          this.lastInvoice.dateAchatString= this.lastInvoice.dateAchatString.replace('T',' ')
          this.bieres=this.allFactures[this.allFactures.length-1].listDeBiere
          this.bieres.forEach(element=>{
            element.prixTotal=element.commande*element.prix
            stringTo=element.prixTotal.toFixed(2);
           element.prixTotal=parseFloat(stringTo)
          })
          stringTo=this.lastInvoice.prixTotal.toFixed(2);
          this.lastInvoice.prixTotal=parseFloat(stringTo)
        }else{
          this.panierMoyen=0
        }
      }
    )

    this.factureService.favoriteUserBeer(this.objJson.id).subscribe(
      data=>{
        this.favoriteBeer=data
        this.biereName=this.favoriteBeer[0]
        this.biereId=this.favoriteBeer[1]
      }
    )
  }


   afficherInvoice(){
     if(!this.lastInvoiceBool){
       document.getElementById('lastInvoice').style.display='block'
       document.getElementById('download').style.display='block'

       this.lastInvoiceBool=true
     }else{
      document.getElementById('lastInvoice').style.display='none'
      document.getElementById('download').style.display='none'
      this.lastInvoiceBool=false
     }
   }

   download(){
     this.factureService.downloadPDF(this.objJson.id,this.lastInvoice.id.toString()).subscribe(
       data=>{
          saveAs(data, "invoice-"+this.objJson.id+"-"+this.lastInvoice.id+".pdf")
       },
       error=>{

         
       }
     )
   }


}
