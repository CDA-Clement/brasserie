import { Component, OnInit } from '@angular/core';
import { HistoriqueDAchatDto } from '../model/historique-dachat-dto';
import { environment } from 'src/environments/environment';
import { faFileInvoice, faCalendarAlt, faReply, faEuroSign, faCalendarDay, faArchive, faCalendarWeek, faBeer, faSearch, faDownload } from '@fortawesome/free-solid-svg-icons';
import { faOrcid } from '@fortawesome/free-brands-svg-icons';
import { FactureService } from '../service/facture.service ';
import { AlertService } from '../service/alert.service';
import { BiereDto } from '../model/biere-dto';
import { element } from 'protractor';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recherche-facture',
  templateUrl: './recherche-facture.component.html',
  styleUrls: ['./recherche-facture.component.css']
})
export class RechercheFactureComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  lastInvoice: HistoriqueDAchatDto=new HistoriqueDAchatDto()
  allFactures: Array<HistoriqueDAchatDto>;
  url:String=environment.backSchema+"://"+environment.backServer;
  rechercheID: boolean;
  faCalendar=faCalendarAlt;
  faCalendarDay=faCalendarDay;
  faCalendar1=faCalendarWeek;
  faEuro=faEuroSign;
  faId=faOrcid;
  faReply=faReply;
  faBeer=faBeer;
  faSearch=faSearch;
  faDownload=faDownload;
  numeroFacture:number
  bieres:BiereDto[]
  stringTo='';
  rechercheEuro: boolean;
  montantFacture: any;
  dateFacture: String;
  rechercheDate: boolean;
  dateFacture1: any;
  rechercheDatePeriod: boolean;
  rechercheAPartir: boolean;
  rechercheJusque: boolean;

  constructor(private router: Router,private factureService: FactureService, private alertService: AlertService) { }

  ngOnInit() {
   
    window.scrollTo(0,0)
    if (localStorage.getItem('current_user') == null) {
      this.router.navigateByUrl("/login")
    }
  }

  recherche(event){
    
    if(event=='id'&&!this.rechercheID){
     document.getElementById('rechercheID').style.display='none'
     document.getElementById('rechercheID1').style.display='block'
     this.rechercheID=true
    }else if(event=='id'&&this.rechercheID){
     document.getElementById('rechercheID').style.display='block'
     document.getElementById('rechercheID1').style.display='none'
     if(this.numeroFacture!=undefined){
      this.factureService.oneInvoice(this.objJson.id,this.numeroFacture.toString()).subscribe(
        data=>{
          this.lastInvoice=data
          if(this.lastInvoice.id!=undefined){
            this.stringTo=this.lastInvoice.prixTotal.toFixed(2);
            this.lastInvoice.prixTotal=parseFloat(this.stringTo)
            this.lastInvoice.dateAchatString=this.lastInvoice.dateAchat.toString()
            this.lastInvoice.dateAchatString= this.lastInvoice.dateAchatString.substring(0,19)
            this.lastInvoice.dateAchatString= this.lastInvoice.dateAchatString.replace('T',' ')
            this.bieres=this.lastInvoice.listDeBiere
            this.bieres.forEach(element=>{
              element.prixTotal=element.prix*element.commande
              this.stringTo=element.prixTotal.toFixed(2);
             element.prixTotal=parseFloat(this.stringTo)
            })
            document.getElementById('invoice').style.display='block'
            document.getElementById("rechercheFacture").style.display='none'
           this.allFactures = new Array<HistoriqueDAchatDto>()
            this.allFactures.push(this.lastInvoice)

          }else{
            this.alertService.addDanger('facture '+this.numeroFacture+' introuvable ')
          }
        },
        error=>{
          this.alertService.addDanger('facture '+this.numeroFacture+' introuvable '+error)

        }
      )
     }
     this.rechercheID=false
    }


    if(event=='euro'&&!this.rechercheEuro){
      document.getElementById('rechercheEuro').style.display='none'
      document.getElementById('rechercheEuro1').style.display='block'
      this.rechercheEuro=true
     }else if(event=='euro'&&this.rechercheEuro){
      document.getElementById('rechercheEuro').style.display='block'
      document.getElementById('rechercheEuro1').style.display='none'
      if(this.montantFacture!=undefined){
       this.factureService.oneInvoicePerPrice(this.objJson.id,this.montantFacture).subscribe(
        data=>{
          this.allFactures=data
          if(this.allFactures.length>0){
            this.allFactures.forEach(element => {
              element.dateAchatString=element.dateAchat.toString()
            element.dateAchatString= element.dateAchatString.substring(0,19)
            element.dateAchatString= element.dateAchatString.replace('T',' ')
              this.stringTo= element.prixTotal.toFixed(2)
              element.prixTotal=parseFloat(this.stringTo)
              this.bieres=element.listDeBiere
              this.bieres.forEach(element=>{
                element.prixTotal=element.prix*element.commande
                this.stringTo=element.prixTotal.toFixed(2);
               element.prixTotal=parseFloat(this.stringTo)
              })
            });
            document.getElementById('invoice').style.display='block'
           
            document.getElementById("rechercheFacture").style.display='none'

          }else{
            this.alertService.addDanger('facture montant '+this.montantFacture +' € introuvable ')
          }
        },
        error=>{
          this.alertService.addDanger('facture montant '+this.montantFacture +' € introuvable '+error)

        }
      )
     }
      this.rechercheEuro=false
     }

     if(event=='date'&&!this.rechercheDate){
      document.getElementById('rechercheDate').style.display='none'
      document.getElementById('rechercheDate1').style.display='block'
      this.rechercheDate=true
     }else if(event=='date'&&this.rechercheDate){
      document.getElementById('rechercheDate').style.display='block'
      document.getElementById('rechercheDate1').style.display='none'
      if(this.dateFacture!=undefined){
        this.dateFacture=this.dateFacture.replace(new RegExp('-', 'g'),".")
       this.factureService.allUserInvoicesPerDate(this.objJson.id,this.dateFacture+"-00:00:00",this.dateFacture+"-23:59:59").subscribe(
        data=>{
          this.allFactures=data
          if(this.allFactures.length>0){
            this.allFactures.forEach(element => {
              element.dateAchatString=element.dateAchat.toString()
            element.dateAchatString= element.dateAchatString.substring(0,19)
            element.dateAchatString= element.dateAchatString.replace('T',' ')
              this.stringTo= element.prixTotal.toFixed(2)
              element.prixTotal=parseFloat(this.stringTo)
              this.bieres=element.listDeBiere
              this.bieres.forEach(element=>{
                element.prixTotal=element.prix*element.commande
                this.stringTo=element.prixTotal.toFixed(2);
               element.prixTotal=parseFloat(this.stringTo)
              })
            });
            document.getElementById('invoice').style.display='block'
           
            document.getElementById("rechercheFacture").style.display='none'

          }else{
            this.alertService.addDanger('facture date '+this.dateFacture+' introuvable ')
          }
        },
        error=>{
          this.alertService.addDanger('facture date '+this.dateFacture+' introuvable '+error)

        }
      )
     }
      this.rechercheDate=false
     }


     if(event=='datePeriod'&&!this.rechercheDatePeriod){
      document.getElementById('rechercheDatePeriod').style.display='none'
      document.getElementById('rechercheDatePeriod1').style.display='block'
      this.rechercheDatePeriod=true
     }else if(event=='datePeriod'&&this.rechercheDatePeriod){
      document.getElementById('rechercheDatePeriod').style.display='block'
      document.getElementById('rechercheDatePeriod1').style.display='none'
      if(this.dateFacture!=undefined&&this.dateFacture1!=undefined){
        this.dateFacture=this.dateFacture.replace(new RegExp('-', 'g'),".")
        this.dateFacture1=this.dateFacture1.replace(new RegExp('-', 'g'),".")
       this.factureService.allUserInvoicesPerDate(this.objJson.id,this.dateFacture+"-00:00:00",this.dateFacture1+"-23:59:59").subscribe(
        data=>{
          this.allFactures=data
          if(this.allFactures.length>0){
            this.allFactures.forEach(element => {
              element.dateAchatString=element.dateAchat.toString()
            element.dateAchatString= element.dateAchatString.substring(0,19)
            element.dateAchatString= element.dateAchatString.replace('T',' ')
              this.stringTo= element.prixTotal.toFixed(2)
              element.prixTotal=parseFloat(this.stringTo)
              this.bieres=element.listDeBiere
              this.bieres.forEach(element=>{
                element.prixTotal=element.prix*element.commande
                this.stringTo=element.prixTotal.toFixed(2);
               element.prixTotal=parseFloat(this.stringTo)
              })
            });
            document.getElementById('invoice').style.display='block'
           
            document.getElementById("rechercheFacture").style.display='none'

          }else{
            this.alertService.addDanger('facture periode du '+this.dateFacture+' au '+this.dateFacture1+' introuvable ')
          }
        },
        error=>{
          this.alertService.addDanger('facture periode du '+this.dateFacture+' au '+this.dateFacture1+' introuvable '+error)

        }
      )
     }
      this.rechercheDatePeriod=false
     }


     if(event=='dateAPartir'&&!this.rechercheAPartir){
      document.getElementById('rechercheAPartir').style.display='none'
      document.getElementById('rechercheAPartir1').style.display='block'
      this.rechercheAPartir=true
     }else if(event=='dateAPartir'&&this.rechercheAPartir){
      document.getElementById('rechercheAPartir').style.display='block'
      document.getElementById('rechercheAPartir1').style.display='none'
      if(this.dateFacture!=undefined){
       var currentYear=new Date().getFullYear().toString()
       var currentMonth=new Date().getMonth().toString()
      currentMonth=(parseInt(currentMonth)+1).toString()
       if(parseInt(currentMonth)<9){
         currentMonth="0"+currentMonth
       }
       var currentDay=new Date().getDate().toString()
      var currentDate=currentYear+"."+currentMonth+"."+currentDay
        this.dateFacture=this.dateFacture.replace(new RegExp('-', 'g'),".")
       this.factureService.allUserInvoicesPerDate(this.objJson.id,this.dateFacture+"-00:00:00",currentDate+"-23:59:59").subscribe(
        data=>{
          this.allFactures=data
          if(this.allFactures.length>0){
            this.allFactures.forEach(element => {
              element.dateAchatString=element.dateAchat.toString()
            element.dateAchatString= element.dateAchatString.substring(0,19)
            element.dateAchatString= element.dateAchatString.replace('T',' ')
              this.stringTo= element.prixTotal.toFixed(2)
              element.prixTotal=parseFloat(this.stringTo)
              this.bieres=element.listDeBiere
              this.bieres.forEach(element=>{
                element.prixTotal=element.prix*element.commande
                this.stringTo=element.prixTotal.toFixed(2);
               element.prixTotal=parseFloat(this.stringTo)
              })
            });
            document.getElementById('invoice').style.display='block'
           
            document.getElementById("rechercheFacture").style.display='none'

          }else{
            this.alertService.addDanger('facture à partir du '+this.dateFacture+' introuvable ')
          }
        },
        error=>{
          this.alertService.addDanger('facture à partir du '+this.dateFacture+' introuvable '+error)

        }
      )
     }
      this.rechercheAPartir=false
     }

     if(event=='dateJusque'&&!this.rechercheJusque){
      document.getElementById('rechercheJusque').style.display='none'
      document.getElementById('rechercheJusque1').style.display='block'
      this.rechercheJusque=true
     }else if(event=='dateJusque'&&this.rechercheJusque){
      document.getElementById('rechercheJusque').style.display='block'
      document.getElementById('rechercheJusque1').style.display='none'
      if(this.dateFacture!=undefined){
       
        this.dateFacture=this.dateFacture.replace(new RegExp('-', 'g'),".")
       this.factureService.allUserInvoicesPerDate(this.objJson.id,"2020.01.01-00:00:00",this.dateFacture+"-23:59:59").subscribe(
        data=>{
          this.allFactures=data
          if(this.allFactures.length>0){

            this.allFactures.forEach(element => {
              element.dateAchatString=element.dateAchat.toString()
            element.dateAchatString= element.dateAchatString.substring(0,19)
            element.dateAchatString= element.dateAchatString.replace('T',' ')
              this.stringTo= element.prixTotal.toFixed(2)
              element.prixTotal=parseFloat(this.stringTo)
              this.bieres=element.listDeBiere
              this.bieres.forEach(element=>{
                element.prixTotal=element.prix*element.commande
                this.stringTo=element.prixTotal.toFixed(2);
               element.prixTotal=parseFloat(this.stringTo)
              })
            });
            document.getElementById('invoice').style.display='block'
           
            document.getElementById("rechercheFacture").style.display='none'
          }else{
            this.alertService.addDanger("facture jusqu'au "+this.dateFacture+' introuvable ')
          }
        },
        error=>{
          this.alertService.addDanger("facture jusqu'au "+this.dateFacture+' introuvable '+error)

        }
      )
     }
      this.rechercheJusque=false
     }


  }
 

  download(event){
    this.factureService.downloadPDF(this.objJson.id,event).subscribe(
      data=>{
         saveAs(data, "invoice-"+this.objJson.id+"-"+event+".pdf")
      },
      error=>{
      }
    )
  }

  return(){
    document.getElementById('invoice').style.display='none'
   
    document.getElementById("rechercheFacture").style.display='block'
    this.rechercheID=false
    window.scrollTo(0,0)
  }

}
