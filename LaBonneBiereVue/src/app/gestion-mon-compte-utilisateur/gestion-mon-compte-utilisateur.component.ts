import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { faUserCog, faUnlockAlt, faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-gestion-mon-compte-utilisateur',
  templateUrl: './gestion-mon-compte-utilisateur.component.html',
  styleUrls: ['./gestion-mon-compte-utilisateur.component.css']
})
export class GestionMonCompteUtilisateurComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  utilisateur: UtilisateurDto;
  age: number;
  faUser=faUserCog
  faPassword=faUnlockAlt
  faInvoice=faFileInvoiceDollar
  constructor(private router: Router,private utilisateurService: UtilisateurService ) { }

  ngOnInit() {
    window.scrollTo(0,0)
    document.getElementById('footerID').style.display="block";

    if (localStorage.getItem('current_user') == null) {
      this.router.navigateByUrl("/login")
    }

    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      util => {
        this.utilisateur = util;

        var timeDiff = Date.now() - new Date(this.utilisateur.dateAge).getTime();
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      }
    );
    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      util => {
        this.utilisateur = util;

        var timeDiff = Date.now() - new Date(this.utilisateur.dateAge).getTime();
        this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      }
    );


  }

}