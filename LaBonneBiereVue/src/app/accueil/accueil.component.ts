import { Component, OnInit } from '@angular/core';
import { AlertService } from '../service/alert.service';
import { IpClientService } from '../service/ip-client.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  origine: string='accueil'
  ipAddress:string;
  constructor(private ip:IpClientService,private alertService: AlertService) { }

  ngOnInit() {
    this.getIP(); 
    window.scrollTo(0,0)
    localStorage.setItem("origine", this.origine);
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)';
    }else{
      this.alertService.addPrimary('Veuillez saisir votre age')
      document.getElementById('id104').style.display = 'block';
    } 

  }

  getIP(){  
    this.ip.getIPAddress().subscribe(
      res=>{  
    });  
  }  
}
