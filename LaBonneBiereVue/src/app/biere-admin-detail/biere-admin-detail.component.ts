import { Component, OnInit } from '@angular/core';
import { BiereDto } from '../model/biere-dto';
import { BiereService } from '../service/biere.service';
import { ActivatedRoute, Router } from '@angular/router';
import { faTh,faSearch, faTrash,faBeer,faHome, faEdit, faPlus,faReply,faUserCog, faChair, faChartLine,faChalkboardTeacher, faTools} from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';
import { TypeDeBiereDto } from '../model/type-de-biere-dto';
import { TypeDeBiereService } from '../service/type-de-biere.service';
import { FileService } from '../service/file.service';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-biere-admin-detail',
  templateUrl: './biere-admin-detail.component.html',
  host: {'(window:scroll)': 'track($event)','(window:mousemove)': 'move($event)'},
  styleUrls: ['./biere-admin-detail.component.css']
})
export class BiereAdminDetailComponent implements OnInit{
  biere: BiereDto
  idBiereParam: any;
  idB: string;
  faReply=faReply
  faDamier=faTh
  faEdit = faEdit;
  faPlus=faPlus
  faTools=faTools
  faHome=faHome
  faTrash = faTrash;
  faBeer=faBeer
  faSearch = faSearch;
  faCourse=faChalkboardTeacher
  faBusiness=faChartLine
  faUser = faUserCog;
  faTable=faChair
  position: number=0
  posBool: boolean=false
  posBool2: boolean;
  count: number;
  damierB: boolean=false
  url:String=environment.backSchema+"://"+environment.backServer
  typeDebieres: Array<TypeDeBiereDto>=new Array<TypeDeBiereDto>()
  selectedFiles: FileList;
  currentFile: File;
  nom=""
  moveB: boolean=false
  supprimable=false;
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  

  constructor(private route: ActivatedRoute, private router: Router,
    private biereService: BiereService, private typeDeBiereService: TypeDeBiereService, private fileService:FileService) { }


  

  ngOnInit() {
    if (this.user != null) {
      if(this.objJson.role.nom!='Admin'){
        this.router.navigateByUrl('/boutique') 
      }
       
    }else{
      this.router.navigateByUrl('/boutique')
    }
    window.scrollTo(0,0)
    
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }
    this.route.queryParams
      .subscribe(params => {
        this.idBiereParam = params;
        
      });
    var strId = Object.values(this.idBiereParam).toString()
    var id = parseInt(strId, 10)    
        this.biereService.getOne(id)
          .subscribe(data => {
            this.biere = data;
            this.nom=this.biere.nom
            this.biereService.getSupprimable(this.nom).subscribe(
              data=>{
                this.supprimable=data
                
              }
            )
           
          }, 
          );

          this.typeDeBiereService.getAll().subscribe(
            donnees => {
              this.typeDebieres = donnees;
              
            }
          );
          

  
          window.scrollTo(0, 0)
      
  }

  refresh(){
    this.biereService.refreshList.subscribe(() => {
      this.biereService.updateBiere(this.biere.id,this.biere) });
    this.biereService.updateBiere(this.biere.id,this.biere).subscribe(
      data=>{
        this.biereService.getOne(this.biere.id)
  }
    )
}

  track(event){
    
    this.position=parseInt(event.path[1].window.scrollY)
    var top:string=(this.position+50).toString()+"px"
      document.getElementById('myModal').style.top=top

   
    if(this.position>=80){
     this.posBool=true
     document.getElementById("top").style.backgroundImage= 'none'
      document.getElementById("top").style.display='block'
      document.getElementById("top").style.position='sticky'
      document.getElementById("top").style.boxShadow= "-1px 20px 20px -13px rgba(0,0,0,0.75)"
     document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=70
     img.height=60
   
     
    }
       if(this.posBool){
        this.countM()
       }
   
     if(this.position==0&&!this.posBool||this.position==0&&!this.posBool2){
       this.posBool2=true
       this.posBool=true
       this.count=0
       document.getElementById("top").style.boxShadow= "none"
       document.getElementById("top").style.height='auto'
     var img=<HTMLImageElement>document.getElementById("img")
     img.width=350
     img.height=280
       
       
     }
     
  }

  move(event){
    if(!this.moveB){
      document.getElementById('button').addEventListener('click', () => {
        document.getElementById('fileInput').click()
      })

      document.getElementById('button1').addEventListener('click', () => {
        document.getElementById('fileInput').click()
      })
      
      this.moveB=true
    }

     
  }
  countM(){
    this.count++
    if(this.count>5){
      
      this.posBool=false

    }
  }
  damier(event){
    
    if(this.damierB==false){
      document.getElementById('myModal').style.display='block'
      this.damierB=true

    }else{
      document.getElementById('myModal').style.display='none'
      this.damierB=false
    }
    
  }

  edit(event){
    if(event=='nom'){
      document.getElementById('inputNom').style.display='block'
      document.getElementById('idN').style.display='none'

    }
    if(event=='prix'){
      document.getElementById('inputPrix').style.display='block'
      document.getElementById('idP').style.display='none'

    }

    if(event=='description'){
      document.getElementById('inputDescription').style.display='block'
      document.getElementById('idD').style.display='none'

    }

    if(event=='degre'){
      document.getElementById('inputDegre').style.display='block'
      document.getElementById('idDe').style.display='none'

    }

    if(event=='quantite'){
      document.getElementById('inputQuantite').style.display='block'
      document.getElementById('idQ').style.display='none'

    }
    if(event=='type'){
      document.getElementById('btnType').style.display='block'

    }
    
  }

  inputNom(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputNom').style.display='none'
      
      document.getElementById('idN').style.display='block'
      document.getElementById('idN').style.marginTop="2%"
        this.refresh()
         }
  }

  inputDescription(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputDescription').style.display='none'
      
      document.getElementById('idD').style.display='block'
      document.getElementById('idD').style.marginTop="2%"
        this.refresh()
         }
  }

  inputDegre(event){
   
    
    if(event.key=="Enter"){
      document.getElementById('inputDegre').style.display='none'
      
      document.getElementById('idDe').style.display='block'
      document.getElementById('idDe').style.marginTop="2%"
        this.refresh()
         }
  }

  inputPrix(event){
    if(event.key=="Enter"){
      document.getElementById('inputPrix').style.display='none'
      document.getElementById('idP').style.display='block'
      document.getElementById('idP').style.marginTop="2%"
        this.refresh()
         }
  }

  inputQuantite(event){
    if(event.key=="Enter"){
      document.getElementById('inputQuantite').style.display='none'
      document.getElementById('idQ').style.display='block'
      document.getElementById('idQ').style.marginTop="2%"
        this.refresh()
         }
  }

  inputType(event){
    
    
    document.getElementById('btnType').style.display='none'
    this.typeDebieres.forEach(element=>{
      if(element.nom==event){
        this.biere.typeDeBiereDto.id=element.id
        this.biere.typeDeBiereDto.nom=event
      }
    })
        this.refresh()
  }


activer(event){
           if(event=="desactiver"){
             this.biere.active=false
           }else{
             this.biere.active=true
           }
           this.refresh()
}
  

selectFile(event) {
  this.selectedFiles = event.target.files;
  this.currentFile = this.selectedFiles.item(0);
  this.fileService.modifFile(this.currentFile,this.nom).subscribe(response => {
		
    if (response instanceof HttpResponse) {	  
     }
     window.location.reload()
   });    
  
}

supprimer(){
  this.biereService.deleteBiere(this.biere.id).subscribe(
    data=>{
      this.router.navigateByUrl("/gestion/bieres")
    }
  )
}

}
