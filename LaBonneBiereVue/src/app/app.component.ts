import { Component, OnInit } from '@angular/core';
import AOS from 'aos';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'LaBonneBiere';
  

  ngOnInit() {
    var log=localStorage.getItem("loginTime")
    if(log!=undefined&&new Date().getHours()-parseInt(log)!=0){
      
      localStorage.removeItem("current_user")
      localStorage.removeItem("access_token")
      localStorage.removeItem("loginTime")
      localStorage.removeItem("isConnected")
      window.location.reload()
    }
    AOS.init({
      duration: 400,
    })

  }
}
