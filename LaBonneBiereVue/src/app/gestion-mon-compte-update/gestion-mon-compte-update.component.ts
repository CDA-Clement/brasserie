import { Component, OnInit, Input } from '@angular/core';
import { UtilisateurService } from '../service/utilisateur.service';
import { UtilisateurDto } from '../model/utilisateur-dto';
import { Router } from '@angular/router';
import { TableService } from '../service/table.service';
import { TableDto } from '../model/table-dto';
import { AlertService } from '../service/alert.service';
import { faUserEdit, faReply, faCheckCircle,faTrash,faTimes} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-gestion-mon-compte-update',
  templateUrl: './gestion-mon-compte-update.component.html',
  styleUrls: ['./gestion-mon-compte-update.component.css']
})
export class GestionMonCompteUpdateComponent implements OnInit {
  user = localStorage.getItem("current_user");
  objJson = JSON.parse(this.user);
  res: string = "";
  utilisateur = new UtilisateurDto();
  update: any
  table = new TableDto();
  resa: number = 0;
  faClose=faTimes
  faEdit=faUserEdit;
  faTrash=faTrash
  faDoor=faReply;
  
  faCheck=faCheckCircle;
  

  constructor(private tableService: TableService, private utilisateurService: UtilisateurService, private router: Router,private alertService: AlertService) { }

  ngOnInit() {
    if (localStorage.getItem('current_user') == null) {
      this.router.navigateByUrl("/login")
    }
    window.scrollTo(0,0)

    this.utilisateurService.getUtilisateur(this.objJson.id).subscribe(
      data => {
        this.utilisateur = data;
      }
    )

    

  }

  onSubmit() {
    var reservat:boolean=false
    if (localStorage.getItem('age') === '18') {
      document.getElementById("general").style.webkitFilter = 'blur(0px)'
    }
    if (this.utilisateur.reservation === true) {
      
      this.resa = 1
    }

    this.tableService.getTableParUtilisateur(this.objJson.id).subscribe(
      data=>{
        this.tableService.getTableParUtilisateur(this.objJson.id).subscribe(
          data1=>{
            this.table=data1;

            if(this.table.id!=undefined){
            
              this.tableService.updateResa(this.objJson.id, this.table.id, this.resa,20).subscribe(
                data => {
                  this.update = data;
                  this.alertService.addSuccess("votre reservation a bien été annulée");
                  reservat=true;
                }
              );
            }

            
          }
        )
      }
    )
     
    this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe(
      donnee => {
        this.utilisateur = donnee;
        if(reservat){
          this.alertService.addSuccess("votre profil a bien été mis à jour")
        }
      }
    )
    
    this.router.navigateByUrl('/mon-compte')
  }
  edit(){
    document.getElementById("btnEdit").style.display="none"
    document.getElementById("info").style.display="none"
    document.getElementById("edit").style.display="block"
    document.getElementById("btnValid").style.display="block"
  }
  
  
  delete(){
    this.utilisateurService.deleteUtilisateur(this.utilisateur.id).subscribe(
      data=>{
        this.alertService.addSuccess('Votre compte a bien été supprimé')
        this.router.navigateByUrl('accueil')
        localStorage.clear()
      }
    )

  }
}
