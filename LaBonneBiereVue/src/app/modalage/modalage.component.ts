import { Component, OnInit, Input } from '@angular/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { AlertService } from '../service/alert.service';
import { ToastRef } from 'ngx-toastr';

@Component({
  selector: 'app-modalage',
  templateUrl: './modalage.component.html',
  styleUrls: ['./modalage.component.css']
})
export class ModalageComponent implements OnInit {

  faUser = faUser;
  date: Date
  public age: number;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    
    this.age = 0;
    this.date = new Date();
    // this.toast()
  }
  // toast(){
  //  var age=localStorage.getItem('age')
  //  if(age==null){
  //      this.alertService.addPrimary('Veuillez saisir votre age')
  //  }
  // }

  public CalculateAge(): void {
    if (this.date) {
      var timeDiff = Date.now() - new Date(this.date).getTime();
      this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      if (this.age >= 18) {
        localStorage.setItem("age", '18');
        document.body.style.webkitFilter = 'blur(0px)';
        document.getElementById('general').style.transition = '1s';
        document.getElementById('general').style.webkitFilter = 'blur(0px)';
        document.getElementById('id104').style.display = 'none';
      } else {
        this.alertService.addDanger("Vous devez avoir plus de 18 ans")

      }
    }
  }
}
