package com.afpa.labonnebiere.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BiereDto {
	
	private Integer id;
	private String nom;
	private String description;
	private Double degre;
	private Double prix;
	private Integer quantite;
	private TypeDeBiereDto typeDeBiereDto;
	private Integer commande;
	private Boolean active;
	private ImageBiereDto imageBiere;
	
	
	
	
	

}
