package com.afpa.labonnebiere.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PanierDto {
	
	
	
	
	private Integer id;
	private String nom;
	private double totalpanier;
	private List<CommandeDto> listDeCommande;
	

}
