package com.afpa.labonnebiere.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TableResaDto {



	private Integer id;
	private String nom;
	private Boolean reservationEnCours;
	private String description;
	private Integer capacite;
	private Boolean active;
	private double heure;
	private Integer nbResa;
	private Integer utilisateur;
	private ImageTableDto imageTable;



}
