package com.afpa.labonnebiere.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.afpa.labonnebiere.entity.Biere;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HistoriqueDAchatDto {
	
	
	
	private Integer id;
	private Integer idUser;
	private Date dateAchat;
	private Double prixTotal;
	private String biere;
	private Integer idBiere;
	private String typeDeBiere;
	
	private List<Biere> listDeBiere;
}
	

