package com.afpa.labonnebiere.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CommandeDto {
	
	

	private Integer id;
	private String nom;
	private Double total;
	private Integer idBiere;
	private Integer commande;
	private double prixUnitaire;
	
	
	private UtilisateurDto utilisateurdto;
	
	

}
