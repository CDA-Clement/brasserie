package com.afpa.labonnebiere.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CommentaireDto {
	
	private Integer id;
	private String nom;
	private String message;
	private Date date;
	private BiereDto biere;

}
