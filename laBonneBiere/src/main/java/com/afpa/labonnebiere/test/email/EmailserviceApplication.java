package com.afpa.labonnebiere.test.email;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.ws.soap.AddressingFeature.Responses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.ReponseDto;
import com.afpa.labonnebiere.dto.ReponseStatut;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;

@RestController
public class EmailserviceApplication {

	private String username = "clement.leuridon@gmail.com";
	private String password = "cyby9t61";
	@Autowired
	IUtilisateurService utilisateurService;
	
	@RequestMapping(value="/send", method=RequestMethod.POST)
	public String sendEmail(@RequestBody EmailMessage emailmessage) throws AddressException, MessagingException, IOException {
		sendmail(emailmessage);
		return "Email sent successfully";
	}
	
	@RequestMapping(value="/send/{id}/{validation}", method=RequestMethod.GET)
	public ReponseDto verificationCompte(
									@PathVariable String validation,
									@PathVariable int id,
									EmailMessage emailmessage) throws AddressException, MessagingException, IOException {
		String validate="expired";
		Optional<UtilisateurDto> u=utilisateurService.findById(id);
		if(u.isPresent()&& u.get().getPassword().substring(0, 5).equals(validation)) {
			if(u.get().getActivated()) {
				validate="validated";
				return ReponseDto.builder()
						.status(ReponseStatut.OK)
						.msg(validate).build();
			}
			
				u.get().setActif(true);
				u.get().setActivated(true);
				u.get().setHeureActivation(new Date().getTime());
				utilisateurService.Maj(u.get());
				EmailMessage email = new EmailMessage();
				email.setTo_address(u.get().getAdresseMail());
				email.setSubject("Validation de compte");
				email.setBody("Votre compte a bien été validé, Bienvenue "+u.get().getUsername());
				sendmail(email);
				validate="validation";
				return ReponseDto.builder()
						.status(ReponseStatut.OK)
						.msg(validate).build(); 			
		}
		return ReponseDto.builder()
				.status(ReponseStatut.OK)
				.msg(validate).build(); 
		
		
	}
	
	
	private void sendmail(EmailMessage emailmessage) throws AddressException, MessagingException, IOException {
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		
		Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
		
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(username, false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailmessage.getTo_address()));
		msg.setSubject(emailmessage.getSubject());
		msg.setContent(emailmessage.getBody(), "text/html");
		msg.setSentDate(new Date());
		
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(emailmessage.getBody(), "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		//MimeBodyPart attachPart = new MimeBodyPart();

		//attachPart.attachFile("/Users/clement/Desktop/barbar.jpeg");

		//multipart.addBodyPart(attachPart);
		msg.setContent(multipart);
//		 sends the e-mail
		Transport.send(msg);
		
	}
	
public void sendInvoice(EmailMessage emailmessage,String filepath) throws AddressException, MessagingException, IOException {
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		
		Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
		
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(username, false));

		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailmessage.getTo_address()));
		msg.setSubject(emailmessage.getSubject());
		msg.setContent(emailmessage.getBody(), "text/html");
		msg.setSentDate(new Date());
		
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(emailmessage.getBody(), "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		MimeBodyPart attachPart = new MimeBodyPart();

		attachPart.attachFile(filepath);

		multipart.addBodyPart(attachPart);
		msg.setContent(multipart);
//		 sends the e-mail
		Transport.send(msg);
		
	}
}
