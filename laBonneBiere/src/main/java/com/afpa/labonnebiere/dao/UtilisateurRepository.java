package com.afpa.labonnebiere.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.Commande;
import com.afpa.labonnebiere.entity.Utilisateur;



@Repository
public interface UtilisateurRepository extends PagingAndSortingRepository<Utilisateur, Integer> {

	//public List<Utilisateur> findAll();
	
	public Page<Utilisateur> findAll(Pageable firstPageWithTwoElements);

	Optional<Utilisateur> findUserByUsername(String username);
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.role = :id", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursByRole (Pageable firstPageWithFiveElements,int id);
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.role = :id and actif= :active", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursByRoleActif (Pageable firstPageWithFiveElements,int id, Boolean active);

	public Optional<Utilisateur> findByUsername(String cap);
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.reservation = true", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursAvecReservationActive();
	
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.actif = :active", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursActive(Pageable firstPageWithFiveElements, Boolean active);

	@Query(value = "SELECT * FROM Utilisateur u WHERE u.role = :id", 
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursByRole(Integer id);
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.username LIKE %:nom%",
			  nativeQuery = true)
	public List<Utilisateur>findAllUtilisateurParNom(@Param(value = "nom") String nom);
	
	@Query(value = "SELECT * FROM Utilisateur u WHERE u.activated = false",
			  nativeQuery = true)
	public List<Utilisateur> UtilisateursNeverActivated();
	
	
//	@Query(value = "SELECT * FROM Utilisateur u WHERE u.role = :role", 
//			  nativeQuery = true)
//	public Collection<Commande> findByFilter(Pageable firstPageWithFiveElements, int role);
	

	
	
	

}