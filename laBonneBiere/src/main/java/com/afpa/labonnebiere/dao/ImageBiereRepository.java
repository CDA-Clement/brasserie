package com.afpa.labonnebiere.dao;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.ImageBiere;

@Repository
public interface ImageBiereRepository extends PagingAndSortingRepository<ImageBiere, Integer> {
	
	public Page<ImageBiere> findAll(Pageable pageable);
	
	public Optional<ImageBiere> findByNom(String nom);
	
	@Query(value = "SELECT * FROM IMAGE_BIERE b WHERE b.biere = :id",
			  nativeQuery = true)
	public List<ImageBiere>findBiereImage(@Param(value = "id") int id);
	
	
	
	

}
