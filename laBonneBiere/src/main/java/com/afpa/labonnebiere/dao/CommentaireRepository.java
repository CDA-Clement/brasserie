package com.afpa.labonnebiere.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.Commentaire;



@Repository
public interface CommentaireRepository extends PagingAndSortingRepository<Commentaire, Integer> {

	public List<Commentaire> findAll();
	
	public Page<Commentaire> findAll(Pageable firstPageWithTwoElements);
	@Query(value = "SELECT * FROM Commentaire c WHERE c.biere = :id", 
			  nativeQuery = true)
	List<Commentaire> commentaireParBiere(int id);
	
	@Query(value = "SELECT * FROM Commentaire c WHERE c.nom = :username", 
			  nativeQuery = true)
	List<Commentaire>  findAllByNom(String username);

	public Collection<Commentaire> findAllByBiere_id(int id);

	
}