package com.afpa.labonnebiere.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import com.afpa.labonnebiere.entity.HistoriqueDAchat;



@Repository
public interface HistoriquedAchatRepository extends PagingAndSortingRepository<HistoriqueDAchat, Integer> {

	public List<HistoriqueDAchat> findAll();
	public Optional<HistoriqueDAchat> findById(int id);
	public Optional<HistoriqueDAchat> findByBiere(String biere);
	
	@Query(value = "SELECT * FROM historiquedachat b WHERE b.id_user= :idUser and b.id= :idFacture",
			  nativeQuery = true)
	public Optional<HistoriqueDAchat> findByIdByUser(int idFacture, int idUser);
	
	
	@Query(value = "SELECT * FROM historiquedachat b WHERE b.id_user= :idUser and b.prix_total= :montant",
			  nativeQuery = true)
	public List<HistoriqueDAchat> findByMontant(int idUser, double montant);
	
	public Page<HistoriqueDAchat> findByidUser(Pageable firt,int idUser);
	
	public List<HistoriqueDAchat> findByidUser(int idUser);
	
	public Page<HistoriqueDAchat> findAll(Pageable firstPageWithTwoElements);
	
	@Query(value = "SELECT * FROM historiquedachat b WHERE b.id_user= :user and b.date_achat between :debut and :fin",
			  nativeQuery = true)
	public Page<HistoriqueDAchat> findByDateAchatBetween(Pageable firt,int user,String debut,String fin);
	
	@Query(value = "SELECT * FROM historiquedachat b WHERE b.id_user= :user and b.date_achat between :debut and :fin",
			  nativeQuery = true)
	public List<HistoriqueDAchat> findAllByDateAchatBetween(int user,String debut,String fin);
	
	@Query(value = "SELECT * FROM historiquedachat b WHERE b.id_user= :user and b.date_achat >= :debut",
			  nativeQuery = true)
	public Page<HistoriqueDAchat> findByDateAchatAfter(Pageable firt,int user,String debut);
	
	@Query(value = "SELECT * FROM historiquedachat b WHERE b.id_user= :user and b.date_achat <= :fin",
			  nativeQuery = true)
	public Page<HistoriqueDAchat> findByDateAchatBefore(Pageable firt,int user,String fin);
	
//	@Query(value = "select biere, type_de_biere, id_user , count(*) from historiquedachat group by biere, type_de_biere, id_user order by count(*) desc",
//			  nativeQuery = true)
//	public List<BiereFavoriteDto> findMostOccurence();
	

	
}