package com.afpa.labonnebiere.dao;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.TableResa;

@Repository
public interface TableResaRepository extends CrudRepository<TableResa, Integer> {
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.capacite between :capacite and 50", nativeQuery = true)
	public List<TableResa> findAll(int capacite);
	
	public Optional<TableResa> findByNom(String nom);
	
	
	@Query("SELECT t FROM  TableResa t WHERE t.reservationEnCours = :disponible")
	public List<TableResa>findAlltableParDisponible( Boolean disponible);
	
	@Query(value = "SELECT * FROM Table_Resa t WHERE t.utilisateur = :utilisateur", 
			  nativeQuery = true)
	public Optional<TableResa>findtableParUtilisateur(Integer utilisateur);
	
	@Query("SELECT t FROM  TableResa t WHERE t.reservationEnCours = true")
	public List<TableResa>findAlltableReservee();
	
	@Query("SELECT t FROM  TableResa t WHERE t.utilisateur = :iduser")
	public List<TableResa>findAlltableReservee(Integer iduser);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.reservation_En_Cours= :resa and t.capacite between :capacite and 50", nativeQuery = true)
	public List<TableResa>findAlltablefiltre( Boolean active, Boolean resa, int capacite);
	
	
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.reservation_en_cours= :resa and t.capacite between :capacite and 50 order by nom", nativeQuery = true)
	public List<TableResa>findAlltablefiltreOrderNom( Boolean active, Boolean resa, int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.capacite between :capacite and 50 order by nom", nativeQuery = true)
	public List<TableResa>findAlltableOrderNom( int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.capacite between :capacite and 50 order by nom", nativeQuery = true)
	public List<TableResa>findAlltableActiveOrderNom( Boolean active, int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.reservation_en_cours= :resa and t.capacite between :capacite and 50 order by nom", nativeQuery = true)
	public List<TableResa>findAlltableResaOrderNom( Boolean resa, int capacite);
	
	
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.reservation_en_cours= :resa and t.capacite between :capacite and 50 order by capacite", nativeQuery = true)
	public List<TableResa>findAlltablefiltreOrderCapacite( Boolean active, Boolean resa, int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.capacite between :capacite and 50 order by capacite", nativeQuery = true)
	public List<TableResa>findAlltableOrderCapacite( int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.capacite between :capacite and 50 order by capacite", nativeQuery = true)
	public List<TableResa>findAlltableActiveOrderCapacite( Boolean active, int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.reservation_en_cours= :resa and t.capacite between :capacite and 50 order by capacite", nativeQuery = true)
	public List<TableResa>findAlltableResaOrderCapacite( Boolean resa, int capacite);
	
	
	
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.reservation_en_cours= :resa and t.capacite between :capacite and 50 order by Nb_Resa", nativeQuery = true)
	public List<TableResa>findAlltablefiltreOrderReservation( Boolean active, Boolean resa, int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.capacite between :capacite and 50 order by Nb_Resa", nativeQuery = true)
	public List<TableResa>findAlltableOrderReservation( int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active and t.capacite between :capacite and 50 order by Nb_Resa", nativeQuery = true)
	public List<TableResa>findAlltableActiveOrderReservation( Boolean active,  int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.reservation_en_cours= :resa and t.capacite between :capacite and 50 order by Nb_Resa", nativeQuery = true)
	public List<TableResa>findAlltableResaOrderReservation( Boolean resa,  int capacite);
	
	
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active = :active  and t.capacite between :capacite and 50", nativeQuery = true)
	public List<TableResa>findAllTableActive( Boolean active, int capacite);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.reservation_En_Cours= :resa  and t.capacite between :capacite and 50", nativeQuery = true)
	public List<TableResa>findAllTableResa( Boolean resa, int capacite);
	
	@Query(value = "SELECT * FROM Table_Resa t WHERE t.nom LIKE %:nom%",
			  nativeQuery = true)
	public List<TableResa>findAlltableParNom(@Param(value = "nom") String nom);
	
	@Query(value = "SELECT * FROM  Table_Resa t WHERE t.active=true", nativeQuery = true)
	public Page<TableResa> findAllbyPage(Pageable firstPageWithFiveElements);
}

