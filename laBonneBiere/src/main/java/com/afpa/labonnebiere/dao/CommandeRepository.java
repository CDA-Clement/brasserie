package com.afpa.labonnebiere.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.afpa.labonnebiere.entity.Commande;

@Repository
public interface CommandeRepository extends PagingAndSortingRepository<Commande, Integer>{
	public List<Commande> findAll();
	
	@Query(value = "SELECT * FROM Commande c WHERE c.utilisateur = :id", 
			  nativeQuery = true)
	public List<Commande> findAllCommandeByUtilisateur(Integer id);
	
	
	@Query(value = "SELECT * FROM Commande c WHERE c.utilisateur = :id AND c.nom=:nom", 
			  nativeQuery = true)
	public Optional<Commande> findByNom(int id,String nom);
	
	

	@Modifying
	@Query(value = "delete from PANIER_LIST_DE_COMMANDE where panier_id=:id", nativeQuery = true)
	public void suppressionTableJointureListDeCommande(int id);
	
	
	@Modifying
	@Query(value = "delete from PANIER_LIST_DE_COMMANDE where LIST_DE_COMMANDE_ID=:id", nativeQuery = true)
	public void suppressionTableJointureListDeCommande2(int id);
	

}
