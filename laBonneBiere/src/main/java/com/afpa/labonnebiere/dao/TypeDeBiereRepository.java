package com.afpa.labonnebiere.dao;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.TypeDeBiere;

@Repository
public interface TypeDeBiereRepository extends PagingAndSortingRepository<TypeDeBiere, Integer> {
	
	public Page<TypeDeBiere> findAll(Pageable pageable);

	public Optional<TypeDeBiere> findByNom(String type);
	public Optional<TypeDeBiere> findById(Integer id);
	

}
