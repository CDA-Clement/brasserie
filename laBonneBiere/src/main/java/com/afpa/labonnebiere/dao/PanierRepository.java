package com.afpa.labonnebiere.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.Panier;



@Repository
public interface PanierRepository extends PagingAndSortingRepository<Panier, Integer> {

	public List<Panier> findAll();
	
	public Page<Panier> findAll(Pageable firstPageWithTwoElements);

	public Optional<Panier>findById(int id);
	
}