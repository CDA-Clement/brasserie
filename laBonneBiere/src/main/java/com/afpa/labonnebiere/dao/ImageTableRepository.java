package com.afpa.labonnebiere.dao;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.ImageBiere;
import com.afpa.labonnebiere.entity.ImageTable;

@Repository
public interface ImageTableRepository extends PagingAndSortingRepository<ImageTable, Integer> {
	
	public Page<ImageTable> findAll(Pageable pageable);
	
	public Optional<ImageTable> findByNom(String nom);
	
//	@Query(value = "SELECT * FROM IMAGE_Table b WHERE b.table = :id",
//			  nativeQuery = true)
//	public Optional<ImageTable>findTableImage(@Param(value = "id") int id);
	
	
	
	

}
