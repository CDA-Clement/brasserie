package com.afpa.labonnebiere.dao;


import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.entity.Biere;

@Repository
public interface BiereRepository extends PagingAndSortingRepository<Biere, Integer> {
	
	@Query(value = "SELECT * FROM Biere b",
			  nativeQuery = true)
	public List<Biere>findAll();
	
	@Query(value = "SELECT * FROM Biere b where b.active=true",
			  nativeQuery = true)
	public List<Biere>findAllActiveSansPage();
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax",
			  nativeQuery = true)
	public Page<Biere> findAll(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax
								);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active ",
			  nativeQuery = true)
	public Page<Biere> findAllActive(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax,
								@Param(value = "active") boolean active
								);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by nom ",
			  nativeQuery = true)
	public Page<Biere> findAllOrderNom(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax
								);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by nom ",
			  nativeQuery = true)
	public Page<Biere> findAllOrderNomActive(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax,
								@Param(value = "active") boolean active);
								
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by quantite",
			  nativeQuery = true)
	public Page<Biere> findAllOrderQteActive(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax,
								@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by quantite",
			  nativeQuery = true)
	public Page<Biere> findAllOrderQte(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by prix",
			  nativeQuery = true)
	public Page<Biere> findAllOrderPrixActive(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax,
								@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by prix",
			  nativeQuery = true)
	public Page<Biere> findAllOrderPrix(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by degre",
			  nativeQuery = true)
	public Page<Biere> findAllOrderdegreActive(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax,
								@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by degre",
			  nativeQuery = true)
	public Page<Biere> findAllOrderdegre(
								Pageable pageable,
								@Param(value = "prixMin") float prixMin,
								@Param(value = "prixMax") float prixMax,
								@Param(value = "qteMin") int qteMin,
								@Param(value = "qteMax") int qteMax,
								@Param(value = "alcMin") float alcMin, 
								@Param(value = "alcMax") float alcMax);
	
	
	public Optional<Biere> findByNom(String nom);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.nom LIKE %:nom%",
			  nativeQuery = true)
	public List<Biere>findAllbiereParNom(@Param(value = "nom") String nom);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active ", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereActive(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax,
											@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax ", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiere(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by nom", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderNomActive(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax,
											@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by nom", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderNom(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by degre", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderDegreActive(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax,
											@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by degre", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderDegre(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by prix", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderPrixActive(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax,
											@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by prix", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderPrix(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax and b.active=:active order by quantite", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderQteActive(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax,
											@Param(value = "active") boolean active);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id and b.prix between :prixMin and :prixMax and b.quantite between :qteMin and :qteMax and b.degre between :alcMin and :alcMax order by quantite", 
			  nativeQuery = true)
	public List<Biere>findAllByTypeDeBiereOrderQte(
											Integer id,
											@Param(value = "prixMin") float prixMin, 
											@Param(value = "prixMax") float prixMax, 
											@Param(value = "qteMin") int qteMin,
											@Param(value = "qteMax") int qteMax,
											@Param(value = "alcMin") float alcMin, 
											@Param(value = "alcMax") float alcMax);
	
	
	
	
	@Query(value = "SELECT * FROM Biere b WHERE b.active = true", 
			  nativeQuery = true)
	public List<Biere>findAllActive(Pageable pageable);
	
	@Query(value = "SELECT * FROM Biere b WHERE b.active = false", 
			  nativeQuery = true)
	public List<Biere>findAllNonActive(Pageable pageable);

	@Query(value = "SELECT * FROM Biere b WHERE b.typedebiere = :id", 
			  nativeQuery = true)
	public List<Biere> findAllByTypeDeBiereSup(Integer id);

	@Query(value = "SELECT * FROM Biere b WHERE b.nom LIKE %:nom% and b.active=true",
			 nativeQuery = true)
	public List<Biere> findAllbiereParNomClient(@Param(value = "nom") String nom);
	
	
	
	
}
