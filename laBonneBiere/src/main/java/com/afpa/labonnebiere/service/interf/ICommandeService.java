package com.afpa.labonnebiere.service.interf;

import java.util.List;

import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.entity.Commande;

public interface ICommandeService {
	
	public List<CommandeDto> chercherTouteLesCommandes();

	public Commande chercherUneCommande(int id);

	Boolean supprimerId(int id);


	Integer ajouter(CommandeDto prod, int id);

	Integer Maj(CommandeDto commentaire);
	
	public List<CommandeDto>AfficherCommandesParUtilisateur(int id);

	public void suppressionTableJointureListDeCommande(int id);
	
	public void suppressionTableJointureListDeCommande2(int id);

	


	

}
