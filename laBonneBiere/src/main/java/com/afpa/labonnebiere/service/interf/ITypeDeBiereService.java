package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import com.afpa.labonnebiere.dto.TypeDeBiereDto;

public interface ITypeDeBiereService {
	
	public List<TypeDeBiereDto> chercherToutsLesTypesDeBieres(int page);

	public Optional<TypeDeBiereDto> findById(int id);

	Boolean supprimerId(int id);


	Integer ajouter(TypeDeBiereDto prod);

	Integer Maj(TypeDeBiereDto prod);

	public Optional<TypeDeBiereDto> trouverParNom(String type);
}


