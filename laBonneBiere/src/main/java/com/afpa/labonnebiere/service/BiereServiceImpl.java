package com.afpa.labonnebiere.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.controller.ControllerBiere;
import com.afpa.labonnebiere.dao.BiereRepository;
import com.afpa.labonnebiere.dao.CommentaireRepository;
import com.afpa.labonnebiere.dao.ImageBiereRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.ImageBiereDto;
import com.afpa.labonnebiere.dto.TypeDeBiereDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.ImageBiere;
import com.afpa.labonnebiere.service.interf.IBiereService;


@Service
public class BiereServiceImpl implements IBiereService {
	static int INC = 6;
	public static int fin;
	private final static Logger logger = Logger.getLogger(BiereServiceImpl.class);

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private BiereRepository biereRepository;

	@Autowired
	private ImageBiereRepository imageBiereRepository;


	@Override
	public List<BiereDto> chercherToutesLesBieres(int page,float prixMin,float prixMax, int qteMin, int qteMax, float alcMin, float alcMax, Boolean active) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);

		if(active==null) {
			List<BiereDto> maliste = this.biereRepository.findAll(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax)
					.stream()
					.map(e->BiereDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.prix(e.getPrix())
							.quantite(e.getQuantite())
							.degre(e.getDegre())
							.active(e.getActive())
							.commande(e.getCommande())
							.description(e.getDescription())
							.typeDeBiereDto(TypeDeBiereDto.builder()
									.id(e.getTypeDeBiere().getId())
									.nom(e.getTypeDeBiere().getNom()).build())
							.build())
					.collect(Collectors.toList());
			fin = maliste.size();
			logger.info("chercher toutes les bieres "+page);
			return maliste;

		}else {
			List<BiereDto> maliste = this.biereRepository.findAllActive(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax,active)
					.stream()
					.map(e->BiereDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.prix(e.getPrix())
							.quantite(e.getQuantite())
							.degre(e.getDegre())
							.active(e.getActive())
							.commande(e.getCommande())
							.description(e.getDescription())
							.typeDeBiereDto(TypeDeBiereDto.builder()
									.id(e.getTypeDeBiere().getId())
									.nom(e.getTypeDeBiere().getNom()).build())
							.build())
					.collect(Collectors.toList());
			fin = maliste.size();
			logger.info("chercher toutes les bieres "+page);
			return maliste;

		}




	}

	@Override
	public List<BiereDto> chercherToutesLesBieresOrder(int page,float prixMin,float prixMax, int qteMin, int qteMax, float alcMin, float alcMax, String sorting, Boolean active) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		if(sorting.equals("nom")) {
			if(active==null) {
				List<BiereDto> maliste = this.biereRepository.findAllOrderNom(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}else {
				List<BiereDto> maliste = this.biereRepository.findAllOrderNomActive(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}


		}

		if(sorting.equals("degre")) {
			if(active==null) {
				List<BiereDto> maliste = this.biereRepository.findAllOrderdegre(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}else {
				List<BiereDto> maliste = this.biereRepository.findAllOrderdegreActive(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}
		}

		if(sorting.equals("prix")) {
			if(active==null) {
				List<BiereDto> maliste = this.biereRepository.findAllOrderPrix(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}else {
				List<BiereDto> maliste = this.biereRepository.findAllOrderPrixActive(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}
		}
		if(sorting.equals("quantite")) {
			if(active==null) {
				List<BiereDto> maliste = this.biereRepository.findAllOrderQte(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}else {
				List<BiereDto> maliste = this.biereRepository.findAllOrderQteActive(firstPageWithFiveElements,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				logger.info("chercher toutes les bieres "+page);
				return maliste;

			}

		}
		return new ArrayList<BiereDto>();


	}


	@Override
	public List<BiereDto> chercherToutesLesBieresActive(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);


		List<BiereDto> maliste = this.biereRepository.findAllActive(firstPageWithFiveElements)
				.stream()
				.map(e->BiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.active(e.getActive())
						.quantite(e.getQuantite())
						.degre(e.getDegre())
						.commande(e.getCommande())
						.description(e.getDescription())
						.typeDeBiereDto(TypeDeBiereDto.builder()
								.id(e.getTypeDeBiere().getId())
								.nom(e.getTypeDeBiere().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher toutes les bieres actives "+page);
		return maliste;
	}

	@Override
	public List<BiereDto> chercherToutesLesBieresNonActive(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);


		List<BiereDto> maliste = this.biereRepository.findAllNonActive(firstPageWithFiveElements)
				.stream()
				.map(e->BiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.active(e.getActive())
						.quantite(e.getQuantite())
						.degre(e.getDegre())
						.commande(e.getCommande())
						.description(e.getDescription())
						.typeDeBiereDto(TypeDeBiereDto.builder()
								.id(e.getTypeDeBiere().getId())
								.nom(e.getTypeDeBiere().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher toutes les bieres Non Actives "+page);
		return maliste;
	}


	@Override
	public Optional<BiereDto> findById(int id) {
		Optional<Biere> prod = this.biereRepository.findById(id);
		Optional<BiereDto> res = Optional.empty();
		if(prod.isPresent()) {
			Biere p = prod.get();
			logger.info("findbyid " + id);
			BiereDto prodDto = this.modelMapper.map(p, BiereDto.class);
			prodDto.setTypeDeBiereDto(TypeDeBiereDto.builder()
					.id(p.getTypeDeBiere().getId())
					.nom(p.getTypeDeBiere().getNom()).build());
			res = Optional.of(prodDto);
		}
		return res;
	}

	@Override
	public Boolean supprimerId(int id) {
		if(this.biereRepository.existsById(id)) {
			logger.info("supression de la biere "+id);
			List<ImageBiere>images=this.imageBiereRepository.findBiereImage(this.biereRepository.findById(id).get().getId());
			for (ImageBiere imageBiere : images) {
				this.imageBiereRepository.delete(imageBiere);
			}
			this.biereRepository.deleteById(id);
			return true;
		}else {
			logger.warn("suppression biere "+id+" inexistante");
			return false;
		}
	}

	@Override
	public Integer ajouter(BiereDto biere) {
		Boolean excep=false;
		String cap = biere.getNom().substring(0, 1).toUpperCase() + biere.getNom().substring(1);
		biere.setNom(cap);
		Biere p = this.modelMapper.map(biere,Biere.class);
		try {
			List<ImageBiere> biereImage = this.imageBiereRepository.findBiereImage(p.getId());
			p.setImageBiere(biereImage);
		}catch(Exception e){
			excep=true;
		}
		if(excep==true){
			p.setImageBiere(new ArrayList<ImageBiere>());
		}
		this.biereRepository.save(p);

		logger.info("la biere "+p.getNom()+" a bien été enregistrée");
		return p.getId();
	}


	@Override
	public Optional<BiereDto> trouverParnom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);

		Optional<Biere> prod = this.biereRepository.findByNom(cap);
		Optional<BiereDto> res = Optional.empty();
		if(prod.isPresent()) {
			Biere p = prod.get();
			BiereDto prodDto = this.modelMapper.map(p, BiereDto.class);
			prodDto.setTypeDeBiereDto(TypeDeBiereDto.builder()
					.id(p.getTypeDeBiere().getId())
					.nom(p.getTypeDeBiere().getNom()).build());

			res = Optional.of(prodDto);
		}
		return res;	}
	
	

	@Override
	public List<BiereDto> listerParTypeDeBiere(Integer id, float prixMin, float prixMax, int qteMin, int qteMax, float alcMin, float alcMax, Boolean active) {
		if(active==null) {
			List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiere(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax)
					.stream()
					.map(e->BiereDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.prix(e.getPrix())
							.quantite(e.getQuantite())
							.degre(e.getDegre())
							.active(e.getActive())
							.commande(e.getCommande())
							.description(e.getDescription())
							.typeDeBiereDto(TypeDeBiereDto.builder()
									.id(e.getTypeDeBiere().getId())
									.nom(e.getTypeDeBiere().getNom()).build())
							.build())
					.collect(Collectors.toList());
			fin = maliste.size();
			return maliste;

		}else {
			List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereActive(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax, active)
					.stream()
					.map(e->BiereDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.prix(e.getPrix())
							.quantite(e.getQuantite())
							.degre(e.getDegre())
							.active(e.getActive())
							.commande(e.getCommande())
							.description(e.getDescription())
							.typeDeBiereDto(TypeDeBiereDto.builder()
									.id(e.getTypeDeBiere().getId())
									.nom(e.getTypeDeBiere().getNom()).build())
							.build())
					.collect(Collectors.toList());
			fin = maliste.size();
			return maliste;
		}




	}

	@Override
	public List<BiereDto> listerParNom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		List<BiereDto> maliste = this.biereRepository.findAllbiereParNom(cap)
				.stream()
				.map(e->BiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
						.degre(e.getDegre())
						.active(e.getActive())
						.commande(e.getCommande())
						.description(e.getDescription())
						.typeDeBiereDto(TypeDeBiereDto.builder()
								.id(e.getTypeDeBiere().getId())
								.nom(e.getTypeDeBiere().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;	
	}
	
	@Override
	public List<BiereDto> listerParNomClient(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		List<BiereDto> maliste = this.biereRepository.findAllbiereParNomClient(cap)
				.stream()
				.map(e->BiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
						.degre(e.getDegre())
						.active(e.getActive())
						.commande(e.getCommande())
						.description(e.getDescription())
						.typeDeBiereDto(TypeDeBiereDto.builder()
								.id(e.getTypeDeBiere().getId())
								.nom(e.getTypeDeBiere().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;		
		}


	@Override
	public Optional<ImageBiereDto> afficherImage(int id, String nomImage) {
		Optional<ImageBiereDto> res = Optional.empty();
		Optional<ImageBiereDto> res1 = Optional.empty();
		Optional<Biere>biere=this.biereRepository.findById(id);
		if(biere.isPresent()) {
			List<ImageBiere>img=this.imageBiereRepository.findBiereImage(id);
			if(img.size()>0) {
				for (ImageBiere imageBiere : img) {
					if(imageBiere.getNom().contains("Logo")) {
						ImageBiereDto i=ImageBiereDto.builder()
								.id(imageBiere.getId())
								.nom(imageBiere.getNom())
								.build();
						res = Optional.of(i);
					}else{
						ImageBiereDto i=ImageBiereDto.builder()
								.id(imageBiere.getId())
								.nom(imageBiere.getNom())
								.build();
						res1 = Optional.of(i);
					}
				}
			}
		}
		if(nomImage.contains("Logo")) {
			return res;
		}else {

			return res1;
		}
	}


	@Override
	public List<BiereDto> listerParTypeDeBiereOrder(Integer id, Float prixMin, Float prixMax, Integer qteMin,Integer qteMax, Float alcMin, Float alcMax, String sorting, Boolean active) {
		if(sorting.equals("rien")) {

			if(active==null) {

				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiere(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}else {
				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereActive(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}
		}if(sorting.equals("nom")) {
			if(active==null) {

				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderNom(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}else {
				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderNomActive(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;

			}
		}if(sorting.equals("degre")) {
			if(active==null) {

				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderDegre(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}else {
				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderDegreActive(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;

			}
		}if(sorting.equals("quantite")) {
			if(active==null) {

				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderQte(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}else {
				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderQteActive(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;

			}
		}if(sorting.equals("prix")) {
			if(active==null) {

				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderPrix(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}else {
				List<BiereDto> maliste = this.biereRepository.findAllByTypeDeBiereOrderQteActive(id, prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,active)
						.stream()
						.map(e->BiereDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.prix(e.getPrix())
								.quantite(e.getQuantite())
								.degre(e.getDegre())
								.active(e.getActive())
								.commande(e.getCommande())
								.description(e.getDescription())
								.typeDeBiereDto(TypeDeBiereDto.builder()
										.id(e.getTypeDeBiere().getId())
										.nom(e.getTypeDeBiere().getNom()).build())
								.build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}
		}
		return new ArrayList<BiereDto>();
	}

	@Override
	public List<BiereDto> findAll() {
		List<BiereDto> maliste = this.biereRepository.findAll()
				.stream()
				.map(e->BiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
						.degre(e.getDegre())
						.active(e.getActive())
						.commande(e.getCommande())
						.description(e.getDescription())
						.typeDeBiereDto(TypeDeBiereDto.builder()
								.id(e.getTypeDeBiere().getId())
								.nom(e.getTypeDeBiere().getNom()).build())
						.build())
				.collect(Collectors.toList());
		 
		
		return maliste;
	}

	@Override
	public List<BiereDto> chercherToutesLesBieresSansPage() {
		List<BiereDto> maliste = this.biereRepository.findAllActiveSansPage()
				.stream()
				.map(e->BiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.prix(e.getPrix())
						.quantite(e.getQuantite())
						.degre(e.getDegre())
						.active(e.getActive())
						.commande(e.getCommande())
						.description(e.getDescription())
						.typeDeBiereDto(TypeDeBiereDto.builder()
								.id(e.getTypeDeBiere().getId())
								.nom(e.getTypeDeBiere().getNom()).build())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
	
		return maliste;

	}

	
}



