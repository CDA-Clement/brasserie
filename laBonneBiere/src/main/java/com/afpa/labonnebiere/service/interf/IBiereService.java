package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.ImageBiereDto;

public interface IBiereService {
	
	public List<BiereDto> chercherToutesLesBieres(int page,float prixMin, float prixMax, int qteMin, int qteMax, float alcMin, float alcMax, Boolean active );
	public List<BiereDto> chercherToutesLesBieresActive(int page); 
	public List<BiereDto>findAll();

	public Optional<BiereDto> findById(int id);

	public Boolean supprimerId(int id);
	
	public Optional<BiereDto> trouverParnom(String nom);


	public Integer ajouter(BiereDto prod);

	public List<BiereDto> listerParTypeDeBiere(Integer id, float prixMin, float prixMax, int qteMin, int qteMax, float alcMin, float alcMax, Boolean active);
	
	public List<BiereDto> listerParNom(String nom);
	public List<BiereDto> listerParNomClient(String user);
	

	public Optional<ImageBiereDto> afficherImage(int id, String nomimage);
	public List<BiereDto> chercherToutesLesBieresNonActive(int page);
	
	public List<BiereDto> listerParTypeDeBiereOrder(Integer id, Float prixMin, Float prixMax, Integer qteMin,
			Integer qteMax, Float alcMin, Float alcMax, String sorting, Boolean active);
	
	public List<BiereDto> chercherToutesLesBieresOrder(int page, float prixMin, float prixMax, int qteMin, int qteMax,
			float alcMin, float alcMax, String sorting, Boolean active);
	
	public List<BiereDto> chercherToutesLesBieresSansPage();

	

}
