package com.afpa.labonnebiere.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.ImageTableRepository;
import com.afpa.labonnebiere.dao.TableResaRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.dto.ImageBiereDto;
import com.afpa.labonnebiere.dto.ImageTableDto;
import com.afpa.labonnebiere.dto.TableResaDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.ImageBiere;
import com.afpa.labonnebiere.entity.ImageTable;
import com.afpa.labonnebiere.entity.TableResa;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.service.interf.ITableResaService;



@Service
public class TableServiceImpl implements ITableResaService {
	static int INC = 1;
	public static int fin;
	private final static Logger logger = Logger.getLogger(TableServiceImpl.class);
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private TableResaRepository tableRepository;
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private ImageTableRepository imageTableRepository;

	@Override
	public List<TableResaDto> chercherToutesLesTables() {
		List<TableResaDto> maListe=this.tableRepository.findAll(0)
				.stream()
				.map(e->TableResaDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.reservationEnCours(e.getReservationEnCours())
						.capacite(e.getCapacite())
						.active(e.getActive())
						.nbResa(e.getNbResa())
						.description(e.getDescription())
						.heure(e.getHeure())
						.utilisateur(e.getUtilisateur())
						.build())
				.collect(Collectors.toList());
		
		return maListe ;
	}

	@Override
	public Optional<TableResaDto> findById(int id) {
		Optional<TableResa> optionaTable=this.tableRepository.findById(id);
		Optional<TableResaDto> res = Optional.empty();
		if(optionaTable.isPresent()) {
			TableResa t=optionaTable.get();
			TableResaDto tableDto=this.modelMapper.map(t,TableResaDto.class);
			res = Optional.of(tableDto);
			
		}
		return res;
	}

	@Override
	public Boolean supprimerId(int id) {
		Optional<TableResa> t=this.tableRepository.findById(id);
		if(t.isPresent()) {
			int tableResa=t.get().getImageTable().getId();
			this.tableRepository.delete(t.get());
			this.imageTableRepository.deleteById(tableResa);
		}
		return t.isPresent();
	}

	@Override
	public Optional<TableResaDto> trouverParnom(String nom) {
		Optional<TableResa> optionaTable=this.tableRepository.findByNom(nom);
		Optional<TableResaDto> res = Optional.empty();
		if(optionaTable.isPresent()) {
			TableResa t=optionaTable.get();
			TableResaDto tableDto=this.modelMapper.map(t,TableResaDto.class);
			res = Optional.of(tableDto);
			
		}
		return res;
	}

	@Override
	public Integer ajouter(TableResaDto table) {
		String cap = table.getNom().substring(0, 1).toUpperCase() + table.getNom().substring(1);
		table.setNom(cap);
		TableResa t=this.modelMapper.map(table,TableResa.class);
		return this.tableRepository.save(t).getId();
		
	}

	@Override
	public List<TableResaDto> listerParDisponibilite(Boolean disponible) {
		List<TableResaDto> maListe=this.tableRepository.findAlltableParDisponible(disponible)
				.stream()
				.map(e->TableResaDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.active(e.getActive())
						.reservationEnCours(e.getReservationEnCours())
						.build())
				.collect(Collectors.toList());
		
		return maListe ;
	}

	@Override
	public Boolean reservationTable(Integer resa, Integer idTable, Integer idUser, Integer heure) {
		Boolean reservation=false;
		if(resa==1) {
			reservation=true;
		}
		
		if(resa!=1&&resa!=0) {
			return false;
		}
		Optional<TableResa> optioTable =this.tableRepository.findById(idTable);
		Optional<Utilisateur>optioUser=this.utilisateurRepository.findById(idUser);
		if(optioUser.isPresent()) {
			Utilisateur p=optioUser.get();
			if(optioTable.isPresent()) {
				TableResa table=optioTable.get();
				table.setReservationEnCours(reservation);
				if(reservation) {
					p.setReservation(true);
					table.setUtilisateur(p.getId());
					table.setNbResa(table.getNbResa()+1);
					table.setHeure(heure);
					this.tableRepository.save(table);
					logger.info("la table "+table.getNom()+" à bien été reservée pour "+p.getNom());
				}else {
					Optional<TableResa> optioTable1 =this.tableRepository.findtableParUtilisateur(idUser);
					if(optioTable1.isPresent()) {
						TableResa table1=optioTable1.get();
						p.setReservation(false);
						table1.setUtilisateur(0);
						table1.setReservationEnCours(reservation);
						table.setNbResa(table.getNbResa()-1);
						table.setHeure(20.00);
						this.tableRepository.save(table1);
						logger.info("l'annumation de la table "+table1.getNom()+" à bien été effectuée par "+p.getNom());
					}
				}
				p.setReservation(reservation);
				this.utilisateurRepository.save(p);
				return true;
				
			}else {
				
				return false;
			}
			
		}else {
			return false;
		}
	}

	@Override
	public Boolean resetAll() {
		List<TableResa>ListeTable=this.tableRepository.findAlltableReservee();
		for (TableResa tableResa : ListeTable) {
			tableResa.setReservationEnCours(false);
			tableResa.setUtilisateur(null);
			tableResa.setHeure(20.00);
			this.tableRepository.save(tableResa);
		}
		List<Utilisateur>ListeUtilisateur=this.utilisateurRepository.UtilisateursAvecReservationActive();
		for (Utilisateur utilisateur : ListeUtilisateur) {
			utilisateur.setReservation(false);
			this.utilisateurRepository.save(utilisateur);
		}
		return true;
	}

	@Override
	public Optional<TableResaDto> afficherParNom(String nom) {
		Optional<TableResa> optionaTable=this.tableRepository.findByNom(nom);
		Optional<TableResaDto> res = Optional.empty();
		if(optionaTable.isPresent()) {
			TableResa t=optionaTable.get();
			TableResaDto tableDto=this.modelMapper.map(t,TableResaDto.class);
			res = Optional.of(tableDto);
			
		}
		return res;
	}
	@Override
	public Optional<TableResaDto>tableParUtilisateur(int id){
		Optional<TableResa> optTable=this.tableRepository.findtableParUtilisateur(id);
		Optional<TableResaDto> res = Optional.empty();
		if(optTable.isPresent()) {
			TableResa t=optTable.get();
			TableResaDto tableDto=this.modelMapper.map(t,TableResaDto.class);
			res = Optional.of(tableDto);
		}
		return res;
		
	}

	@Override
	public Integer update(TableResaDto table) {
		String cap = table.getNom().substring(0, 1).toUpperCase() + table.getNom().substring(1);
		table.setNom(cap);
		TableResa t=this.modelMapper.map(table,TableResa.class);
		return this.tableRepository.save(t).getId();
		 
	}
	
	@Override
	public Optional<ImageTableDto> afficherImage(int id, String nomImage) {
		Optional<ImageTableDto> res = Optional.empty();
		
		Optional<TableResa>table=this.tableRepository.findById(id);
		if(table.isPresent()) {
			Optional<ImageTable>img=this.imageTableRepository.findById(table.get().getImageTable().getId());
			if(img.isPresent()) {
				
						ImageTableDto i=ImageTableDto.builder()
								.id(img.get().getId())
								.nom(img.get().getNom())
								.build();
						res = Optional.of(i);
					
			}
		}
		return res;
		
	}

	@Override
	public List<TableResaDto> listerParTypeDeTable(int capacite, Boolean active, Boolean resa) {
		
		if(active==null&&resa==null) {
			List<TableResaDto> maListe=this.tableRepository.findAll(capacite)
			.stream()
			.map(e->TableResaDto.builder()
					.id(e.getId())
					.nom(e.getNom())
					.reservationEnCours(e.getReservationEnCours())
					.capacite(e.getCapacite())
					.active(e.getActive())
					.nbResa(e.getNbResa())
					.description(e.getDescription())
					.heure(e.getHeure())
					.build())
			.collect(Collectors.toList());
	
	return maListe ;
		}
		
		if(active!=null&&resa!=null) {
			List<TableResaDto> maListe=this.tableRepository.findAlltablefiltre( active, resa, capacite)
			.stream()
			.map(e->TableResaDto.builder()
					.id(e.getId())
					.nom(e.getNom())
					.reservationEnCours(e.getReservationEnCours())
					.capacite(e.getCapacite())
					.active(e.getActive())
					.nbResa(e.getNbResa())
					.description(e.getDescription())
					.heure(e.getHeure())
					.build())
			.collect(Collectors.toList());
	
	return maListe ;
		}
		
		if(active!=null&&resa==null) {
			List<TableResaDto> maListe=this.tableRepository.findAllTableActive( active, capacite)
					.stream()
					.map(e->TableResaDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.reservationEnCours(e.getReservationEnCours())
							.capacite(e.getCapacite())
							.active(e.getActive())
							.nbResa(e.getNbResa())
							.description(e.getDescription())
							.heure(e.getHeure())
							.build())
					.collect(Collectors.toList());
			
			return maListe ;
		}
		
		if(active==null&&resa!=null) {
			
			List<TableResaDto> maListe=this.tableRepository.findAllTableResa( resa, capacite)
					.stream()
					.map(e->TableResaDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.reservationEnCours(e.getReservationEnCours())
							.capacite(e.getCapacite())
							.active(e.getActive())
							.nbResa(e.getNbResa())
							.description(e.getDescription())
							.heure(e.getHeure())
							.build())
					.collect(Collectors.toList());
			
			return maListe ;
		}
		return null;
	}

	@Override
	public List<TableResaDto> listerParTypeDeTableOrder(Integer capacite, Boolean active, Boolean resa, String sorting) {
		
		
		if(active==null&&resa==null) {
			if(sorting.equalsIgnoreCase("Nom")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableOrderNom(capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			if(sorting.equalsIgnoreCase("Capacite")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableOrderCapacite( capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
			if(sorting.equalsIgnoreCase("Reservation")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableOrderReservation( capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
		}
		if(active!=null&&resa!=null) {
			if(sorting.equalsIgnoreCase("Nom")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltablefiltreOrderNom( active, resa, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			if(sorting.equalsIgnoreCase("Capacite")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltablefiltreOrderCapacite( active, resa, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
			if(sorting.equalsIgnoreCase("Reservation")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltablefiltreOrderReservation( active, resa, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
		}
		
		else if(active!=null&&resa==null) {
			if(sorting.equalsIgnoreCase("Nom")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableActiveOrderNom( active, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			if(sorting.equalsIgnoreCase("Capacite")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableActiveOrderCapacite( active, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
			if(sorting.equalsIgnoreCase("Reservation")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableActiveOrderReservation( active, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
		}
		
		if(active==null&&resa!=null) {
			if(sorting.equalsIgnoreCase("Nom")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableResaOrderNom( resa, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			if(sorting.equalsIgnoreCase("Capacite")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableResaOrderCapacite( resa, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
			if(sorting.equalsIgnoreCase("Reservation")) {
				List<TableResaDto> maListe=this.tableRepository.findAlltableResaOrderReservation( resa, capacite)
						.stream()
						.map(e->TableResaDto.builder()
								.id(e.getId())
								.nom(e.getNom())
								.reservationEnCours(e.getReservationEnCours())
								.capacite(e.getCapacite())
								.active(e.getActive())
								.nbResa(e.getNbResa())
								.description(e.getDescription())
								.heure(e.getHeure())
								.build())
						.collect(Collectors.toList());
				
				return maListe ;
				
			}
			
		}
		return null;
	}

	@Override
	public List<TableResaDto> listerParNom(String nom) {
		
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		List<TableResaDto> maListe=this.tableRepository.findAlltableParNom( cap)
				.stream()
				.map(e->TableResaDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.reservationEnCours(e.getReservationEnCours())
						.capacite(e.getCapacite())
						.active(e.getActive())
						.nbResa(e.getNbResa())
						.description(e.getDescription())
						.heure(e.getHeure())
						.build())
				.collect(Collectors.toList());
		
		return maListe ;
		
	}

	@Override
	public TableResaDto chercherToutesLesTablesPage(Integer page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<TableResaDto> maListe=this.tableRepository.findAllbyPage(firstPageWithFiveElements)
				.stream()
				.map(e->TableResaDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.reservationEnCours(e.getReservationEnCours())
						.capacite(e.getCapacite())
						.active(e.getActive())
						.nbResa(e.getNbResa())
						.description(e.getDescription())
						.heure(e.getHeure())
						.utilisateur(e.getUtilisateur())
						.build())
				.collect(Collectors.toList());
		return maListe.get(0);
	}
	
	

}
