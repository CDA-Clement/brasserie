package com.afpa.labonnebiere.service.interf;

import org.springframework.security.core.Authentication;

import com.afpa.labonnebiere.security.model.JwtTokens;
import com.afpa.labonnebiere.security.model.UserSecDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

public interface IJwtTokenService {

    JwtTokens createTokens(Authentication authentication);
    String createToken(UserSecDto user);
    String createRefreshToken(UserSecDto user);

    JwtTokens refreshJwtToken(String token);
    Jws<Claims> validateJwtToken(String token);

}
