package com.afpa.labonnebiere.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.hibernate.result.NoMoreReturnsException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.HistoriquedAchatRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.HistoriqueDAchatDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.HistoriqueDAchat;
import com.afpa.labonnebiere.service.interf.IHistoriqueDAchatService;


@Service
public class HistoriqueDAchatServiceImpl implements IHistoriqueDAchatService {
	static int INC = 6;
	public static int fin;
	private final static Logger logger = Logger.getLogger(HistoriqueDAchatServiceImpl.class);

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private HistoriquedAchatRepository historiqueDAchatRepository;



	@Override
	public Optional<HistoriqueDAchatDto> findById(int id) {
		Optional<HistoriqueDAchat> prod = this.historiqueDAchatRepository.findById(id);
		Optional<HistoriqueDAchatDto> res = Optional.empty();
		if(prod.isPresent()) {
			HistoriqueDAchat h = prod.get();
			logger.info("findbyid " + id);
			HistoriqueDAchatDto prodDto = this.modelMapper.map(h, HistoriqueDAchatDto.class);
			res = Optional.of(prodDto);
		}
		return res;
	}

	@Override
	public Boolean supprimerById(int id) {
		if(this.historiqueDAchatRepository.existsById(id)) {
			logger.info("supression 'historique d'achat "+id);
			this.historiqueDAchatRepository.deleteById(id);
			return true;
		}else {
			logger.warn("suppression historique d'achat "+id+" inexistante");
			return false;
		}
	}





	@Override
	public List<HistoriqueDAchatDto> chercherTousLesHistoriquesDAchatPage(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);


		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findAll(firstPageWithFiveElements)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat par page "+page);
		return maliste;
	}


	@Override
	public List<HistoriqueDAchatDto> chercherTousLesHistoriquesDAchat() {
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findAll()
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat ");
		return maliste;
	}


	@Override
	public List<HistoriqueDAchatDto> findByIdUser(int page,int idUser) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findByidUser(firstPageWithFiveElements,idUser)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat pour l'utilisateur "+idUser);
		return maliste;
	}


	@Override
	public List<HistoriqueDAchatDto> findByIdDateBetween(int page, int idUser, String debut, String fin1) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findByDateAchatBetween(firstPageWithFiveElements, idUser, debut, fin1)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat pour l'utilisateur "+idUser+ " pour la pérdiode "+debut+" "+fin1);
		return maliste;
	}


	@Override
	public List<HistoriqueDAchatDto> findByIdDateBefore(int page, int idUser, String debut) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findByDateAchatAfter(firstPageWithFiveElements, idUser, debut)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat pour l'utilisateur "+idUser+ " après "+debut);
		return maliste;
	}
	


	@Override
	public List<HistoriqueDAchatDto> findByIdDateAfter(int page, int idUser, String fin1) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findByDateAchatAfter(firstPageWithFiveElements, idUser, fin1)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat pour l'utilisateur "+idUser+ " avant "+fin1);
		return maliste;
	}

	@Override
	public Optional<HistoriqueDAchatDto> findByIdByUser(int id, int idUser) {
		Optional<HistoriqueDAchat> prod = this.historiqueDAchatRepository.findByIdByUser(id,idUser);
		Optional<HistoriqueDAchatDto> res = Optional.empty();
		if(prod.isPresent()) {
			HistoriqueDAchat h = prod.get();
			logger.info("findbyid " + id);
			HistoriqueDAchatDto prodDto = this.modelMapper.map(h, HistoriqueDAchatDto.class);
			res = Optional.of(prodDto);
		}
		return res;
	}

	@Override
	public List<HistoriqueDAchatDto> findAllByIdUser(int idUser) {
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findByidUser(idUser)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat de l'utilisateur "+idUser);
		return maliste;
	
	}

	@Override
	public List<HistoriqueDAchatDto> findAllByIdDateBetween(int idUser, String debut, String fin1) {
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findAllByDateAchatBetween(idUser, debut, fin1)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat pour l'utilisateur "+idUser+ " pour la pérdiode "+debut+" "+fin1);
		return maliste;
	}

	@Override
	public ArrayList<String> findMostOccurences(int idUser) {
		Integer maxOccurrences=0;
		String favoriteName="";
		String favoriteTypeName="";
		Integer favoriteId=0; 
		ArrayList<String> mafavorite=new ArrayList<String>();
		List<HistoriqueDAchat> maList = this.historiqueDAchatRepository.findByidUser(idUser);
		
		for (HistoriqueDAchat biereFavorite : maList) {
			Integer occurence=0;
			for (HistoriqueDAchat historiqueDAchat : maList) {
				if(biereFavorite.getBiere().equals(historiqueDAchat.getBiere())){
					occurence++;
				}
			}
			if(occurence>maxOccurrences) {
				maxOccurrences=occurence;
				favoriteName=biereFavorite.getBiere();
				favoriteId=biereFavorite.getIdBiere();
				
			}
		}
		
		mafavorite.add(favoriteName);
		mafavorite.add(favoriteId.toString());
		mafavorite.add(maxOccurrences.toString());
		
		maxOccurrences=0;
		
		for (HistoriqueDAchat biereFavorite : maList) {
			Integer occurence=0;
			for (HistoriqueDAchat historiqueDAchat : maList) {
				if(biereFavorite.getTypeDeBiere().equals(historiqueDAchat.getTypeDeBiere())){
					occurence++;
				}
			}
			if(occurence>maxOccurrences) {
				maxOccurrences=occurence;
				favoriteTypeName=biereFavorite.getTypeDeBiere();
			}
		}
		
		mafavorite.add(favoriteTypeName);
		
		
		logger.info("chercher la biere favorite de "+idUser);
		return mafavorite;
	}
	
	
	@Override
	public Integer findBiereMoment() {
		Integer maxOccurrences=0;
		Integer favoriteId=-1; 
		
		List<HistoriqueDAchat> maList = this.historiqueDAchatRepository.findAll();
		if(maList.size()>0) {
			for (HistoriqueDAchat biereFavorite : maList) {
				Integer occurence=0;
				for (HistoriqueDAchat historiqueDAchat : maList) {
					if(biereFavorite.getBiere().equals(historiqueDAchat.getBiere())){
						occurence++;
					}
				}
				if(occurence>maxOccurrences) {
					maxOccurrences=occurence;
					biereFavorite.getBiere();
					favoriteId=biereFavorite.getIdBiere();
					
				}
			}
			
		}
		
		
		
		
		return favoriteId;
	}

	@Override
	public List<HistoriqueDAchatDto> findByMontant(int idUser, double montant) {
		List<HistoriqueDAchatDto> maliste = this.historiqueDAchatRepository.findByMontant(idUser, montant)
				.stream()
				.map(e->HistoriqueDAchatDto.builder()
						.id(e.getId())
						.dateAchat(e.getDateAchat())
						.idUser(e.getIdUser())
						.prixTotal(e.getPrixTotal())
						.listDeBiere(e.getListDeBiere())
						.build())
						
				.collect(Collectors.toList());
		fin = maliste.size();
		logger.info("chercher tous les historiques d'achat de l'utilisateur "+idUser+" par montant");
		return maliste;
	}

	@Override
	public Optional<HistoriqueDAchatDto> findBynom(String biere) {
		Optional<HistoriqueDAchat> prod = this.historiqueDAchatRepository.findByBiere(biere);
		Optional<HistoriqueDAchatDto> res = Optional.empty();
		if(prod.isPresent()) {
			HistoriqueDAchat h = prod.get();
			logger.info("findbyid " + biere);
			HistoriqueDAchatDto prodDto = this.modelMapper.map(h, HistoriqueDAchatDto.class);
			res = Optional.of(prodDto);
		}
		return res;
	}

	



}
