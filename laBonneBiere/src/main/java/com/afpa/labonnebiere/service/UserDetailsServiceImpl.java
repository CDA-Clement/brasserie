package com.afpa.labonnebiere.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.security.model.UserSecDto;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
    private UtilisateurRepository userRepository;
	
	@Override
	public UserSecDto loadUserByUsername(String username) {

		Objects.requireNonNull(username);
		Utilisateur userE = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found"));
		
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(userE.getRole().getNom()));

		return new UserSecDto(
				userE.getId(), 
				userE.getUsername(), 
				userE.getPassword(), 
				authorities,
				userE.getActif(), 
				LocalDate.now(), 
				userE.getTokenSecret());
	}

}