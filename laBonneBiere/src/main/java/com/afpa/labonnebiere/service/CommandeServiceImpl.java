package com.afpa.labonnebiere.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.cert.PKIXRevocationChecker.Option;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.CommandeRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.entity.Commande;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.ICommandeService;


@Service
public class CommandeServiceImpl implements ICommandeService {
	static int INC = 20;
	public static int fin;
	private final static Logger logger = Logger.getLogger(CommandeServiceImpl.class);
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private CommandeRepository commandeRepository;
	@Autowired
	private BiereServiceImpl biereService;

	
	
	

	@Override
	public List<CommandeDto> chercherTouteLesCommandes() {
		
		List<CommandeDto> maliste = this.commandeRepository.findAll()
				.stream()
				.map(e->CommandeDto.builder()
						.id(e.getId())
						.commande(e.getCommande())
						.nom(e.getNom())
						.total(e.getTotal())
						.utilisateurdto(UtilisateurDto.builder()
								.id(e.getUtilisateur().getId())
								.username(e.getUtilisateur().getUsername())
								.build())
						
						.build())
								
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
					
	}


	@Override
	public Integer ajouter(CommandeDto prod, int id) {
		Commande c = this.modelMapper.map(prod,Commande.class);
		try {
			this.commandeRepository.save(c);
			logger.info("commande "+c.getId()+" enregistrée");
		} catch (Exception e) {
			logger.error("commande "+c.getId()+" non enregistrée "+e.getMessage());
			return null;
		}

		return c.getId();
	}


	@Override
	public Integer Maj(CommandeDto commentaire) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Boolean supprimerId(int id) {
		logger.info("suppression commande "+id);
		this.commandeRepository.deleteById(id);
		return true;
	}


	@Override
	public List<CommandeDto> AfficherCommandesParUtilisateur(int id) {
		
		List<CommandeDto> maliste = this.commandeRepository.findAllCommandeByUtilisateur(id)
				.stream()
				.map(e->CommandeDto.builder()
						.id(e.getId())
						.commande(e.getCommande())
						.nom(e.getNom())
						.idBiere(e.getIdBiere())
						.prixUnitaire(e.getPrixUnitaire())
						.total(e.getTotal())
						.utilisateurdto(UtilisateurDto.builder()
								.id(e.getUtilisateur().getId())
								.username(e.getUtilisateur().getUsername())
								.build())
						
						.build())
								
				.collect(Collectors.toList());
		fin = maliste.size();
		for (CommandeDto commande: maliste){
			BigDecimal bd = new BigDecimal(commande.getTotal()).setScale(2, RoundingMode.HALF_UP);
			commande.setTotal(bd.doubleValue());
			
		}
		return maliste;
	}


	@Override
	public void suppressionTableJointureListDeCommande(int id) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void suppressionTableJointureListDeCommande2(int id) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Commande chercherUneCommande(int id) {
		if(this.commandeRepository.existsById(id)) {
			Optional<Commande> cmd = this.commandeRepository.findById(id);
			Commande cm = cmd.get();
			
			return cm;
		}else {
		
	}
		return null;


	
	
	

}
}