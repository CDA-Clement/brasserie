package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.afpa.labonnebiere.dto.ImageTableDto;
import com.afpa.labonnebiere.dto.TableResaDto;

public interface ITableResaService {
	
	public List<TableResaDto> chercherToutesLesTables();

	public Optional<TableResaDto> findById(int id);

	public Boolean supprimerId(int id);
	
	public Optional<TableResaDto> trouverParnom(String nom);


	public Integer ajouter(TableResaDto table);

	public List<TableResaDto> listerParDisponibilite(Boolean disponible);
	
	public Integer update(TableResaDto table);
	
	

	public Boolean reservationTable(Integer resa, Integer idTable, Integer idUser, Integer heure);

	public Boolean resetAll();

	public Optional<TableResaDto> afficherParNom(String nom);

	Optional<TableResaDto> tableParUtilisateur(int id);

	public List<TableResaDto> listerParTypeDeTable( int qteMin, Boolean active, Boolean resa);
	
	public List<TableResaDto> listerParTypeDeTableOrder( Integer qteMin, Boolean active, Boolean resa, String sorting);
	
	public List<TableResaDto> listerParNom( String recherche);

	Optional<ImageTableDto> afficherImage(int id, String nomImage);

	public TableResaDto chercherToutesLesTablesPage(Integer page);
	
	

}
