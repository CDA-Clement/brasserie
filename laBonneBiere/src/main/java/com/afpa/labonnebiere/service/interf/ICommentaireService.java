package com.afpa.labonnebiere.service.interf;

import java.util.List;

import com.afpa.labonnebiere.dto.CommentaireDto;

public interface ICommentaireService {
	
	public List<CommentaireDto> chercherTousLesCommentaires(int page);

	

	Boolean supprimerId(int id);


	Integer ajouter(CommentaireDto prod, int id);

	Integer Maj(CommentaireDto commentaire);

	List<CommentaireDto> findByIdBiere(int id);



	public List<CommentaireDto> trouverParNom(String username);



	public List<CommentaireDto> trouverParBiere(Integer username);


	

}
