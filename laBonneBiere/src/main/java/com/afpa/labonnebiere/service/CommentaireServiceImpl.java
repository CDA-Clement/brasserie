package com.afpa.labonnebiere.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.CommentaireRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommentaireDto;
import com.afpa.labonnebiere.dto.TypeDeBiereDto;
import com.afpa.labonnebiere.entity.Commentaire;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.service.interf.ICommentaireService;

@Service
public class CommentaireServiceImpl implements ICommentaireService {
	static int INC = 20;
	public static int fin;
	private final static Logger logger = Logger.getLogger(CommentaireServiceImpl.class);
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private CommentaireRepository commentaireRepository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Override
	public List<CommentaireDto> chercherTousLesCommentaires(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);

		List<CommentaireDto> maliste = this.commentaireRepository.findAll(firstPageWithFiveElements).stream()
				.map(e -> CommentaireDto.builder().id(e.getId()).nom(e.getNom()).message(e.getMessage())
						.date(e.getDate())
						.biere(BiereDto.builder().degre(e.getBiere().getDegre())
								.description(e.getBiere().getDescription()).id(e.getBiere().getId())
								.nom(e.getBiere().getNom()).prix(e.getBiere().getPrix())
								.quantite(e.getBiere().getQuantite())
								.typeDeBiereDto(TypeDeBiereDto.builder().id(e.getBiere().getTypeDeBiere().getId())
										.nom(e.getBiere().getTypeDeBiere().getNom()).build())
								.build())
						.build())

				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

	@Override
	public List<CommentaireDto> findByIdBiere(int id) {
		List<CommentaireDto> maliste = this.commentaireRepository.commentaireParBiere(id).stream()
				.map(e -> CommentaireDto.builder().id(e.getId()).nom(e.getNom()).message(e.getMessage())
						.date(e.getDate())
						.biere(BiereDto.builder().degre(e.getBiere().getDegre())
								.description(e.getBiere().getDescription()).id(e.getBiere().getId())
								.nom(e.getBiere().getNom()).prix(e.getBiere().getPrix())
								.quantite(e.getBiere().getQuantite())
								.typeDeBiereDto(TypeDeBiereDto.builder().id(e.getBiere().getTypeDeBiere().getId())
										.nom(e.getBiere().getTypeDeBiere().getNom()).build())
								.build())
						.build())

				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

	@Override
	public Boolean supprimerId(int id) {
		if (this.commentaireRepository.existsById(id)) {
			this.commentaireRepository.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Integer ajouter(CommentaireDto commentaire, int id) {
		

		Commentaire p = this.modelMapper.map(commentaire, Commentaire.class);
		
		Optional<Utilisateur> utilisateurOptional = this.utilisateurRepository.findById(id);
		if (utilisateurOptional.isPresent()) {
			Utilisateur u = utilisateurOptional.get();
			commentaire.setNom(u.getUsername());
			commentaire.setDate(new Date());
		}

		try {
			this.commentaireRepository.save(p);
			logger.info("commentaire "+p.getId()+" a bien été enregistré");
		} catch (Exception e) {
			logger.error("commentaire "+p.getId()+" n'a pas été enregistré "+ e.getStackTrace());
			return null;
		}
		
		return p.getId();
	}

	@Override
	public Integer Maj(CommentaireDto commentaire) {
		Commentaire p = this.modelMapper.map(commentaire, Commentaire.class);

		try {
			if (commentaireRepository.existsById(commentaire.getId())) {
				this.commentaireRepository.save(p);
				logger.info("commentaire "+p.getId()+" a bien été mis a jour");
				return p.getId();
			} else {
				return -1;
			}
		} catch (Exception e) {
			logger.error("commentaire "+p.getId()+" n'a pas été mis a jour "+ e.getStackTrace());

			return -1;
		}

	}

	@Override
	public List<CommentaireDto> trouverParNom(String username) {
		List<CommentaireDto> maliste = this.commentaireRepository.findAllByNom(username).stream()
				.map(e -> CommentaireDto.builder().id(e.getId()).nom(e.getNom()).message(e.getMessage())
						.biere(BiereDto.builder().degre(e.getBiere().getDegre())
								.description(e.getBiere().getDescription()).id(e.getBiere().getId())
								.nom(e.getBiere().getNom()).prix(e.getBiere().getPrix())
								.quantite(e.getBiere().getQuantite())
								.typeDeBiereDto(TypeDeBiereDto.builder().id(e.getBiere().getTypeDeBiere().getId())
										.nom(e.getBiere().getTypeDeBiere().getNom()).build())
								.build())
						.build())

				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

	@Override
	public List<CommentaireDto> trouverParBiere(Integer username) {
		List<CommentaireDto> maliste = this.commentaireRepository.findAllByBiere_id(username).stream()
				.map(e -> CommentaireDto.builder().id(e.getId()).nom(e.getNom()).message(e.getMessage())
						.biere(BiereDto.builder().degre(e.getBiere().getDegre())
								.description(e.getBiere().getDescription()).id(e.getBiere().getId())
								.nom(e.getBiere().getNom()).prix(e.getBiere().getPrix())
								.quantite(e.getBiere().getQuantite())
								.typeDeBiereDto(TypeDeBiereDto.builder().id(e.getBiere().getTypeDeBiere().getId())
										.nom(e.getBiere().getTypeDeBiere().getNom()).build())
								.build())
						.build())

				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

}
