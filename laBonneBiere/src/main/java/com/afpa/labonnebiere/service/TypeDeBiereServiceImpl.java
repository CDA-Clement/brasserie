package com.afpa.labonnebiere.service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.controller.ControllerImageBiere;
import com.afpa.labonnebiere.dao.BiereRepository;
import com.afpa.labonnebiere.dao.TypeDeBiereRepository;
import com.afpa.labonnebiere.dto.TypeDeBiereDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.TypeDeBiere;
import com.afpa.labonnebiere.service.interf.ITypeDeBiereService;



@Service
public class TypeDeBiereServiceImpl implements ITypeDeBiereService {
	static int INC = 20;
	public static int fin;
	private final static Logger logger = Logger.getLogger(TypeDeBiereServiceImpl.class);

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private TypeDeBiereRepository typeDeBiereRepository;

	@Autowired
	private BiereRepository biereRepository;




	@Override
	public Optional<TypeDeBiereDto> findById(int id) {
		Optional<TypeDeBiere> prod = this.typeDeBiereRepository.findById(id);
		Optional<TypeDeBiereDto> res = Optional.empty();
		if(prod.isPresent()) {
			TypeDeBiere p = prod.get();
			TypeDeBiereDto typeDeBiereDto = this.modelMapper.map(p, TypeDeBiereDto.class);
			res = Optional.of(typeDeBiereDto);
		}
		return res;
	}

	@Override
	public Boolean supprimerId(int id) {
		Optional<TypeDeBiere> optionalType=this.typeDeBiereRepository.findById(id);
		if(optionalType.isPresent()) {
			List<Biere> list=this.biereRepository.findAllByTypeDeBiereSup(optionalType.get().getId());
			if(list.size()>0) {
				for (Biere biere : list) {
					biere.setTypeDeBiere(TypeDeBiere.builder().id(0).build());
					this.biereRepository.save(biere);
					logger.info("suppression tyde de biere avec changement de type de biere contenu "+biere);
				}
			}
			this.typeDeBiereRepository.deleteById(id);
			return true;
		}else {
			logger.info("aucun typde de biere a supprimer");
			return false;
		}
	}


	@Override
	public Integer ajouter(TypeDeBiereDto typeDeBiere) {
		String cap = typeDeBiere.getNom().substring(0, 1).toUpperCase() + typeDeBiere.getNom().substring(1);
		typeDeBiere.setNom(cap);
		TypeDeBiere p = this.modelMapper.map(typeDeBiere,TypeDeBiere.class);
		try {
			this.typeDeBiereRepository.save(p);
			logger.info("type de biere ajouté "+p.getNom());
		} catch (Exception e) {
			logger.error("type de biere non ajouté "+e.getStackTrace());
			return null;
		}


		return p.getId();
	}

	@Override
	public Integer Maj(TypeDeBiereDto typeDeBiere) {
		String cap = typeDeBiere.getNom().substring(0, 1).toUpperCase() + typeDeBiere.getNom().substring(1);
		typeDeBiere.setNom(cap);

		TypeDeBiere p = this.modelMapper.map(typeDeBiere,TypeDeBiere.class);


		try {
			if(typeDeBiereRepository.existsById(typeDeBiere.getId())) {
				this.typeDeBiereRepository.save(p);	
				logger.info("type de biere "+p.getNom()+" bien été modifié");
				return p.getId();
			}else {
				return 0;
			}
		} catch (Exception e) {
			logger.error("le type de biere "+p.getNom()+" n'a pas été modifié "+e.getStackTrace());

			return 0;
		}

	}



	@Override
	public List<TypeDeBiereDto> chercherToutsLesTypesDeBieres(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		typeDeBiereRepository.findAll(firstPageWithFiveElements);
		List<TypeDeBiereDto> maliste = this.typeDeBiereRepository.findAll(firstPageWithFiveElements)
				.stream()
				.map(e->TypeDeBiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public Optional<TypeDeBiereDto> trouverParNom(String type) {
		String cap = type.substring(0, 1).toUpperCase() + type.substring(1);
		Optional<TypeDeBiere> prod = this.typeDeBiereRepository.findByNom(cap);
		Optional<TypeDeBiereDto> res = Optional.empty();
		if(prod.isPresent()) {
			TypeDeBiere p = prod.get();
			TypeDeBiereDto typeDeBiereDto = this.modelMapper.map(p, TypeDeBiereDto.class);
			res = Optional.of(typeDeBiereDto);
		}
		return res;
	}


}
