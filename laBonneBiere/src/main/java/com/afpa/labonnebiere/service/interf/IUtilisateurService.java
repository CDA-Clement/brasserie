package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;

public interface IUtilisateurService {
	
	public List<UtilisateurDto> chercherTousLesUtilisateurs(int page);

	public Optional<UtilisateurDto> findById(int id);

	Boolean supprimerId(int id);


	String ajouter(UtilisateurDto prod);

	Integer Maj(UtilisateurDto utilisateur);

	List<UtilisateurDto>listerParRole(int page,int id);


	Optional<UtilisateurDto> trouverParUsername(String nom);
	
	List<UtilisateurDto> listerParNom(String user);
	
	public void findAllNeverActivate();

	

	Integer ajoutBiere(UtilisateurDto utilisateur, BiereDto biere);


	public Integer supprimerBiere(UtilisateurDto u, String biere, Integer quantite);
	
	List<UtilisateurDto> listerByFilter(int page, Boolean active, int role, String sorting);

	
}
