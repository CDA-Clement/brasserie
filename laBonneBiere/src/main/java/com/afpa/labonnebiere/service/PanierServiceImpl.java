package com.afpa.labonnebiere.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.BiereRepository;
import com.afpa.labonnebiere.dao.CommandeRepository;
import com.afpa.labonnebiere.dao.HistoriquedAchatRepository;
import com.afpa.labonnebiere.dao.PanierRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.dto.PanierDto;
import com.afpa.labonnebiere.dto.RoleDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.Commande;
import com.afpa.labonnebiere.entity.HistoriqueDAchat;
import com.afpa.labonnebiere.entity.Panier;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.service.interf.IPanierService;
import com.afpa.labonnebiere.test.email.EmailMessage;
import com.afpa.labonnebiere.test.email.EmailserviceApplication;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


@Service
public class PanierServiceImpl implements IPanierService {
	static int INC = 20;
	public static int fin;
	private final static Logger logger = Logger.getLogger(PanierServiceImpl.class);
	@Value("${pathInvoices.invoices.path}")
	private String path;
	
	@Value("${mailAdmin.admin.mail}")
	private String mailAdmin;
	
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private PanierRepository panierRepository;
	@Autowired
	private CommandeRepository commandeRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private BiereRepository biereRepository;
	@Autowired
	private HistoriquedAchatRepository historiqueAchat;
	@Autowired
	private EmailserviceApplication smtp;


	@Override
	public List<PanierDto> chercherTousLesPaniers(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<PanierDto> maliste = this.panierRepository.findAll(firstPageWithFiveElements)
				.stream()
				.map(e->{
					PanierDto panierDto=PanierDto.builder()
							.id(e.getId())
							.nom(e.getNom())
							.totalpanier(e.getTotalPanier())
							.build();

					panierDto.setListDeCommande(new ArrayList<CommandeDto>());

					for(Commande c : e.getListDeCommande()) {
						panierDto.getListDeCommande().add(CommandeDto.builder()
								.id(c.getId())
								.nom(c.getNom())
								.total(c.getTotal())
								.utilisateurdto(UtilisateurDto.builder()
										.id(c.getUtilisateur().getId())
										.nom(c.getUtilisateur().getNom())
										.prenom(c.getUtilisateur().getPrenom())
										.username(c.getUtilisateur().getUsername())
										.role(RoleDto.builder()
												.id(c.getUtilisateur().getRole().getId())
												.nom(c.getUtilisateur().getRole().getNom()).build())
										.build())
								.build());

					}

					return panierDto;
				})

				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}


	@Override
	public Integer ajouter(PanierDto prod, int id) {
		Panier c = this.modelMapper.map(prod,Panier.class);
		try {
			this.panierRepository.save(c);
			logger.info("le panier "+c.getId()+" a bien été enregistré");
		} catch (Exception e) {
			logger.error("le panier "+c.getId()+" n'a pas été enregistré " +e.getStackTrace());
			return null;
		}

		return null;
	}


	@Override
	public Integer Maj(List<CommandeDto> commande) throws Exception {
		double total=0;
		String pathfile="";
		String biere="";
		Integer maxBiere=0;
		String typeDeBiere="";
		Integer idBiere=0;
		List<Biere>list= new ArrayList<Biere>();
		Optional<Utilisateur> utilisateur=this.utilisateurRepository.findById(commande.get(0).getUtilisateurdto().getId());
		if(utilisateur.isPresent()) {
			 total=utilisateur.get().getPanier().getTotalPanier();
			List<Commande> commande1=this.commandeRepository.findAllCommandeByUtilisateur(utilisateur.get().getId());
			for (Commande c : commande1) {
				total+=c.getTotal();
				if(c.getCommande()>maxBiere) {
					maxBiere=c.getCommande();
					biere=c.getNom();
					Biere bibine=this.biereRepository.findByNom(biere).get();
					typeDeBiere=bibine.getTypeDeBiere().getNom();
					idBiere=bibine.getId();
				}
				utilisateur.get().getPanier().getListDeCommande().add(c);
			}
			utilisateur.get().getPanier().setTotalPanier(total);
			Panier p=this.modelMapper.map(utilisateur.get().getPanier(), Panier.class);
			this.panierRepository.save(p);


			for (Commande c : commande1) {
				if(c.getCommande()>0) {
					
					Biere b=biereRepository.findById(c.getIdBiere()).get();
					BiereDto bDto=this.modelMapper.map(b, BiereDto.class);
					bDto.setCommande(c.getCommande());
					list.add(b);
				
					
				}
				commandeRepository.suppressionTableJointureListDeCommande2(c.getId());
				commandeRepository.delete(c);

			}
			}
			
			
			HistoriqueDAchat historique = HistoriqueDAchat.builder()
					.prixTotal(utilisateur.get().getPanier().getTotalPanier())
					.dateAchat(new Date())
					.idUser(utilisateur.get().getId())
					.biere(biere)
					.idBiere(idBiere)
					.typeDeBiere(typeDeBiere)
					.listDeBiere(list)
					.build();
			if(list.size()>0) {
				
				
				this.historiqueAchat.save(historique);
				logger.info("historique d'achat crééé pour "+utilisateur.get().getUsername());
				
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(path+"invoice-"+historique.getIdUser()+"-"+historique.getId()+".pdf"));
				pathfile=path+"invoice-"+historique.getIdUser()+"-"+historique.getId()+".pdf";
				document.open();
				Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
				Chunk chunk = new Chunk("Facture N° "+historique.getId(), font);
				Paragraph parap6=new Paragraph(" ");
				Chunk chunk2 = new Chunk(utilisateur.get().getNom());
				Chunk chunk3 = new Chunk(utilisateur.get().getPrenom());
				Chunk chunk4 = new Chunk(utilisateur.get().getAdresseMail());
				Paragraph parap=new Paragraph(chunk2);
				Paragraph parap4=new Paragraph(chunk3);
				Paragraph parap5=new Paragraph(chunk4);
				Paragraph parap2=new Paragraph(" ");
				Paragraph parap3=new Paragraph(" ");
				document.add(chunk);
				document.add(parap6);
				document.add(parap);
				document.add(parap4);
				document.add(parap5);
				document.add(parap2);
				document.add(parap3);
				PdfPTable table = new PdfPTable(3);
				addTableHeader(table);
				for (Biere element : historique.getListDeBiere()) {
					table.addCell(element.getNom());
					for (Commande c :utilisateur.get().getPanier().getListDeCommande()) {
        	   			if(c.getIdBiere()==element.getId()) {
        	   				table.addCell(c.getCommande().toString());
        	   				Double total1=round(c.getTotal(),2);
        	   				table.addCell(total1.toString()+" €");
        	   			}
        	   		}
				}
				addTableFooter(table);
				table.addCell(" ");
				table.addCell("Montant HT");
				Double ht=historique.getPrixTotal()/1.196;
				ht=round(ht,2);
				table.addCell(ht.toString()+" €");
				table.addCell(" ");
				table.addCell("Montant T.V.A");
				Double tva=historique.getPrixTotal()-ht;
				tva=round(tva,2);
			
				table.addCell(tva.toString()+" €");
				table.addCell(" ");
				table.addCell("Montant TTC");
				Double ttc=round(historique.getPrixTotal(),2);
				table.addCell(ttc.toString()+" €");
				//addCustomRows(table);
				document.add(table);
				document.close();
				
				
				
				
				try {
					smtp.sendInvoice(EmailMessage.builder()
							.body("Confirmation de votre achat")
							.subject("Confirmation de la commande "+historique.getId())
							.to_address(utilisateur.get().getAdresseMail()+", "+mailAdmin)
							.build(),pathfile);
					logger.info("mail envoyé à "+utilisateur.get().getAdresseMail());
				} catch (AddressException e) {
					logger.error("mail non envoyé à "+utilisateur.get().getAdresseMail()+" "+e.getStackTrace());
					e.printStackTrace();
				} catch (MessagingException e) {
					logger.error("mail non envoyé à "+utilisateur.get().getAdresseMail()+" "+e.getStackTrace());
				} catch (IOException e) {
					logger.error("mail non envoyé à "+utilisateur.get().getAdresseMail()+" "+e.getStackTrace());
				}
			}else {
				
				logger.info(utilisateur.get().getUsername()+" a envoyé une commande vide");
			}
			
			utilisateur.get().setListHistoriqueAchat(historique);
			utilisateur.get().getPanier().setTotalPanier(0.0);
			this.utilisateurRepository.save(utilisateur.get());
			return utilisateur.get().getId() ;
		}
	
	private void addTableHeader(PdfPTable table) {
		Font font = FontFactory.getFont(FontFactory.TIMES, 12, BaseColor.BLACK);
	    Stream.of("Designation Produit", "Quantité(s)", "Total")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle,font));
	        table.addCell(header);
	    });
	}
	
	private void addTableFooter(PdfPTable table) {
		Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 12, BaseColor.BLACK);
		 Stream.of(" ", " ", "TOTAUX")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle,font));
	        table.addCell(header);
	      });
	}
	
	private void addRows(PdfPTable table) {
		
	    table.addCell("row 1, col 1");
	    table.addCell("row 1, col 2");
	    table.addCell("row 1, col 3");
	}
		
	


	@Override
	public Boolean supprimerId(int id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public PanierDto AfficherPanierParUtilisateur(int id) {
		Optional<Panier> panier = this.panierRepository.findById(id);
		if(panier.isPresent()) {
			Panier e=panier.get();
			PanierDto panierDto=PanierDto.builder()
					.id(e.getId())
					.nom(e.getNom())
					.totalpanier(e.getTotalPanier())
					.build();

			panierDto.setListDeCommande(new ArrayList<CommandeDto>());

			for(Commande c : e.getListDeCommande()) {
				panierDto.getListDeCommande().add(CommandeDto.builder()
						.id(c.getId())
						.nom(c.getNom())
						.total(c.getTotal())
						.commande(c.getCommande())
						.utilisateurdto(UtilisateurDto.builder()
								.id(c.getUtilisateur().getId())
								.nom(c.getUtilisateur().getNom())
								.prenom(c.getUtilisateur().getPrenom())
								.username(c.getUtilisateur().getUsername())
								.role(RoleDto.builder()
										.id(c.getUtilisateur().getRole().getId())
										.nom(c.getUtilisateur().getRole().getNom()).build())
								.build())
						.build());
			}

		return panierDto;
		}
		else {
			return null;
		}
	}


	@Override
	public int SupprimerPanier(UtilisateurDto u) {
		for (CommandeDto commande : u.getPanier().getListDeCommande()) {
			this.commandeRepository.suppressionTableJointureListDeCommande2(commande.getId());		
		}
		
		Utilisateur ut = this.modelMapper.map(u,Utilisateur.class);
		ut.getPanier().setTotalPanier(0.0);
		ut.getPanier().getListDeCommande().clear();
		this.panierRepository.save(ut.getPanier());
		logger.info("le panier de l'utilisateur "+ut.getNom()+" a bien été remis à zero");
		return u.getId();
	}
	
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

	





}
