package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import com.afpa.labonnebiere.dto.RoleDto;

public interface IRoleService {
	
	public List<RoleDto> chercherTousLesTypesDeRoles(int page);

	public Optional<RoleDto> findById(int id);

	Boolean supprimerId(int id);
//	
//	Boolean suprimerRoleActif(int id);


	Boolean ajouter(String prod);
//
//	Integer Maj(RoleDto prod);

	public Optional<RoleDto> trouverParnom(String nom);
}


