package com.afpa.labonnebiere.service;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.BiereRepository;
import com.afpa.labonnebiere.dao.CommandeRepository;
import com.afpa.labonnebiere.dao.HistoriquedAchatRepository;
import com.afpa.labonnebiere.dao.PanierRepository;
import com.afpa.labonnebiere.dao.RoleRepository;
import com.afpa.labonnebiere.dao.TableResaRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.HistoriqueDAchatDto;
import com.afpa.labonnebiere.dto.PanierDto;
import com.afpa.labonnebiere.dto.RoleDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.Commande;
import com.afpa.labonnebiere.entity.HistoriqueDAchat;
import com.afpa.labonnebiere.entity.Panier;
import com.afpa.labonnebiere.entity.TableResa;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;
import com.afpa.labonnebiere.test.email.EmailMessage;
import com.afpa.labonnebiere.test.email.EmailserviceApplication;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService {
	static int INC = 6;
	public static int fin;
	@Value("${address.server.ip}")
	private String ipAddressServerBack;
	
	@Value("${mailAdmin.admin.mail}")
	private String mailAdmin;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private ModelMapper modelMapper;
	private final static Logger logger = Logger.getLogger(UtilisateurServiceImpl.class);
	

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private PanierRepository panierRepository;
	@Autowired
	private HistoriquedAchatRepository historiqueRepository;
	@Autowired
	private BiereServiceImpl biereRepository;
	@Autowired
	private CommandeRepository commandeRepository;
	@Autowired
	private TableResaRepository tableResaRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private EmailserviceApplication smtp;


	@Override
	public Optional<UtilisateurDto> trouverParUsername(String nom) {
		
		Optional<Utilisateur> prod = this.utilisateurRepository.findUserByUsername(nom);
		Optional<UtilisateurDto> res = Optional.empty();
		if (prod.isPresent()) {
			Utilisateur u = prod.get();
			UtilisateurDto utilisateurDto = this.modelMapper.map(u, UtilisateurDto.class);
			res = Optional.of(utilisateurDto);
		}
		return res;
	}

	@Override
	public Optional<UtilisateurDto> findById(int id) {
		Optional<Utilisateur> prod = this.utilisateurRepository.findById(id);
		Optional<UtilisateurDto> res = Optional.empty();
		if (prod.isPresent()) {
			Utilisateur p = prod.get();
			UtilisateurDto utilisateurDto = this.modelMapper.map(p, UtilisateurDto.class);
			res = Optional.of(utilisateurDto);
		}
		return res;
	}

	
	
	@Override
	public Boolean supprimerId(int id) {
		if (this.utilisateurRepository.existsById(id)) {
			int idpanier = utilisateurRepository.findById(id).get().getPanier().getId();
//			List<HistoriqueDAchat> list=this.historiqueRepository.findByidUser(id);
//			for (HistoriqueDAchat historiqueDAchat : list) {
//				this.historiqueRepository.deleteById(historiqueDAchat.getId());
//				
//			}
			this.utilisateurRepository.deleteById(id);
			this.panierRepository.deleteById(idpanier);
			logger.info("utilisateur "+id+" a bien été supprimé");
			return true;
		} else {
			logger.info("utilisateur "+id+" n'a pas  été supprimé car il n'existe pas");
			return false;
		}
	}

	@Override
	public String ajouter(UtilisateurDto utilisateur) {
		String cap = utilisateur.getNom().substring(0, 1).toUpperCase() + utilisateur.getNom().substring(1);
		utilisateur.setNom(cap);
		Utilisateur p = this.modelMapper.map(utilisateur, Utilisateur.class);
		if(p.getRole() == null) {
			p.setRole(this.roleRepository.findByNom("User").get());			
		} else {
			p.setRole(p.getRole());
		}
		p.setPanier(Panier.builder().nom("panier " + p.getUsername()).totalPanier(0.0).build());
		p.getPanier().setListDeCommande(new ArrayList<Commande>());
		p.setPassword(passwordEncoder.encode(p.getPassword()));
		panierRepository.save(p.getPanier());
		p.setPanier(Panier.builder()
				.id(p.getPanier().getId())
				.totalPanier(0.0)
				.build());
		

//		p.setListHistoriqueAchat(HistoriqueDAchat.builder().listDeBiere(new ArrayList<Biere>()).build());
//		historiqueRepository.save(p.getListHistoriqueAchat());
//		p.setListHistoriqueAchat(HistoriqueDAchat.builder().id(p.getListHistoriqueAchat().getId()).build());
		
		try {
			p.setReservation(false);
			p.setActif(false);
			p.setActivated(false);
			p.setHeureActivation(new Date().getTime());
			this.utilisateurRepository.save(p);
			logger.info("création de l'utilisateur "+p.getUsername()+" "+p.getId());
			EmailMessage mail=new EmailMessage();
			EmailMessage mail2=new EmailMessage();
			mail.setTo_address(p.getAdresseMail());
			URL myURL = new URL("http://"+ipAddressServerBack+"/validation/"+p.getId()+"/"+p.getPassword().subSequence(0, 5));
			
			mail.setSubject("validation du compte "+p.getUsername());
			mail.setBody("veuillez cliquer sur le lien suivant "+myURL);
			mail2.setTo_address(mailAdmin);
			mail2.setSubject("creation du compte "+p.getUsername());
			mail2.setBody(p.getUsername()+" vient de créer son compte");
			smtp.sendEmail(mail);
			smtp.sendEmail(mail2);
		} catch (Exception e) {
			logger.error("l'utilisateur "+p.getUsername()+" "+p.getId()+" n'a pas été enregistré "+e.getMessage());
			int i= e.getLocalizedMessage().indexOf("UTILISATEUR");
			String rep=e.getLocalizedMessage().substring(i+12, i+24);
			
			
			
			return rep;
		}
		return "ok";
	}

	@Override
	public Integer ajoutBiere(UtilisateurDto utilisateur, BiereDto biere) {

		Optional<BiereDto> biere1 = biereRepository.findById(biere.getId());
		Optional<Commande> commande1 = commandeRepository.findByNom(utilisateur.getId(),biere1.get().getNom());
		Optional<Utilisateur> util = utilisateurRepository.findById(utilisateur.getId());

		Commande c = new Commande();
		Utilisateur p = this.modelMapper.map(utilisateur, Utilisateur.class);
		BiereDto biere2 = biere1.get();

		if (biere.getQuantite() <= biere2.getQuantite()) {
			if (commande1.isPresent() && util.isPresent() && commande1.get().getUtilisateur().getId() == util.get().getId()) {
				commande1.get().setCommande(biere.getQuantite() + commande1.get().getCommande());
				commande1.get().setId(commande1.get().getId());
				commande1.get().setTotal(commande1.get().getCommande() * biere2.getPrix());
				c = commande1.get();
				//commandeRepository.delete(commande1.get());

			} else {
				c.setCommande(biere.getQuantite());
				c.setNom(biere2.getNom());
				c.setIdBiere(biere2.getId());
				c.setPrixUnitaire(biere2.getPrix());
				c.setTotal(biere.getQuantite() * biere2.getPrix());
				c.setUtilisateur(p);
			}
			commandeRepository.save(c);
			
			
			biere2.setQuantite(biere2.getQuantite() - biere.getQuantite());
			biereRepository.ajouter(biere2);
			if(biere.getQuantite()==0) {
				this.commandeRepository.delete(c);
				logger.info("suppression de la commande "+c.getId()+" car elle est vide");
			}
			
			return p.getId();

		} else {
			return -1;
		}

	}
	
	@Override
	public Integer supprimerBiere(UtilisateurDto u, String biere, Integer quantite) {
		Utilisateur p = this.modelMapper.map(u, Utilisateur.class);
		
		String regex = "[0-9]+";
		if (biere.matches(regex)) {
			int id = Integer.parseInt(biere);
			Optional<BiereDto> b1 = biereRepository.findById(id);
			Optional<Commande> com = commandeRepository.findByNom(u.getId(),b1.get().getNom());
			if (com.isPresent()) {
				if (quantite >= com.get().getCommande()) {
					int quantiter = com.get().getCommande();
					b1.get().setQuantite(b1.get().getQuantite() + quantite);
					biereRepository.ajouter(b1.get());
					commandeRepository.suppressionTableJointureListDeCommande2(com.get().getId());
					commandeRepository.delete(com.get());


				} else {
					int quantiter = com.get().getCommande();
					com.get().setCommande(com.get().getCommande() - quantite);
					Optional<BiereDto> b = biereRepository.findById(id);
					b.get().setQuantite(b.get().getQuantite() + quantite);
					biereRepository.ajouter(b.get());
					com.get().setTotal(com.get().getCommande() * b.get().getPrix());
					Commande c = new Commande();
					c = com.get();
					commandeRepository.suppressionTableJointureListDeCommande2(com.get().getId());
					commandeRepository.delete(com.get());
					commandeRepository.save(c);
				}

			}

		} 
		return 1;
	}

	@Override
	public Integer Maj(UtilisateurDto utilisateur) {
		Utilisateur util=this.utilisateurRepository.findById(utilisateur.getId()).get();
		Utilisateur p = this.modelMapper.map(utilisateur, Utilisateur.class);
		if(!utilisateur.getPassword().equals(util.getPassword())) {
			p.setPassword(passwordEncoder.encode(p.getPassword()));
		}
		p.setPanier(Panier.builder()
				.nom("panier " + p.getUsername())
				.id(p.getPanier().getId())
				.totalPanier(p.getPanier().getTotalPanier())
				.build());
		
		panierRepository.save(p.getPanier());
		
//		p.setListHistoriqueAchat(HistoriqueDAchat.builder().listDeBiere(p.getListHistoriqueAchat().getListDeBiere())
//				.id(p.getListHistoriqueAchat().getId()).build());
//		
//		historiqueRepository.save(p.getListHistoriqueAchat());

		try {
			if (utilisateurRepository.existsById(p.getId())) {
				if(!utilisateur.getReservation()&&p.getReservation()) {
				Optional<TableResa>tableopt=this.tableResaRepository.findtableParUtilisateur(utilisateur.getId());
				if(tableopt.isPresent()) {
				tableopt.get().setReservationEnCours(false);
				tableopt.get().setUtilisateur(null);
				this.tableResaRepository.save(tableopt.get());
				}
				}

				if (p.getAdresseMail().equals(utilisateur.getAdresseMail())) {
					
					this.utilisateurRepository.save(p);

				} else {
					p.setAdresseMail(utilisateur.getAdresseMail());
					this.utilisateurRepository.save(p);

				}
				
				
				logger.info("l'utilisateur "+ p.getUsername()+" "+p.getId()+" a bien été mis a jour");
				return p.getId();
			} else {
				return -1;
			}
		} catch (Exception e) {
			logger.error("l'utilisateur "+utilisateur.getUsername()+" "+utilisateur.getId()+" n'a pas été mis a jour "+e.getMessage());
			return -1;
		}

	}

	@Override
	public List<UtilisateurDto> chercherTousLesUtilisateurs(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);

		utilisateurRepository.findAll(firstPageWithFiveElements);

		List<UtilisateurDto> maliste = this.utilisateurRepository.findAll(firstPageWithFiveElements).stream()
				.map(e -> UtilisateurDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.adresseMail(e.getAdresseMail())
						.actif(e.getActif())
						.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
						.password(e.getPassword())
						.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
//						.listHistoriqueAchat(HistoriqueDAchatDto.builder().id(e.getListHistoriqueAchat().getId())
//								.dateAchat(e.getListHistoriqueAchat().getDateAchat())
//								.listDeBiere(e.getListHistoriqueAchat().getListDeBiere())
//								.prixTotal(e.getListHistoriqueAchat().getPrixTotal()).build())
						.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;

	}

	@Override
	public List<UtilisateurDto> listerParRole(int page,int id) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		List<UtilisateurDto> maliste = this.utilisateurRepository.UtilisateursByRole(firstPageWithFiveElements,id).stream()
				.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
						.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
						.password(e.getPassword())
						.actif(e.getActif())
						.listHistoriqueAchat(HistoriqueDAchatDto.builder().id(e.getListHistoriqueAchat().getId())
								.dateAchat(e.getListHistoriqueAchat().getDateAchat())
								.listDeBiere(e.getListHistoriqueAchat().getListDeBiere())
								.prixTotal(e.getListHistoriqueAchat().getPrixTotal()).build())
						.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
						.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public List<UtilisateurDto> listerByFilter(int page, Boolean active, int role, String sorting) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		if(sorting.equalsIgnoreCase("rien")) {
			if(active==null) {
				List<UtilisateurDto> maliste = this.utilisateurRepository.UtilisateursByRole(firstPageWithFiveElements,role).stream()
						.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
								.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
								.password(e.getPassword())
								.actif(e.getActif())
								.listHistoriqueAchat(HistoriqueDAchatDto.builder().id(e.getListHistoriqueAchat().getId())
										.dateAchat(e.getListHistoriqueAchat().getDateAchat())
										.listDeBiere(e.getListHistoriqueAchat().getListDeBiere())
										.prixTotal(e.getListHistoriqueAchat().getPrixTotal()).build())
								.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
								.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
				
			}if(active!=null&&role==0) {
				List<UtilisateurDto> maliste = this.utilisateurRepository.UtilisateursActive(firstPageWithFiveElements, active).stream()
						.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
								.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
								.password(e.getPassword())
								.actif(e.getActif())
								.listHistoriqueAchat(HistoriqueDAchatDto.builder().id(e.getListHistoriqueAchat().getId())
										.dateAchat(e.getListHistoriqueAchat().getDateAchat())
										.listDeBiere(e.getListHistoriqueAchat().getListDeBiere())
										.prixTotal(e.getListHistoriqueAchat().getPrixTotal()).build())
								.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
								.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
				
			}if(active!=null&&role!=0) {
				List<UtilisateurDto> maliste = this.utilisateurRepository.UtilisateursByRoleActif(firstPageWithFiveElements,role, active).stream()
						.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
								.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
								.password(e.getPassword())
								.actif(e.getActif())
								.listHistoriqueAchat(HistoriqueDAchatDto.builder().id(e.getListHistoriqueAchat().getId())
										.dateAchat(e.getListHistoriqueAchat().getDateAchat())
										.listDeBiere(e.getListHistoriqueAchat().getListDeBiere())
										.prixTotal(e.getListHistoriqueAchat().getPrixTotal()).build())
								.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
								.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
						.collect(Collectors.toList());
				fin = maliste.size();
				return maliste;
			}
			
		}else {
			
		}
		return null;
	}

	@Override
	public List<UtilisateurDto> listerParNom(String user) {
		List<UtilisateurDto> maliste = this.utilisateurRepository.findAllUtilisateurParNom(user).stream()
				.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
						.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
						.password(e.getPassword())
						.actif(e.getActif())
						.listHistoriqueAchat(HistoriqueDAchatDto.builder().id(e.getListHistoriqueAchat().getId())
								.dateAchat(e.getListHistoriqueAchat().getDateAchat())
								.listDeBiere(e.getListHistoriqueAchat().getListDeBiere())
								.prixTotal(e.getListHistoriqueAchat().getPrixTotal()).build())
						.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
						.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public void findAllNeverActivate() {
		Long date=new Date().getTime();
		List<UtilisateurDto> maliste=this.utilisateurRepository.UtilisateursNeverActivated()
		.stream()
		.map(e -> UtilisateurDto.builder().id(e.getId()).nom(e.getNom()).adresseMail(e.getAdresseMail())
				.dateAge(e.getDateAge()).prenom(e.getPrenom()).username(e.getUsername())
				.password(e.getPassword())
				.actif(e.getActif())
				.activated(e.getActivated())
				.heureActivation(e.getHeureActivation())
				
				.panier(PanierDto.builder().id(e.getPanier().getId()).nom(e.getPanier().getNom()).build())
				.role(RoleDto.builder().id(e.getRole().getId()).nom(e.getRole().getNom()).build()).build())
		.collect(Collectors.toList());
		
		for (UtilisateurDto utilisateurDto : maliste) {
			if(date-utilisateurDto.getHeureActivation()>86400000) {
				this.utilisateurRepository.deleteById(utilisateurDto.getId());
			}
			
		}

	}

}
