package com.afpa.labonnebiere.service.interf;

import org.springframework.security.core.Authentication;

import com.afpa.labonnebiere.security.model.AuthenticationRequest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

public interface IAuthenticationService {

	Authentication getAuthentication(Jws<Claims> request);
	Authentication authenticate(AuthenticationRequest authenticationRequest);

}

