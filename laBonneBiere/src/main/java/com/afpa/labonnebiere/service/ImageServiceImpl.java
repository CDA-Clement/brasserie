package com.afpa.labonnebiere.service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.BiereRepository;
import com.afpa.labonnebiere.dao.ImageBiereRepository;
import com.afpa.labonnebiere.dao.ImageTableRepository;
import com.afpa.labonnebiere.dao.TableResaRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.ImageBiereDto;
import com.afpa.labonnebiere.dto.ImageTableDto;
import com.afpa.labonnebiere.dto.TypeDeBiereDto;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.ImageBiere;
import com.afpa.labonnebiere.entity.ImageTable;
import com.afpa.labonnebiere.entity.TableResa;
import com.afpa.labonnebiere.entity.TypeDeBiere;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.IImageService;


@Service
public class ImageServiceImpl implements IImageService {
	static int INC = 20;
	public static int fin;
	private final static Logger logger = Logger.getLogger(ImageServiceImpl.class);
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private ImageBiereRepository imageBiereRepository;
	@Autowired
	private BiereRepository biereRepository;
	@Autowired
	private TableResaRepository tableRepository;
	@Autowired
	private ImageTableRepository imageTableRepository;


	@Override
	public List<ImageBiereDto> chercherToutesLesImagesBieres(int page) {
		Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		
		
		List<ImageBiereDto> maliste = this.imageBiereRepository.findAll(firstPageWithFiveElements)
				.stream()
				.map(e->ImageBiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
		
		
	}

	
	@Override
	public Optional<ImageBiereDto> findById(int id) {
		Optional<ImageBiere> prod = this.imageBiereRepository.findById(id);
		Optional<ImageBiereDto> res = Optional.empty();
		if(prod.isPresent()) {
			ImageBiere p = prod.get();
			ImageBiereDto prodDto = this.modelMapper.map(p, ImageBiereDto.class);
			res = Optional.of(prodDto);
		}
		return res;
	}
	
	@Override
	public Boolean supprimerId(int id) {
		if(this.imageBiereRepository.existsById(id)) {
			this.imageBiereRepository.deleteById(id);
			logger.info("suppression de l'image "+id);
			
			return true;
		}else {
			logger.warn("impossible de supprimer l'image "+id);
			return false;
		}
	}
	
	@Override
	public Integer ajouter(ImageBiereDto image,ImageBiereDto image2, String nomBiere) {
		String cap = nomBiere.substring(0, 1).toUpperCase() + nomBiere.substring(1);
		nomBiere=cap;
		
		
		String cap1 = image.getNom().substring(0, 1).toUpperCase() + image.getNom().substring(1);
		image.setNom(cap1);
		
		String cap2 = image2.getNom().substring(0, 1).toUpperCase() + image2.getNom().substring(1);
		image2.setNom(cap2);
		
		ImageBiere i = this.modelMapper.map(image,ImageBiere.class);
		ImageBiere i2 = this.modelMapper.map(image2,ImageBiere.class);
		
		try {
			this.imageBiereRepository.save(i);
			this.imageBiereRepository.save(i2);
			logger.info("image "+i.getId()+" "+i2.getId()+" ont bien été enregistrées");
		} catch (Exception e) {
			logger.error("les images "+i.getId()+" "+i2.getId()+" n'ont pas été enregistrées");
			return -1;
		}
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		Optional<Biere>biere=this.biereRepository.findByNom(nomBiere);
		if(biere.isPresent()) {
			Biere b=biere.get();
			if(b.getImageBiere().size()==0) {
				b.getImageBiere().add(i);
				b.getImageBiere().add(i2);
				this.biereRepository.save(b);
				logger.info("les images ont bien été liées à la biere");
			}else {
				return -1;
			}
			
		}else {
			return-2;
		}
		return i.getId();
		
		
	}
	
	@Override
	public Integer ajouterTable(ImageTableDto image, String nomTable) {

		ImageTable i = this.modelMapper.map(image,ImageTable.class);
		
		
		try {
			this.imageTableRepository.save(i);
			logger.info("image "+i.getId()+" a bien été enregistrée");
		} catch (Exception e) {
			logger.error("les images "+i.getId()+" a pas été enregistrée");
			return -1;
		}
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		Optional<TableResa>table=this.tableRepository.findByNom(nomTable);
		if(table.isPresent()) {
			TableResa t=table.get();
			if(t.getImageTable()==null) {
				t.setImageTable(i);
				this.tableRepository.save(t);
				logger.info("les images ont bien été liées à la biere");
			}else {
				return -1;
			}
			
		}else {
			return-2;
		}
		return i.getId();
		
		
	}
//	@Override
//	public Integer edit(ImageBiereDto image, String nomBiere) {
//		String cap = nomBiere.substring(0, 1).toUpperCase() + nomBiere.substring(1);
//		nomBiere=cap;
//		String cap1 = image.getNom().substring(0, 1).toUpperCase() + image.getNom().substring(1);
//		image.setNom(cap1);
//		ImageBiere i = this.modelMapper.map(image,ImageBiere.class);
//		
//		
//		try {
//			this.imageBiereRepository.save(i);
//			
//			logger.info("image "+i.getId()+" ont bien été enregistrées");
//		} catch (Exception e) {
//			logger.error("les images "+i.getId()+" n'ont pas été enregistrées");
//			return -1;
//		}
//		try {
//			TimeUnit.SECONDS.sleep(1);
//		} catch (InterruptedException e) {
//			
//			e.printStackTrace();
//		}
//		Optional<Biere>biere=this.biereRepository.findByNom(nomBiere);
//		if(biere.isPresent()) {
//			Biere b=biere.get();
//			if(b.getImageBiere().size()>0) {
//				int index=-1;
//				for (ImageBiere img : b.getImageBiere()) {
//					if(img.getNom().equalsIgnoreCase(cap1)) {
//						index=b.getImageBiere().indexOf(img);
//					}
//				}
//				b.getImageBiere().remove(index);
//				b.getImageBiere().add(i);
//				this.biereRepository.save(b);
//				logger.info("les images ont bien été liées à la biere");
//			}else {
//				return -1;
//			}
//			
//		}else {
//			return-2;
//		}
//		return i.getId();
//		
//		
//	}


	@Override
	public Optional<ImageBiereDto> trouverParnom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		
		Optional<ImageBiere> prod = this.imageBiereRepository.findByNom(cap);
		Optional<ImageBiereDto> res = Optional.empty();
		if(prod.isPresent()) {
			ImageBiere p = prod.get();
			
			ImageBiereDto prodDto = this.modelMapper.map(p, ImageBiereDto.class);

			res = Optional.of(prodDto);
		}
		return res;	}


	@Override
	public List<ImageBiereDto> findParBiere(int id) {
		
		List<ImageBiereDto> maliste = this.imageBiereRepository.findBiereImage(id)
				.stream()
				.map(e->ImageBiereDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}


}
