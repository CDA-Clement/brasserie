package com.afpa.labonnebiere.service.interf;

import java.util.List;

import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.dto.PanierDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;

public interface IPanierService {
	
	public List<PanierDto> chercherTousLesPaniers(int page);

	

	Boolean supprimerId(int id);


	Integer ajouter(PanierDto prod, int id);


	PanierDto AfficherPanierParUtilisateur(int id);



	public int SupprimerPanier(UtilisateurDto u);



	public Integer Maj(List<CommandeDto> commande) throws Exception;

	


	

}
