package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import com.afpa.labonnebiere.dto.ImageBiereDto;
import com.afpa.labonnebiere.dto.ImageTableDto;

public interface IImageService {
	
	public List<ImageBiereDto> chercherToutesLesImagesBieres(int page);

	public Optional<ImageBiereDto> findById(int id);

	public Boolean supprimerId(int id);
	
	public Optional<ImageBiereDto> trouverParnom(String nom);


	public Integer ajouter(ImageBiereDto image, ImageBiereDto image2, String nomBiere);

	public List<ImageBiereDto> findParBiere(int id);

	public Integer ajouterTable(ImageTableDto image, String nomTable);

	//public Integer edit(ImageBiereDto image, String nomBiere);

	

}
