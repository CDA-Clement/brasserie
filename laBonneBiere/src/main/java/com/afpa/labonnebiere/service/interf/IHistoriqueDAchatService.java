package com.afpa.labonnebiere.service.interf;

import java.util.List;
import java.util.Optional;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.HistoriqueDAchatDto;

public interface IHistoriqueDAchatService {
	
	public List<HistoriqueDAchatDto> chercherTousLesHistoriquesDAchatPage(int page);
	
	public List<HistoriqueDAchatDto> chercherTousLesHistoriquesDAchat();

	public Optional<HistoriqueDAchatDto> findById(int id);
	
	public Optional<HistoriqueDAchatDto> findByIdByUser(int id, int idUser);

	public List<HistoriqueDAchatDto> findByIdUser(int page,int idUser);
	
	public List<HistoriqueDAchatDto> findByMontant(int idUser, double montant);
	
	public List<HistoriqueDAchatDto> findAllByIdUser(int idUser);
	
	public List<HistoriqueDAchatDto> findByIdDateBetween(int page,int idUser,String debut,String fin);
	
	public List<HistoriqueDAchatDto> findByIdDateBefore(int page,int idUser,String debut);
	
	public List<HistoriqueDAchatDto> findByIdDateAfter(int page,int idUser,String fin);
	
	public Boolean supprimerById(int id);

	public List<HistoriqueDAchatDto> findAllByIdDateBetween(int idUser,String debut, String fin);
	
	public List<String> findMostOccurences(int idUser);

	public Optional<HistoriqueDAchatDto> findBynom(String biere);

	public Object findBiereMoment();



}
