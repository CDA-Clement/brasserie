package com.afpa.labonnebiere.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.catalina.Executor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.service.interf.ICleanService;

@Service
public class CleanServiceImpl implements ICleanService  {
	private final static Logger logger = Logger.getLogger(CleanServiceImpl.class);
	private static Integer nb=0;
	@Autowired
	private BiereServiceImpl biereService;
	
	@Autowired
	private UtilisateurServiceImpl utilisateurService;
	
	@Autowired
	private CommandeServiceImpl commandeService;
	
	@Autowired
	private TableServiceImpl tableService;
	
	
	
	@Override
	@Async
	public void CleanDB() throws InterruptedException {
		Integer date=Calendar.HOUR_OF_DAY;
		if(date>=0&&date<6) {
			if(this.tableService.resetAll()){
				logger.info("reset des réservations de tables journalière");
			}else {
				logger.warn("probleme lors du reset des réservations de tables journalière");
			}
			
			
		}
		if(nb%4==0) {
			logger.info("nettoyage des utilisateurs non activés de la B.D.D");
			this.utilisateurService.findAllNeverActivate();
			
		}
		logger.info("nettoyage des commandes de la B.D.D");
		List<CommandeDto> commandes=this.commandeService.chercherTouteLesCommandes();
		List<BiereDto> bieres=biereService.findAll();

		for (CommandeDto c : commandes) {
			for (BiereDto b : bieres) {
				if (b.getNom().equals(c.getNom())) {
					int cmd = c.getCommande();
					b.setQuantite(b.getQuantite() + cmd);
					this.biereService.ajouter(b);
					this.commandeService.supprimerId(c.getId());

				}
			}

		}
		
		nb++;
		Thread.sleep(21600000L);
		CleanDB();
	  }

}
