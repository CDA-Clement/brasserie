package com.afpa.labonnebiere.service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.afpa.labonnebiere.dao.RoleRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.dto.RoleDto;
import com.afpa.labonnebiere.entity.Role;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.service.interf.IRoleService;



@Service
public class RoleServiceImpl implements IRoleService {
	static int INC = 5;
	public static int fin;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;



	
	@Override
	public Optional<RoleDto> findById(int id) {
		Optional<Role> prod = this.roleRepository.findById(id);
		Optional<RoleDto> res = Optional.empty();
		if(prod.isPresent()) {
			Role p = prod.get();
			RoleDto roleDto = this.modelMapper.map(p, RoleDto.class);
			res = Optional.of(roleDto);
		}
		return res;
	}
	
	@Override
	public Boolean supprimerId(int id) {
		Optional<Role> prod=this.roleRepository.findById(id);
		if(prod.isPresent()) {
			List<Utilisateur> list=utilisateurRepository.UtilisateursByRole(prod.get().getId());
			if(list.size()>0) {
				for (Utilisateur utilisateur : list) {
					utilisateur.setRole(Role.builder().Id(0).build());
					utilisateurRepository.save(utilisateur);
					
				}
			}
			
			
			this.roleRepository.deleteById(id);
			return true;
		}else {
			
			return false;
		}
		
	}
	
	
//	@Override
//	public Boolean suprimerRoleActif(int id) {
//		//je ne sais pas encore si on doit l'utiliser ou pas a voir
//		return null;
//	}
//	
	
	@Override
	public Boolean ajouter(String role) {
		String cap = role.substring(0, 1).toUpperCase() + role.substring(1);
		
		Role p = Role.builder().nom(cap).build();
		try {
			
			this.roleRepository.save(p);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
//	
//	@Override
//	public Integer Maj(RoleDto role) {
//		String cap = role.getNom().substring(0, 1).toUpperCase() + role.getNom().substring(1);
//		role.setNom(cap);
//		Role p = this.modelMapper.map(role,Role.class);
//		
//		
//		try {
//			
//			if(roleRepository.existsById(role.getId())) {
//				this.roleRepository.save(p);	
//				return p.getId();
//			}else {
//				return 0;
//			}
//		} catch (Exception e) {
//			
//			
//			return 0;
//		}
//		
//	}
	


	@Override
	public List<RoleDto> chercherTousLesTypesDeRoles(int page) {
Pageable firstPageWithFiveElements = PageRequest.of(page, INC);
		
		roleRepository.findAll(firstPageWithFiveElements);
		
		List<RoleDto> maliste = this.roleRepository.findAll()
				.stream()
				.map(e->RoleDto.builder()
						.id(e.getId())
						.nom(e.getNom())
						.build())
				.collect(Collectors.toList());
		fin = maliste.size();
		return maliste;
	}

	@Override
	public Optional<RoleDto> trouverParnom(String nom) {
		String cap = nom.substring(0, 1).toUpperCase() + nom.substring(1);
		Optional<Role> prod = this.roleRepository.findByNom(cap);
		Optional<RoleDto> res = Optional.empty();
		if(prod.isPresent()) {
			Role p = prod.get();
			RoleDto roleDto = this.modelMapper.map(p, RoleDto.class);
			res = Optional.of(roleDto);
		}
		return res;
	}

	


}
