package com.afpa.labonnebiere.security.model;

public class AuthenticationRequest {
    public String username;
    public String password;
}
