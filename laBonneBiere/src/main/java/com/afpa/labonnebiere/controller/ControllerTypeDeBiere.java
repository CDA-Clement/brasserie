package com.afpa.labonnebiere.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.TypeDeBiereDto;
import com.afpa.labonnebiere.service.interf.ITypeDeBiereService;


@RestController
public class ControllerTypeDeBiere {
	int page;

	@Autowired
	ITypeDeBiereService typeDeBiereService;


	@GetMapping(value = "/typeDeBieres")
	public List<TypeDeBiereDto> listTypeDeBiere() {
		return this.typeDeBiereService.chercherToutsLesTypesDeBieres(page);	
	}

	@GetMapping("/typeDeBieres/{type}")
	public ResponseEntity<?> AfficherTypeDeBiere(@PathVariable String type) {
		Optional<TypeDeBiereDto> typebiereOptional=null;
		String regex = "[0-9]+";
		if(type.matches(regex)) {
			int id=Integer.parseInt(type);
			typebiereOptional = this.typeDeBiereService.findById(id);
		}else {
			typebiereOptional = this.typeDeBiereService.trouverParNom(type);

		}
		if (typebiereOptional.isPresent()) {

			return ResponseEntity.ok().body(typebiereOptional.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun typeDeBiere a cet identifiant");
		}

	}
	@PreAuthorize("hasAnyAuthority('Admin')")
	@PostMapping("/typeDeBieres")
	ResponseEntity<Integer> ajouter(@Valid @RequestBody TypeDeBiereDto typeDeBiereDto) throws URISyntaxException {
		if(typeDeBiereDto.getId()==null) {
			Integer result = typeDeBiereService.ajouter(typeDeBiereDto);
			return ResponseEntity.created(new URI("/typeDeBiere/" + result))
					.body(result);

		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
		}
	}
	@PreAuthorize("hasAnyAuthority('Admin')")
	@PutMapping("/typeDeBieres")
	ResponseEntity<Boolean> mettreAJourTypeDeBiere(
			@Valid 
			@RequestBody TypeDeBiereDto prod
			) throws URISyntaxException {
		if(prod.getId()!=null) {
			Integer result = typeDeBiereService.Maj(prod);
			if(result!=0) {
				return ResponseEntity.ok().body(true);	
			}else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
			}

		}else {

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);

		}

	}
	@PreAuthorize("hasAnyAuthority('Admin')")
	@DeleteMapping("/typeDeBieres/{nom}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable String nom) {
		Boolean bool=false;
		String regex = "[0-9]+";
		if(nom.matches(regex)) {
			int id=Integer.parseInt(nom);
			bool=typeDeBiereService.supprimerId(id);
		}else {
			Optional<TypeDeBiereDto> biereOptional=this.typeDeBiereService.trouverParNom(nom);
			if(biereOptional.isPresent()) {
				bool=typeDeBiereService.supprimerId(biereOptional.get().getId());

			}
		}
		
		if(bool==true) {
			
			return ResponseEntity.ok().body(bool);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}
	}
}



