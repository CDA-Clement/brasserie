package com.afpa.labonnebiere.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.apache.pdfbox.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.HistoriqueDAchatDto;
import com.afpa.labonnebiere.service.interf.IHistoriqueDAchatService; 
@RestController
public class ControllerHistoriqueDAchat {
	private int page;
	private final static Logger logger = Logger.getLogger(ControllerHistoriqueDAchat.class);

	@Autowired
	IHistoriqueDAchatService historiqueDAchatService;

	@Value("${pathInvoices.invoices.path}")
	private String path;
	
	

	@GetMapping(value = "/achats/{page}/{idUser}")
	public List<HistoriqueDAchatDto> listhistoriqueAchat(
			@PathVariable String page,
			@PathVariable Integer idUser) {
		return this.historiqueDAchatService.findByIdUser(Integer.parseInt(page),idUser);
	}
	
	@GetMapping(value = "/achats/{idUser}")
	public List<HistoriqueDAchatDto> allHisthistoriqueAchat(
			@PathVariable Integer idUser) {
		return this.historiqueDAchatService.findAllByIdUser(idUser);
	}

	@GetMapping("/achatsPeriod/{page}/{idUser}/{debut}/{fin}")
	public ResponseEntity<?> AfficherFactureBetweenPeriode(
			@PathVariable String page,
			@PathVariable String idUser,
			@PathVariable String debut,
			@PathVariable String fin) {
		
		List<HistoriqueDAchatDto> listhistoiqueOptional= new ArrayList<HistoriqueDAchatDto>();

		listhistoiqueOptional=(this.historiqueDAchatService.findByIdDateBetween(Integer.parseInt(page), Integer.parseInt(idUser), debut, fin));
		
			return ResponseEntity.ok().body(listhistoiqueOptional); 
		

	}
	
	@GetMapping("/achatsPeriod/{idUser}/{debut}/{fin}")
	public ResponseEntity<?> AfficherAllFactureBetweenPeriode(
			@PathVariable String idUser,
			@PathVariable String debut,
			@PathVariable String fin) {
		debut=debut.replace("-", " ");
		debut=debut.replace(".", "-");
		fin=fin.replace("-", " ");
		fin=fin.replace(".", "-");
		List<HistoriqueDAchatDto> listhistoiqueOptional= new ArrayList<HistoriqueDAchatDto>();
		listhistoiqueOptional=(this.historiqueDAchatService.findAllByIdDateBetween(Integer.parseInt(idUser),debut, fin));
		
			return ResponseEntity.ok().body(listhistoiqueOptional); 
		
	}
	
	@GetMapping("/achatsAfter/{page}/{idUser}/{debut}")
	public ResponseEntity<?> AfficherFactureApres(
			@PathVariable String page,
			@PathVariable String idUser,
			@PathVariable String debut) {
		
		List<HistoriqueDAchatDto> listhistoiqueOptional= new ArrayList<HistoriqueDAchatDto>();

		listhistoiqueOptional=(this.historiqueDAchatService.findByIdDateAfter(Integer.parseInt(page), Integer.parseInt(idUser), debut));
		
			return ResponseEntity.ok().body(listhistoiqueOptional); 

	}
	
	@GetMapping("/achatsBefore/{page}/{idUser}/{fin}")
	public ResponseEntity<?> AfficherFactureAvant(
			@PathVariable String page,
			@PathVariable String idUser,
			@PathVariable String fin) {
		
		List<HistoriqueDAchatDto> listhistoiqueOptional= new ArrayList<HistoriqueDAchatDto>();

		listhistoiqueOptional=(this.historiqueDAchatService.findByIdDateBefore(Integer.parseInt(page), Integer.parseInt(idUser), fin));
		

			return ResponseEntity.ok().body(listhistoiqueOptional); 

	}
	
	@GetMapping("/achat/{user}/{idFacture}")
	public ResponseEntity<?> trouverFacture(
			@PathVariable String user,
			@PathVariable int idFacture) {
		Optional<HistoriqueDAchatDto> factureOptional=null;
		HistoriqueDAchatDto facture=new HistoriqueDAchatDto();
		
		logger.info("utilisateur souhaite afficher  la biere" +user);
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);
			factureOptional= this.historiqueDAchatService.findByIdByUser(idFacture,Integer.parseInt(user));
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("aucune facture a cet identifiant");
		}
		
		if (factureOptional.isPresent()) {
			return ResponseEntity.ok().body(factureOptional.get()); 
		}else {
			logger.warn("aucune biere à cet identifiant"+user.toString());
			return ResponseEntity.ok().body(facture);
		}

	}
	
	@GetMapping("/achat/price/{user}/{montant}")
	public ResponseEntity<?> trouverFactureParMontant(
			@PathVariable String user,
			@PathVariable Double montant) {
		List<HistoriqueDAchatDto> factureOptional=null;
		logger.info("utilisateur souhaite afficher  la biere" +user);
			factureOptional= this.historiqueDAchatService.findByMontant(Integer.parseInt(user),montant);
		
			return ResponseEntity.ok().body(factureOptional); 
		

	}
	
	
	@GetMapping("/achats/biereFavorite/{user}")
	public ResponseEntity<?> trouverbierefavorite(@PathVariable String user) {
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);
			return ResponseEntity.status(HttpStatus.OK).body(this.historiqueDAchatService.findMostOccurences(id));
		
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("aucun achat effectué");
		}
	}
	
	@GetMapping("/achats/biereMoment")
	public ResponseEntity<?> trouverbiereDuMoment() {
		
			return ResponseEntity.status(HttpStatus.OK).body(this.historiqueDAchatService.findBiereMoment());
		
	}
	
	@GetMapping("/achats/downloadInvoice/{user}/{lastInvoiceId}")
	public ResponseEntity<?> downloadInvoice(
							@PathVariable Integer user,
							@PathVariable Integer lastInvoiceId) {
		byte[] file;
	//	File file1=new File(path+"invoice-"+user+"-"+lastInvoiceId+".pdf");
		try {
			file = IOUtils.toByteArray(new FileInputStream(path+"invoice-"+user+"-"+lastInvoiceId+".pdf"));
			return ResponseEntity
            .ok()
            .contentType(MediaType.APPLICATION_PDF)
            .body(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("utilisateur souhaite afficher  la biere" +user);
		
			return ResponseEntity.ok().body("telechargement impossible"); 
		
			
		}

//	@Transactional
//	@GetMapping("/bieres/type/{user}")
//	public ResponseEntity<?> AfficherBiereParType(@PathVariable String user) {
//		List<BiereDto> biereOptional=null;
//		logger.info("utilisateur souhaite un affichage filtré" +user);
//		String regex = "[0-9]+";
//		if(user.matches(regex)) {
//			
//			int id=Integer.parseInt(user);
//			biereOptional=this.biereService.listerParTypeDeBiere(id);
//			return ResponseEntity.ok().body(biereOptional); 
//		}else {
//			Optional<TypeDeBiereDto>option=this.typeBiereService.trouverParNom(user);
//			if(option.isPresent()) {
//				biereOptional=this.biereService.listerParTypeDeBiere(option.get().getId());
//				return ResponseEntity.ok().body(biereOptional);
//			}
//
//		}
//			logger.warn("aucune biere trouvée avec filtre"+user.toString());
//			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucune biere a cet identifiant");
//
//	}
//
//	@Transactional
//	@PreAuthorize("hasAuthority('Admin')")
//	@PostMapping("/bieres")
//	ResponseEntity<?> ajouter(@Valid @RequestBody BiereDto biereDto) throws URISyntaxException {
//		if(biereDto.getId()==null) {
//			logger.info("user souhaite une création de biere" + biereDto.toString());
//			Integer result = biereService.ajouter(biereDto);
//			if(result>-1) {
//				return ResponseEntity.ok().body(biereDto);
//			}else {
//				logger.warn("Cette biere avec cet identifiant "+biereDto.getNom()+" existe déja");
//				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cette biere "+biereDto.getNom()+ " existe deja");
//			}
//
//		}else {
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
//
//		}
//	}
//	@PreAuthorize("hasAuthority('Admin')")
//	@PutMapping("/bieres")
//	ResponseEntity<Boolean> mettreAJourBiere(
//			@Valid 
//			@RequestBody BiereDto prod
//			) throws URISyntaxException {
//		
//		if(prod.getId()!=null) {
//			Integer result = biereService.ajouter(prod);
//			logger.info("mise à jour de la biere "+ result);
//			return ResponseEntity.ok().body(true);	
//
//
//		}
//		logger.warn("mise à jour de la biere "+prod.getNom()+" mauvaise requete");
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
//
//	}
//	@PreAuthorize("hasAuthority('Admin')")
//	@DeleteMapping("/bieres/{biere}")
//	public ResponseEntity<Boolean> deleteGroup(@PathVariable String biere) {
//		Boolean bool=false;
//		
//		String regex = "[0-9]+";
//		if(biere.matches(regex)) {
//			int id=Integer.parseInt(biere);
//		Optional<BiereDto>bibine=this.biereService.findById(id);
//		if(bibine.isPresent()) {
//			BiereDto bibine2=bibine.get();
//			List<CommentaireDto>liste=this.commentaireService.trouverParBiere(bibine2.getId());
//			if(liste.size()>0) {
//				for (CommentaireDto commentaireDto : liste) {
//					logger.info("suppression de commentaire pour "+bibine2.getNom());
//					this.commentaireService.supprimerId(commentaireDto.getId());
//				}
//			}
//			bool=biereService.supprimerId(id);
//			this.imageBiereService.supprimerId(bibine2.getImageBiere().getId());
//			File file2 =new File(path+bibine2.getImageBiere().getNom());
//			if(file2.exists()) {
//				logger.info("suppression de l'image de "+bibine2.getNom());
//				file2.delete();				
//			}
//		}
//		
//		}else {
//			Optional<BiereDto> biereOptional=this.biereService.trouverParnom(biere);
//			if(biereOptional.isPresent()) {
//					BiereDto bibine2=biereOptional.get();
//					List<CommentaireDto>liste=this.commentaireService.trouverParBiere(bibine2.getId());
//					if(liste.size()>0) {
//						for (CommentaireDto commentaireDto : liste) {
//							logger.info("suppression de commentaire pour "+bibine2.getNom());
//							this.commentaireService.supprimerId(commentaireDto.getId());
//						}
//					}
//					bool=biereService.supprimerId(bibine2.getId());
//					this.imageBiereService.supprimerId(bibine2.getImageBiere().getId());
//					File file2 =new File(path+bibine2.getImageBiere().getNom());
//					if(file2.exists()) {
//						logger.info("suppression de l'image de "+bibine2.getNom());
//						file2.delete();	
//					}
//			}
//			
//		}
//			
//		
//		if(bool==true) {
//			logger.info("suppression de la biere "+biere);
//			return ResponseEntity.ok().body(bool);
//		}else {
//			logger.warn("suppression de la biere "+biere+" à échouée");
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
//		}
//	}

}

