package com.afpa.labonnebiere.controller;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.mapstruct.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.HistoriqueDAchatDto;
import com.afpa.labonnebiere.dto.ReponseDto;
import com.afpa.labonnebiere.dto.ReponseStatut;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.ICommentaireService;
import com.afpa.labonnebiere.service.interf.IHistoriqueDAchatService;
import com.afpa.labonnebiere.service.interf.IImageService;
import com.afpa.labonnebiere.service.interf.ITypeDeBiereService; 
@RestController
public class ControllerBiere {
	
	private final static Logger logger = Logger.getLogger(ControllerBiere.class);

	@Autowired
	IBiereService biereService;
	@Autowired
	IHistoriqueDAchatService historiqueService;

	@Autowired
	ICommentaireService commentaireService;
	@Autowired
	ITypeDeBiereService typeBiereService;
	@Autowired
	IImageService imageBiereService;
	@Value("${pathUpload.photo.path}")
	private String path;
	private String sorting1="";

	@GetMapping(value = "/boutique")
	public List<BiereDto> listAllBiere() {
			return this.biereService.chercherToutesLesBieresSansPage();
		}	
	

	@GetMapping(value = "/boutique/{page}/{active}")
	public List<BiereDto> listBiere(@PathVariable String page,
									@PathVariable String active) {
		logger.info("Affichage de la boutique page "+page);
		if(active.equalsIgnoreCase("True")) {
			return this.biereService.chercherToutesLesBieresActive(Integer.parseInt(page));
			
		}else if(active.equalsIgnoreCase("False")) {
			return this.biereService.chercherToutesLesBieresNonActive(Integer.parseInt(page));
		
		}else {
			return this.biereService.chercherToutesLesBieres(Integer.parseInt(page),0,Float.MAX_VALUE,0,Integer.MAX_VALUE,0,Float.MAX_VALUE, null);
		}
		
	}

	@GetMapping("/bieres/recherche/{user}")
	public ResponseEntity<?> AfficherBiere(@PathVariable String user) {

		List<BiereDto> ListbiereOptional= new ArrayList<BiereDto>();
		logger.info("utilisateur souhaite trier les bières par nom" +user);
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);

			ListbiereOptional.add(this.biereService.findById(id).get());
		}else {

			ListbiereOptional=this.biereService.listerParNom(user);
		}


		if (ListbiereOptional.size()>0) {

			return ResponseEntity.ok().body(ListbiereOptional); 
		}else {
			logger.warn("aucune liste de biere trouvée avec cette recherche "+user.toString());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ListbiereOptional);
		}

	}
	
	@GetMapping("/boutique/Client/recherche/{user}")
	public ResponseEntity<?> AfficherBiereClient(@PathVariable String user) {

		List<BiereDto> ListbiereOptional= new ArrayList<BiereDto>();
		logger.info("utilisateur souhaite trier les bières par nom" +user);
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);

			ListbiereOptional.add(this.biereService.findById(id).get());
		}else {

			ListbiereOptional=this.biereService.listerParNomClient(user);
		}


		if (ListbiereOptional.size()>0) {

			return ResponseEntity.ok().body(ListbiereOptional); 
		}else {
			logger.warn("aucune liste de biere trouvée avec cette recherche "+user.toString());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ListbiereOptional);
		}

	}

	@GetMapping("/biere/{user}")
	public ResponseEntity<?> AfficherBiere1(@PathVariable String user) {
		Optional<BiereDto> biereOptional=null;
		logger.info("utilisateur souhaite afficher  la biere" +user);
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);
			biereOptional= this.biereService.findById(id);
		}else {
			logger.warn("aucune biere à cet identifiant"+user.toString());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("aucune biere a cet identifiant");
		}

		if (biereOptional.isPresent()) {
			
			return ResponseEntity.ok().body(biereOptional.get()); 
		}else {
			logger.warn("aucune biere à cet identifiant"+user.toString());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucune biere a cet identifiant");
		}
		

	}
	
	
	@GetMapping("/biere/deletable/{biere}")
	public boolean supprimable(@PathVariable String biere) {
		Optional<HistoriqueDAchatDto> biereOptional=null;
		biereOptional=this.historiqueService.findBynom(biere);
		return biereOptional.isPresent();
		
		
		
	}
	
	
	@Transactional
	@GetMapping("/bieres/type/{filtre}/{page}/{active}/{noActive}/{prixMin}/{prixMax}/{qteMin}/{qteMax}/{alcMin}/{alcMax}/{sorting}")
	public ResponseEntity<?> AfficherBiereParType(
			@PathVariable ArrayList<Integer> filtre, 
			@PathVariable int page,
			@PathVariable Boolean active,
			@PathVariable Boolean noActive,
			@PathVariable Float prixMin,
			@PathVariable Float prixMax,
			@PathVariable Integer qteMin,
			@PathVariable Integer qteMax,
			@PathVariable Float alcMin,
			@PathVariable Float alcMax,
			@PathVariable String sorting) {
		this.sorting1=sorting;
		if(prixMax<=0) {
			prixMax=Float.MAX_VALUE;
		}
		if(qteMax<=0) {
			qteMax=Integer.MAX_VALUE;
		}
		if(alcMax<=0) {
			alcMax=Float.MAX_VALUE;
		}
		
		if(prixMin==null) {
			prixMin=(float) 0.0;
		}
		if(qteMin==null) {
			qteMin=0;
		}
		
		if(alcMin==null) {
			alcMin=(float) 0.0;
		}
		
		if(!active&&!noActive) {
			active=null;
		}
		else if(active&&!noActive) {
			active=true;
		}
		else if(!active&&noActive) {
			active=false;
		}
		
		List<BiereDto> biereOptional=null;
		List<BiereDto> listFin=new ArrayList<>();
		logger.info("utilisateur souhaite un affichage filtré" +filtre);
		if(filtre.get(0)!=-1) {
			if(sorting.equals("rien")) {
				for (Integer f : filtre) {
					biereOptional=this.biereService.listerParTypeDeBiere(f,prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,active);
					for (BiereDto b : biereOptional) {
						listFin.add(b);
					}			
				}
			}else {
				for (Integer f : filtre) {
					biereOptional=this.biereService.listerParTypeDeBiereOrder(f,prixMin, prixMax, qteMin, qteMax, alcMin, alcMax,sorting,active);
					for (BiereDto b : biereOptional) {
						listFin.add(b);
					}			
				}
				
				
			
				
				
			}
			
		}else {
			if(sorting.equals("rien")) {
				listFin=this.biereService.chercherToutesLesBieres(page,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax,active);				
			}else {
				listFin=this.biereService.chercherToutesLesBieresOrder(page,prixMin,prixMax, qteMin, qteMax, alcMin, alcMax, sorting,active);	
			}
		}
		logger.warn("aucune biere trouvée avec filtre"+filtre.toString());
			return ResponseEntity.ok().body(listFin); 
		

	}

	@Transactional
	@PreAuthorize("hasAuthority('Admin')")
	@PostMapping("/bieres")
	ResponseEntity<?> ajouter(@Valid @RequestBody BiereDto biereDto) throws URISyntaxException {
		if(biereDto.getId()==null) {
			biereDto.setActive(true);
			logger.info("user souhaite une création de biere" + biereDto.toString());
			Integer result = biereService.ajouter(biereDto);
			if(result>-1) {
				return ResponseEntity.ok().body(biereDto);
			}else {
				logger.warn("Cette biere avec cet identifiant "+biereDto.getNom()+" existe déja");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cette biere "+biereDto.getNom()+ " existe deja");
			}

		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);

		}
	}
	@PreAuthorize("hasAuthority('Admin')")
	@PutMapping("/bieres")
	ResponseEntity<Boolean> mettreAJourBiere(
			@Valid 
			@RequestBody BiereDto prod
			) throws URISyntaxException {

		if(prod.getId()!=null) {
			Integer result = biereService.ajouter(prod);
			logger.info("mise à jour de la biere "+ result);
			return ResponseEntity.ok().body(true);	


		}
		logger.warn("mise à jour de la biere "+prod.getNom()+" mauvaise requete");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);

	}
	@PreAuthorize("hasAuthority('Admin')")
	@DeleteMapping("/bieres/{biere}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable int biere) {
		Boolean b=false;
		
			b=this.biereService.supprimerId(biere);
			logger.info("suppression de la biere "+biere);
			return ResponseEntity.ok().body(b);

		
	}
	
	
	

	
}
	
	


