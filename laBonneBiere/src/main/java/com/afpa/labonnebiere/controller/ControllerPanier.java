package com.afpa.labonnebiere.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.dto.PanierDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.service.BiereServiceImpl;
import com.afpa.labonnebiere.service.interf.IPanierService;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;


@RestController
public class ControllerPanier {
	int page;

	@Autowired
	IUtilisateurService utilisateurService;
	@Autowired
	IPanierService panierService;
	private final static Logger logger = Logger.getLogger(ControllerPanier.class);

	@Transactional
	@GetMapping("/paniers/{user}")
	public ResponseEntity<?> AfficherPaniers(@PathVariable String user){
		
		Optional<UtilisateurDto>optionalUtilisateur=this.utilisateurService.trouverParUsername(user);
		if(optionalUtilisateur.isPresent()) {
			PanierDto panier=panierService.AfficherPanierParUtilisateur(optionalUtilisateur.get().getPanier().getId());
			if(panier==null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("mauvaise requete");
			}else {
				return ResponseEntity.ok().body(panier);
			}
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}
		
	}
	

	@PostMapping("/paniers/{user}")
	public ResponseEntity<?> ajouterBierePaniers(@RequestBody BiereDto biere,
			@PathVariable String user){
		Optional<UtilisateurDto>optionalUtilisateur=this.utilisateurService.trouverParUsername(user);
		if(optionalUtilisateur.isPresent()) {
			UtilisateurDto u=optionalUtilisateur.get();

			this.utilisateurService.ajoutBiere(u, biere);
			return ResponseEntity.ok().body(optionalUtilisateur.get().getPanier().getListDeCommande());

		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}

	}


	@Transactional
	@PutMapping("/paniers/{user}")
	public ResponseEntity<?> ajouterBierePaniers(@RequestBody List<CommandeDto>commande,
			@PathVariable String user){
		Optional<UtilisateurDto>optionalUtilisateur=this.utilisateurService.trouverParUsername(user);
		if(optionalUtilisateur.isPresent()) {
			UtilisateurDto u=optionalUtilisateur.get();
			try {
				this.panierService.Maj(commande);
			
			} catch (Exception e) {
				logger.error("creation de la commande "+commande.toString()+" a échouée");
			}
			return ResponseEntity.ok().body(optionalUtilisateur.get().getPanier());
			
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}
		
	}
	
	
	@DeleteMapping("/paniers/{user}")
	public ResponseEntity<?> supprimerBierePanier(
			@PathVariable String user){
		
		String cap = user.substring(0, 1).toUpperCase() + user.substring(1);
		user=cap;
		
		Optional<UtilisateurDto>optionalUtilisateur=this.utilisateurService.trouverParUsername(user);
		if(optionalUtilisateur.isPresent()) {
			UtilisateurDto u=optionalUtilisateur.get();
			
//			for (CommandeDto commande : u.getPanier().getListDeCommande()) {
//				int id=this.utilisateurService.supprimerBiere(u, commande.getNom(), commande.getCommande());
//			}
			
			int id =this.panierService.SupprimerPanier(u);
			
			return ResponseEntity.ok().body(id);
			
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}
		
	}
	
	

}

