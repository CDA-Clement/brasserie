package com.afpa.labonnebiere.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.ReponseDto;
import com.afpa.labonnebiere.dto.ReponseStatut;
import com.afpa.labonnebiere.dto.TableResaDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.service.interf.ITableResaService;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;


@RestController
public class ControllerTableResa {
	
	private final static Logger logger = Logger.getLogger(ControllerBiere.class);

	@Autowired
	ITableResaService tableResaService;
	@Autowired
	IUtilisateurService utilisateurService;


	@GetMapping(value = "/tables/All")
	public ResponseEntity<?> listTablesReservation() {
		List<TableResaDto> liste =this.tableResaService.chercherToutesLesTables();
		return ResponseEntity.ok().body(liste); 
	}
	
	@GetMapping(value = "/tables/All/{page}")
	public ResponseEntity<?> listTablesReservation(@PathVariable Integer page) {
		TableResaDto table =this.tableResaService.chercherToutesLesTablesPage(page);
		return ResponseEntity.ok().body(table); 
	}

	@GetMapping("/tables/disponible/{dispo}")
	public ResponseEntity<?> AfficherTableDiponible(@PathVariable Integer dispo) {
		Boolean disponible=false;
		if(dispo==1) {
			disponible=true;
		}
		if(dispo!=1 &&dispo!=0) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("attention la requete n'est pas bonne");
		}
		
		
			List<TableResaDto> tableResaDispo = this.tableResaService.listerParDisponibilite(disponible);

		if (tableResaDispo.size()>0) {

			return ResponseEntity.ok().body(tableResaDispo); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucune table de disponible");
		}

	}
	
	@GetMapping("/tables/recherche/{recherche}")
	public ResponseEntity<?> AfficherTableDiponible(@PathVariable String recherche) {
		List<TableResaDto> ListTableOptional= new ArrayList<TableResaDto>();
		logger.info("utilisateur souhaite trier les bières par nom" +recherche);
			ListTableOptional=this.tableResaService.listerParNom(recherche);
		
		if (ListTableOptional.size()>0) {

			return ResponseEntity.ok().body(ListTableOptional); 
		}else {
			logger.warn("aucune liste de biere trouvée avec cette recherche "+recherche);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucune biere a cet identifiant");
		}
		

	}
	
	@GetMapping("/tables/{id}")
	public ResponseEntity<?> AfficherTableParNom(@PathVariable int id) {
			
			Optional<TableResaDto> optTableResa= this.tableResaService.findById(id);

		if (optTableResa.isPresent()) {

			return ResponseEntity.ok().body(optTableResa.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucune table ne porte ce nom");
		}

	}

	@PostMapping("/tables")
	ResponseEntity<?> ajouter(@Valid @RequestBody TableResaDto tableDto) throws URISyntaxException {
		if(tableDto.getId()==null) {
			
			logger.info("user souhaite une création d'une table" + tableDto.toString());
			Integer result = tableResaService.ajouter(tableDto);
			if(result>-1) {
				return ResponseEntity.ok().body(tableDto);
			}else {
				logger.warn("Cette table avec cet identifiant "+tableDto.getNom()+" existe déja");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("cette table "+tableDto.getNom()+ " existe deja");
			}

		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);

		}
	}
	
	
	
	@Transactional
	@GetMapping("/tables/type/{active}/{noActive}/{qteMin}/{sorting}/{resa}/{noResa}")
	public ResponseEntity<?> AfficherBiereParType( 
			
			@PathVariable Boolean active,
			@PathVariable Boolean noActive,
			@PathVariable Integer qteMin,
			@PathVariable String sorting,
			@PathVariable Boolean resa,
			@PathVariable Boolean noResa) {
		
		
		if(qteMin==null) {
			qteMin=0;
		}
		
		
		if(!active&&!noActive) {
			active=null;
		}
		else if(active&&!noActive) {
			active=true;
		}
		else if(!active&&noActive) {
			active=false;
		}
		
		if(!resa&&!noResa) {
			resa=null;
		}
		else if(resa&&!noResa) {
			resa=true;
		}
		else if(!resa&&noResa) {
			resa=false;
		}
		
		List<TableResaDto> tableOptional=null;
		
	
			if(sorting.equals("rien")) {
				
					tableOptional=this.tableResaService.listerParTypeDeTable( qteMin,active,resa);
				
			}else {
				
					tableOptional=this.tableResaService.listerParTypeDeTableOrder( qteMin,active,resa, sorting);
							
				}
				
			return ResponseEntity.ok().body(tableOptional); 
		

	}
	
	@PutMapping("/tables/{idUser}/{idTable}/{disponible}/{heure}")
	ResponseEntity<Boolean> mettreAJourReservation(
			@Valid 
			@PathVariable Integer disponible,
			@PathVariable Integer idTable,
			@PathVariable Integer idUser,
			@PathVariable Integer heure,
			@RequestBody String reserva
			) throws URISyntaxException {
		boolean reserv=false;
		if(disponible!=null&&idTable!=null) {
			Optional<UtilisateurDto>utilOp=this.utilisateurService.findById(idUser);
			if(utilOp.isPresent()) {
				if(utilOp.get().getReservation()==true &&disponible==1) {
					return ResponseEntity.status(HttpStatus.NOT_FOUND).body(reserv);
				}
				
				Boolean reservation =tableResaService.reservationTable(disponible,idTable,idUser,heure);
				if(reservation) {
					reserv=true;
					return ResponseEntity.ok().body(reserv);	
				}else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND).body(reserv);
				}
				
			}else {
				
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reserv);
				
			}
			}else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reserv);
			}

	}
	
	@PutMapping("/tables/{idUser}")
	ResponseEntity<?> RemettreAZeroReservation(
			@Valid 
			@PathVariable Integer idUser,
			@RequestBody String reserva
			)  {
		
		Optional<UtilisateurDto>utilOp=this.utilisateurService.findById(idUser);
		if(utilOp.isPresent()) {
			UtilisateurDto util=utilOp.get();
			if(util.getRole().getNom().equalsIgnoreCase("Admin")) {
				this.tableResaService.resetAll();
				return ResponseEntity.ok().body(this.tableResaService.resetAll());
			}else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Vous n'êtes pas un Admin");
			}
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Vous n'êtes pas Connecté");
		}
		
	}
	@PreAuthorize("hasAuthority('Admin')")
	@PutMapping("/tables/active")
	ResponseEntity<?> active(
			@Valid 
			@RequestBody TableResaDto table
			) throws URISyntaxException {
		
				return ResponseEntity.ok().body(this.tableResaService.update(table));
		
		
				
		
	}
	
	@GetMapping("/tables/utilisateur/{idUser}")
	ResponseEntity<?> tableParUtilisateur(
			@Valid 
			@PathVariable Integer idUser){
		Optional<UtilisateurDto>optUser=this.utilisateurService.findById(idUser);
		if(optUser.isPresent()) {
			Optional<TableResaDto>optTable=this.tableResaService.tableParUtilisateur(idUser);
			if(optTable.isPresent()) {
				return ResponseEntity.ok().body(optTable.get());
			}else {
				ReponseDto reponse=new ReponseDto(ReponseStatut.OK,"aucune reservation pour cette utilisateur");
				
				return ResponseEntity.ok().body(reponse);
			}
			
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("aucun utilisateur trouvé");
		}
				
		
	}
		
	

	@DeleteMapping("/tables/{table}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable Integer table) {
		Boolean delete=false;
			delete=tableResaService.supprimerId(table);
			if(delete) {
				return ResponseEntity.ok().body(delete);
				
			}else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(delete);
			}
		
			
			
		}
		
		
	
}



