package com.afpa.labonnebiere.controller;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommandeDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.entity.Commande;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.ICommandeService;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;

@RestController
public class ControllerCommandes {
	
	@Autowired
	IBiereService biereService;

	@Autowired
	IUtilisateurService utilisateurService;
	@Autowired
	ICommandeService commandeService;

	@Transactional
	@GetMapping("/commandes/{user}")
	public ResponseEntity<?> AfficherCommandes(@PathVariable String user) {

		Optional<UtilisateurDto> optionalUtilisateur = this.utilisateurService.trouverParUsername(user);
		if (optionalUtilisateur.isPresent()) {
			List<CommandeDto> list = commandeService.AfficherCommandesParUtilisateur(optionalUtilisateur.get().getId());
			return ResponseEntity.ok().body(list);

		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}

	}

	@PostMapping("/commandes/{user}")
	public ResponseEntity<?> ajouterBiereCommandes(@RequestBody BiereDto biere, @PathVariable String user) {
		Optional<UtilisateurDto> optionalUtilisateur = this.utilisateurService.trouverParUsername(user);
		if (optionalUtilisateur.isPresent()) {
			UtilisateurDto u = optionalUtilisateur.get();
			this.utilisateurService.ajoutBiere(u, biere);
			return ResponseEntity.ok().body(commandeService.AfficherCommandesParUtilisateur(u.getId()));

		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}

	}
	@Transactional
	@DeleteMapping("/commandes/{user}/{biere}/{quantite}")
	public ResponseEntity<?> supprimerBierePanier(@PathVariable String user, @PathVariable String biere,
			@PathVariable int quantite) {
		String cap1 = biere.substring(0, 1).toUpperCase() + biere.substring(1);
		biere = cap1;

		Optional<UtilisateurDto> optionalUtilisateur = this.utilisateurService.trouverParUsername(user);
		if (optionalUtilisateur.isPresent()) {
			UtilisateurDto u = optionalUtilisateur.get();

			int id = this.utilisateurService.supprimerBiere(u, biere, quantite);
			return ResponseEntity.ok().body(id);

		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}

	}

	@DeleteMapping("/commandes/{id}")
	public ResponseEntity<?> supprimerBiere(@PathVariable int id) {
		Commande c = this.commandeService.chercherUneCommande(id);
		List<BiereDto> list = this.biereService.findAll();
		for (BiereDto biereDto : list) {
			if (biereDto.getNom().equals(c.getNom())) {
				int waka = c.getCommande();
				biereDto.setQuantite(biereDto.getQuantite() + waka);
				this.biereService.ajouter(biereDto);
				this.commandeService.supprimerId(id);

				return ResponseEntity.ok().body(id);

			}

		}
		return null;

	}

}
