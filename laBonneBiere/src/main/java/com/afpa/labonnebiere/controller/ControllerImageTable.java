package com.afpa.labonnebiere.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.afpa.labonnebiere.dto.ImageBiereDto;
import com.afpa.labonnebiere.dto.ImageTableDto;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.IImageService;
import com.afpa.labonnebiere.service.interf.ITableResaService;

@RestController
public class ControllerImageTable {
	int page;
	
	@Value("${pathUploadTable.photo.path}")
	private String pathTable;
	
	private final static Logger logger = Logger.getLogger(ControllerImageTable.class);
	@Autowired
	IImageService imageService;
	
	
	@Autowired
	ITableResaService tableService;
	
	


//	@GetMapping(value = "/images")
//	public List<BiereDto> listBiere() {
//		return this.biereService.chercherToutesLesBieres(page);	
//	}
//
	
		@GetMapping("/imageTable/{tableId}/{nomimage}")
		public ResponseEntity<?> AfficherImageTable(
				@PathVariable String nomimage,
				@PathVariable int tableId
				) {
			Optional<ImageTableDto>image=this.tableService.afficherImage(tableId,nomimage);
			if(image.isPresent()) {
				ImageTableDto img=image.get();
				
				File file2 =new File(pathTable+img.getNom());
				byte[]b=new byte[(int) file2.length()];
				try  {
					FileInputStream fis = new FileInputStream(file2);
					fis.read(b);
//					int content;
//					while ((content = fis.read()) != -1) {
//						// convert to char and display it
//					}
					fis.close();
					return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(b);

				} catch (IOException e) {
					return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun fichier trouvé");
				}
			
			}else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun fichier trouvédfgfgd");
			}
	}


	
	@Transactional
	@PreAuthorize("hasAuthority('Admin')")
	@PostMapping("/imageTable/{nomTable}")
	ResponseEntity<?> ajouterTable(@Valid
			@RequestBody MultipartFile[] file, 
			@PathVariable String nomTable) throws URISyntaxException {
		 nomTable = nomTable.substring(0, 1).toUpperCase() + nomTable.substring(1);
	
		if (file == null) {
			logger.error("aucun fichier envoyé");
			throw new RuntimeException("You must select the a file for uploading");
		}
//		try {
//			InputStream inputStream = file[0].getInputStream();
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String originalName = file[0].getOriginalFilename();
		//String name = file.getName();
		String contentType = file[0].getContentType();
		long size = file[0].getSize();
		
		
		File file2 =new File(pathTable+nomTable+".jpeg");
		
	try {
		ImageTableDto i=new ImageTableDto();
		i.setNom(nomTable+".jpeg");
		
		
		file[0].transferTo(file2);
		
		this.imageService.ajouterTable(i, nomTable);
		
	} catch (IllegalStateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		// Do processing with uploaded file data in Service layer
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}
	
	@Transactional
	@PreAuthorize("hasAuthority('Admin')")
	@PutMapping("/imageTable/{nomBiere}")
	ResponseEntity<?> edit(@Valid
			@RequestBody MultipartFile[] file, 
			@PathVariable String nomBiere) throws URISyntaxException {
	
		if (file == null) {
			logger.error("aucun fichier envoyé");
			throw new RuntimeException("You must select the a file for uploading");
		}
//		try {
//			InputStream inputStream = file[0].getInputStream();
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String originalName = file[0].getOriginalFilename();
		//String name = file.getName();
		String contentType = file[0].getContentType();
		long size = file[0].getSize();
		File file2 =new File(pathTable+nomBiere+".jpeg");
		
		
	try {
//		ImageTableDto i=new ImageTableDto();
//		i.setNom(originalName);
		file[0].transferTo(file2);
		//this.imageService.edit(i, nomBiere);
		
	} catch (IllegalStateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		// Do processing with uploaded file data in Service layer
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}
}

