package com.afpa.labonnebiere.controller;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.RoleDto;
import com.afpa.labonnebiere.service.interf.IRoleService;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;



@RestController
public class ControllerRole {
	int page;

	@Autowired
	IRoleService roleService;
	
	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping(value = "/roles")
	public List<RoleDto> listRole() {
		return this.roleService.chercherTousLesTypesDeRoles(page);	
	}
	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping("/roles/{role}")
	public ResponseEntity<?> AfficherRole(@PathVariable String role) {
		Optional<RoleDto> roleOptional=null;
		String regex = "[0-9]+";
		if(role.matches(regex)) {
			int id=Integer.parseInt(role);
			roleOptional = this.roleService.findById(id);
		}else {
			roleOptional=this.roleService.trouverParnom(role);

		}
		if (roleOptional.isPresent()) {

			return ResponseEntity.ok().body(roleOptional.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun role a cet identifiant");
		}

	}
	
	
	
	@PostMapping("/roles")
	ResponseEntity<Boolean> ajouter(@Valid @RequestBody String roleDto) throws URISyntaxException {
		
			Boolean result = roleService.ajouter(roleDto);
			return ResponseEntity.ok().body(result);
			
    }
//	
//	@PutMapping("/roles")
//	ResponseEntity<Boolean> mettreAJourRole(
//			@Valid 
//			@RequestBody RoleDto prod
//			) throws URISyntaxException {
//		if(prod.getId()!=null) {
//			Integer result = roleService.Maj(prod);
//			if(result!=0) {
//				
//				return ResponseEntity.ok().body(true);	
//			}else {
//				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
//			}
//			
//		}else {
//			
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
//			
//		}
//
//	}
//	
	@DeleteMapping("/roles/{role}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable Integer role) {
		Boolean bool=false;

			bool=roleService.supprimerId(role);
		if(bool==true) {
			return ResponseEntity.ok().body(bool);
			
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}
		
	}
	
	

}

