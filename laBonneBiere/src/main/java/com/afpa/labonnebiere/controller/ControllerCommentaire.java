package com.afpa.labonnebiere.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.CommentaireDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.ICommentaireService;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;


@RestController
public class ControllerCommentaire {
	int page;

	@Autowired
	ICommentaireService commentaireService;
	@Autowired
	IBiereService biereService;
	@Autowired
	IUtilisateurService utilisateurService;


	@GetMapping(value = "/commentaires")
	public List<CommentaireDto> listCommentaire() {
		return this.commentaireService.chercherTousLesCommentaires(page);	
	}
	@Transactional
	@GetMapping("/commentairesByBiere/{idt}")
	public ResponseEntity<?> AfficherCommentaire(@PathVariable Integer idt) {
	Integer id=-1;
	Optional<BiereDto> b=biereService.findById(idt);
	if(b.isPresent()) {
		BiereDto biere=b.get();
		id=biere.getId();
		List<CommentaireDto> commentaireOptional = this.commentaireService.findByIdBiere(id);
			return ResponseEntity.ok().body(commentaireOptional); 
		
	}else {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("cette biere "+idt+" est inconnue");
	}

	}
	@Transactional
	@GetMapping("/commentairesByUser/{nom}")
	public ResponseEntity<?> AfficherCommentaireByUser(@PathVariable String nom) {
	Integer id=-1;
	Optional<UtilisateurDto> u=utilisateurService.trouverParUsername(nom);
	if(u.isPresent()) {
		UtilisateurDto util=u.get();
		
		List<CommentaireDto> commentaireOptional = this.commentaireService.trouverParNom(util.getUsername());
		if (commentaireOptional.size()!=0) {
			
			return ResponseEntity.ok().body(commentaireOptional); 
		}else {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun commentaire pour "+nom);
		}
	}else {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("username "+nom+" inconnu");
	}

	}
	
	
	@PostMapping("/commentaires/{id}")
	ResponseEntity<Integer> ajouter(@Valid 
			@RequestBody CommentaireDto commentaireDto,
			@PathVariable int id) throws URISyntaxException {
		Optional<BiereDto> test = this.biereService.findById(id);
		if(test.isPresent()) {
			BiereDto testget = test.get();
			if(commentaireDto.getId()==null) {
				commentaireDto.setBiere(testget);
				Integer result = commentaireService.ajouter(commentaireDto, id);
				return ResponseEntity.created(new URI("/commentaire/" + result))
						.body(result);
				
			}else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
			}		
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(0);
		}
    }
	
	@PutMapping("/commentaires/{id}")
	ResponseEntity<Boolean> mettreAJourCommentaire(
			@Valid 
			@RequestBody CommentaireDto prod
			) throws URISyntaxException {
		if(prod.getId()!=null) {
			Integer result = commentaireService.Maj(prod);
			if(result!=-1) {
				
				return ResponseEntity.ok().body(true);	
			}else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(false);
			}
			
		}else {
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
			
		}

	}
	
	@DeleteMapping("/commentaires/{id}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable Integer id) {
		Boolean bool=commentaireService.supprimerId(id);
		if(bool==true) {
			
			return ResponseEntity.ok().body(bool);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}	
	}
}

