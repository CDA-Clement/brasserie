package com.afpa.labonnebiere.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.labonnebiere.constant.AdminUserDefaultConf;
import com.afpa.labonnebiere.dto.BiereDto;
import com.afpa.labonnebiere.dto.ReponseDto;
import com.afpa.labonnebiere.dto.RoleDto;
import com.afpa.labonnebiere.dto.UtilisateurDto;
import com.afpa.labonnebiere.service.interf.IRoleService;
import com.afpa.labonnebiere.service.interf.IUtilisateurService;


@RestController
public class ControllerUtilisateur {

	@Autowired
	IUtilisateurService utilisateurService;

	@Autowired
	IRoleService roleService;

	@Autowired
	private AdminUserDefaultConf adminUserDefaultConf;
	
	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping(value = "/utilisateurs/All/{page}")
	public ResponseEntity<?> listUtilisateur(@PathVariable int page) {
		return ResponseEntity.ok().body(this.utilisateurService.chercherTousLesUtilisateurs(page));
	}
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@GetMapping("/utilisateurs/{user}")
	public ResponseEntity<?> AfficherUtilisateur(@PathVariable String user) {
		Optional<UtilisateurDto> utilisateurOptional=null;
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);
			utilisateurOptional = this.utilisateurService.findById(id);
		}else {	
			utilisateurOptional = this.utilisateurService.trouverParUsername(user);
		}
		if (utilisateurOptional.isPresent()) {
			return ResponseEntity.ok().body(utilisateurOptional.get()); 
		}else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
		}
	}
//	@PreAuthorize("hasAuthority('Admin')")
//	@GetMapping("/utilisateurs/role/{user}")
//	public ResponseEntity<?> AfficherUtilisateurParRole(@PathVariable String user) {
//		List<UtilisateurDto> utilisateurOptional=null;
//		String regex = "[0-9]+";
//		if(user.matches(regex)) {
//			int id=Integer.parseInt(user);
//			Optional<RoleDto>optionalRole=this.roleService.findById(id);
//			if(optionalRole.isPresent()) {
//				utilisateurOptional=this.utilisateurService.listerParRole(id);
//				return ResponseEntity.ok().body(utilisateurOptional); 
//			}
//
//		}else {	
//			Optional<RoleDto>optionalRole = this.roleService.trouverParnom(user);
//			if(optionalRole.isPresent()) {
//				utilisateurOptional=this.utilisateurService.listerParRole(optionalRole.get().getId());
//				return ResponseEntity.ok().body(utilisateurOptional);
//			}
//		}
//
//		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun utilisateur a cet identifiant");
//	}
	
	@PreAuthorize("hasAuthority('Admin')")
	@GetMapping("/utilisateurs/{page}/{active}/{noActive}/{role}/{sorting}")
	public ResponseEntity<?> AfficherUtilisateurParType(
			@PathVariable int page,
			@PathVariable Boolean active,
			@PathVariable Boolean noActive,
			@PathVariable int role,
			@PathVariable String sorting
			) {
		
		if(!active&&!noActive) {
			active=null;
		}
		else if(active&&!noActive) {
			active=true;
		}
		else if(!active&&noActive) {
			active=false;
		}
		if(active==null&&role==0) {
			return ResponseEntity.ok().body(this.utilisateurService.chercherTousLesUtilisateurs(page));
		}else {
			
			
			return ResponseEntity.ok().body(this.utilisateurService.listerByFilter(page, active, role, sorting)); 
		}
		
	}
	
	@GetMapping("/utilisateurs/recherche/{user}")
	public ResponseEntity<?> AfficherBiere(@PathVariable String user) {

		List<UtilisateurDto> ListbiereOptional= new ArrayList<UtilisateurDto>();
		
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);

			ListbiereOptional.add(this.utilisateurService.findById(id).get());
		}else {

			ListbiereOptional=this.utilisateurService.listerParNom(user);
		}


		if (ListbiereOptional.size()>0) {

			return ResponseEntity.ok().body(ListbiereOptional); 
		}else {
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ListbiereOptional);
		}

	}
	
	
	


	@PreAuthorize("hasAuthority('Admin')")
	@PostMapping("/utilisateurs")
	ResponseEntity<?> ajouter(@Valid @RequestBody UtilisateurDto utilisateurDto) throws URISyntaxException {
		ReponseDto rep=new ReponseDto();
			String result = utilisateurService.ajouter(utilisateurDto);
			rep.setMsg(result);
				return ResponseEntity.ok(rep);

		
	}
	@PostMapping("/utilisateursFree")
	ResponseEntity<?> ajouterFree(@Valid @RequestBody UtilisateurDto utilisateurDto) throws URISyntaxException {
		ReponseDto rep=new ReponseDto();
			String result = utilisateurService.ajouter(utilisateurDto);
			rep.setMsg(result);
			
				return ResponseEntity.ok().body(rep);
			 
		}
		
	
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@PutMapping("/utilisateurs")
	ResponseEntity<Object> mettreAJourUtilisateur(
			@Valid 
			@RequestBody UtilisateurDto prod
			) throws URISyntaxException {
		String pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})";
		if(!prod.getAdresseMail().matches(pattern)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("le format de votre adresse mail n'est pas correcte");
		}
		if(prod.getId()!=null) {
			if(prod.getActif() &&!prod.getActivated()) {
				prod.setActivated(true);
				prod.setHeureActivation(new Date().getTime());
			}
			Integer result = utilisateurService.Maj(prod);
			if(result!=-1) {
				return ResponseEntity.ok().body(this.utilisateurService.findById(prod.getId()).get());	
			}else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(this.utilisateurService.findById(prod.getId()).get());
			}

		}else {

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(this.utilisateurService.findById(prod.getId()).get());

		}

	}
	@PreAuthorize("hasAnyAuthority('User','Admin')")
	@DeleteMapping("/utilisateurs/{user}")
	public ResponseEntity<Boolean> deleteGroup(@PathVariable String user) {
		Boolean bool=false;
		String regex = "[0-9]+";
		if(user.matches(regex)) {
			int id=Integer.parseInt(user);
			bool=utilisateurService.supprimerId(id);

		}else {
			Optional<UtilisateurDto> utilisateurOptional = this.utilisateurService.trouverParUsername(user);
			if (utilisateurOptional.isPresent()) {
				UtilisateurDto u=utilisateurOptional.get();
				bool=utilisateurService.supprimerId(u.getId());

			}
		}
		if(bool) {

			return ResponseEntity.ok().body(bool);
		}else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bool);
		}
		
	}



}

