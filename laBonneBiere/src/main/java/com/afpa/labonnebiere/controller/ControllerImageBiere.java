package com.afpa.labonnebiere.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.afpa.labonnebiere.dto.ImageBiereDto;
import com.afpa.labonnebiere.dto.ImageTableDto;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.IImageService;
import com.afpa.labonnebiere.service.interf.ITableResaService;

@RestController
public class ControllerImageBiere {
	int page;
	@Value("${pathUpload.photo.path}")
	private String pathBiere;
	
	@Value("${pathUploadTable.photo.path}")
	private String pathTable;
	
	private final static Logger logger = Logger.getLogger(ControllerImageBiere.class);
	@Autowired
	IImageService imageService;
	@Autowired
	IBiereService biereService;
	
	@Autowired
	ITableResaService tableService;
	
	


//	@GetMapping(value = "/images")
//	public List<BiereDto> listBiere() {
//		return this.biereService.chercherToutesLesBieres(page);	
//	}
//
	@GetMapping("/image/{idBiere}/{nomimage}")
	public ResponseEntity<?> AfficherImageBiere(
			@PathVariable int idBiere,
			@PathVariable String nomimage
			) {
		Optional<ImageBiereDto>image=this.biereService.afficherImage(idBiere,nomimage);
		if(image.isPresent()) {
			ImageBiereDto img=image.get();
			
			File file2 =new File(pathBiere+img.getNom());
			byte[]b=new byte[(int) file2.length()];
			try  {
				FileInputStream fis = new FileInputStream(file2);
				fis.read(b);
//				int content;
//				while ((content = fis.read()) != -1) {
//					// convert to char and display it
//				}
				fis.close();
				return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(b);

			} catch (IOException e) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun fichier trouvé");
			}
		
		}else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("aucun fichier trouvédfgfgd");
		}
	}
		


	@Transactional
	@PreAuthorize("hasAuthority('Admin')")
	@PostMapping("/image/{nomBiere}")
	ResponseEntity<?> ajouter(@Valid
			@RequestBody MultipartFile[] file, 
			@PathVariable String nomBiere) throws URISyntaxException {
	
		if (file == null) {
			logger.error("aucun fichier envoyé");
			throw new RuntimeException("You must select the a file for uploading");
		}
//		try {
//			InputStream inputStream = file[0].getInputStream();
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String originalName = file[0].getOriginalFilename();
		//String name = file.getName();
		String contentType = file[0].getContentType();
		long size = file[0].getSize();
		String originalName1 = file[1].getOriginalFilename();
		//String name = file.getName();
		String contentType1 = file[1].getContentType();
		long size1 = file[1].getSize();
		
		File file2 =new File(pathBiere+originalName.toLowerCase());
		File file3 =new File(pathBiere+originalName1.toLowerCase());
		
	try {
		ImageBiereDto i=new ImageBiereDto();
		i.setNom(originalName);
		ImageBiereDto i2=new ImageBiereDto();
		i2.setNom(originalName1);
		file[0].transferTo(file2);
		file[1].transferTo(file3);
		this.imageService.ajouter(i,i2, nomBiere);
		
	} catch (IllegalStateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		// Do processing with uploaded file data in Service layer
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}
	
	
	@Transactional
	@PreAuthorize("hasAuthority('Admin')")
	@PutMapping("/image/{nomBiere}")
	ResponseEntity<?> edit(@Valid
			@RequestBody MultipartFile[] file, 
			@PathVariable String nomBiere) throws URISyntaxException {
	
		if (file == null) {
			logger.error("aucun fichier envoyé");
			throw new RuntimeException("You must select the a file for uploading");
		}
//		try {
//			InputStream inputStream = file[0].getInputStream();
//			
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String originalName = file[0].getOriginalFilename();
		//String name = file.getName();
		String contentType = file[0].getContentType();
		long size = file[0].getSize();
		File file2 =new File(pathBiere+originalName.toLowerCase());
		
		
	try {
		ImageBiereDto i=new ImageBiereDto();
		i.setNom(originalName);
		file[0].transferTo(file2);
		//this.imageService.edit(i, nomBiere);
		
	} catch (IllegalStateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		// Do processing with uploaded file data in Service layer
		return new ResponseEntity<String>(originalName, HttpStatus.OK);
	}
}

