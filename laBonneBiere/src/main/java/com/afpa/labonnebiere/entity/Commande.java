package com.afpa.labonnebiere.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@DynamicInsert
@DynamicUpdate
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMANDE_SEQ")
	private Integer id;
	private String nom;
	private Double total;
	private Integer idBiere;
	private Integer commande;
	private double prixUnitaire;
	
	@ManyToOne
	@JoinColumn(name = "utilisateur")
	private Utilisateur utilisateur;
	
	
	
	
}
