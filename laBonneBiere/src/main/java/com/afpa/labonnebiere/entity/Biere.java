package com.afpa.labonnebiere.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@DynamicInsert
@DynamicUpdate
public class Biere {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BIERE_SEQ")
	private Integer id;
	
	@Column(unique=true)
	private String nom;
	private String description;
	private Double degre;
	private Double prix;
	private Integer quantite;
	private Integer commande;
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "typedebiere")
	private TypeDeBiere typeDeBiere;

	@ManyToMany
	private List<HistoriqueDAchat> historique;

	@ManyToMany
	private List<Panier> panier;
	
	@OneToMany
	@JoinColumn(name = "biere")
	private List<ImageBiere> imageBiere;
}
