package com.afpa.labonnebiere.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@DynamicInsert
@DynamicUpdate
public class Panier {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PANIER_SEQ")
	private Integer id;
	private String nom;
	private Double totalPanier;
	@OneToMany
	private List<Commande> listDeCommande;
}
