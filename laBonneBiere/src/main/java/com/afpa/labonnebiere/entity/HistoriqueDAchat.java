package com.afpa.labonnebiere.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@DynamicInsert
@DynamicUpdate
public class HistoriqueDAchat {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HISTORIQUEDACHAT_SEQ")
	private Integer id;
	private Date dateAchat;
	private Double prixTotal;
	private Integer idUser;
	private String biere;
	private Integer idBiere;
	private String typeDeBiere;
	@ManyToMany
	private List<Biere> listDeBiere;
}
