package com.afpa.labonnebiere.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
@DynamicInsert
@DynamicUpdate
public class TableResa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TABLE_SEQ")
	private Integer id;
	
	private String nom;
	
	private Boolean reservationEnCours;
	
	private String description;
	
	private Integer capacite;
	
	private Boolean active;
	
	private Integer nbResa;
	
	private double heure;
	
	private Integer utilisateur;
	
	@OneToOne
	@JoinColumn
	private ImageTable imageTable;

}
