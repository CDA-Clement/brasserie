package com.afpa.labonnebiere.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@DynamicInsert
@DynamicUpdate
public class Commentaire {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMENTAIRE_SEQ")
	private Integer id;
	
	private String nom;
	
	private String message;
	
	private Date date;
	
	@ManyToOne
	@JoinColumn(name = "biere")
	private Biere biere;
}
