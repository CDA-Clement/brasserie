package com.afpa.labonnebiere;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.afpa.labonnebiere.constant.AdminUserDefaultConf;
import com.afpa.labonnebiere.dao.BiereRepository;
import com.afpa.labonnebiere.dao.CommentaireRepository;
import com.afpa.labonnebiere.dao.HistoriquedAchatRepository;
import com.afpa.labonnebiere.dao.ImageBiereRepository;
import com.afpa.labonnebiere.dao.ImageTableRepository;
import com.afpa.labonnebiere.dao.PanierRepository;
import com.afpa.labonnebiere.dao.RoleRepository;
import com.afpa.labonnebiere.dao.TableResaRepository;
import com.afpa.labonnebiere.dao.TypeDeBiereRepository;
import com.afpa.labonnebiere.dao.UtilisateurRepository;
import com.afpa.labonnebiere.entity.Biere;
import com.afpa.labonnebiere.entity.Commentaire;
import com.afpa.labonnebiere.entity.ImageBiere;
import com.afpa.labonnebiere.entity.ImageTable;
import com.afpa.labonnebiere.entity.Panier;
import com.afpa.labonnebiere.entity.Role;
import com.afpa.labonnebiere.entity.TableResa;
import com.afpa.labonnebiere.entity.TypeDeBiere;
import com.afpa.labonnebiere.entity.Utilisateur;
import com.afpa.labonnebiere.service.interf.IBiereService;
import com.afpa.labonnebiere.service.interf.ICleanService;
import com.afpa.labonnebiere.service.interf.ICommandeService;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableAsync
@EnableSwagger2
@SpringBootApplication
public class DemoApplication implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		
	}
	@Value("${pathUpload.photo.path}")
	private String path;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.afpa.labonnebiere.controller"))
				.build();
	}


	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private ICleanService cleanService;
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedMethods("GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS");
	}
	@Bean
	public CommandLineRunner init(TableResaRepository tableResaRepository,
			RoleRepository roleRepository,
			UtilisateurRepository utilisateurRepository,
			PanierRepository panierRepository,
			HistoriquedAchatRepository historiqueDAchat,
			AdminUserDefaultConf adminUserConf, 
			TypeDeBiereRepository typeDeBiereRepository,
			BiereRepository biereRepository,
			CommentaireRepository commentaireRepository,
			ImageBiereRepository imageBiereRepository,
			ImageTableRepository imageTableRepository) {
		return (String... args)->{
			this.cleanService.CleanDB();
			List<TypeDeBiere> type=(List<TypeDeBiere>) typeDeBiereRepository.findAll();

			if(type.size()==0) {
				File repo =new File(path);
				if(!repo.exists()) {
					boolean test = repo.mkdir();

				}

				imageTableRepository.save(new ImageTable(1,"Seigneur.jpeg"));
				imageTableRepository.save(new ImageTable(2,"Duchesse.jpeg"));
				imageTableRepository.save(new ImageTable(3,"Président.jpeg"));
				imageTableRepository.save(new ImageTable(4,"La Royale.jpeg"));
				imageTableRepository.save(new ImageTable(5,"Principale.jpeg"));
				imageTableRepository.save(new ImageTable(6,"La Double.jpeg"));
				imageTableRepository.save(new ImageTable(7,"Romance.jpeg"));

				Optional<ImageTable> seigneur=imageTableRepository.findById(1);
				Optional<ImageTable> duchesse=imageTableRepository.findById(2);
				Optional<ImageTable> president=imageTableRepository.findById(3);
				Optional<ImageTable> royale=imageTableRepository.findById(4);
				Optional<ImageTable> principale=imageTableRepository.findById(5);
				Optional<ImageTable> laDouble=imageTableRepository.findById(6);
				Optional<ImageTable> romance=imageTableRepository.findById(7);


				tableResaRepository.save(new TableResa(1,"Seigneur",false,"Pour les soirées entre amis",5,true,0,20.00, 0,seigneur.get()));
				tableResaRepository.save(new TableResa(2,"Duchesse",false,"En petit comité",3,true,0,20.00,null,duchesse.get()));
				tableResaRepository.save(new TableResa(3,"Président",false,"La traditionnelle 4 personnes",4,true,0,20.00,0,president.get()));
				tableResaRepository.save(new TableResa(4,"La Royale",false,"Parfait pour un tête à tête",2,true,0,20.00,0,royale.get()));
				tableResaRepository.save(new TableResa(5,"Principale",false,"Pour les grandes réunions",7,true,0,20.00,0,principale.get()));
				tableResaRepository.save(new TableResa(6,"La Double",false,"Un petit coin de tranquilité entre amis",5,true,0,20.00,0,laDouble.get()));
				tableResaRepository.save(new TableResa(7,"Romance",false,"Pour une soirée romantique",2,true,0,20.00,0,romance.get()));
				typeDeBiereRepository.save(new TypeDeBiere(1,"Tampon"));
				typeDeBiereRepository.save(new TypeDeBiere(2,"Blonde"));
				typeDeBiereRepository.save(new TypeDeBiere(3,"Brune"));
				typeDeBiereRepository.save(new TypeDeBiere(4,"Triple"));
				typeDeBiereRepository.save(new TypeDeBiere(5,"Ambrée"));
				typeDeBiereRepository.save(new TypeDeBiere(6,"Ipa"));
				typeDeBiereRepository.save(new TypeDeBiere(7,"Stout"));
				typeDeBiereRepository.save(new TypeDeBiere(8,"Trappist"));
				typeDeBiereRepository.save(new TypeDeBiere(9,"Lager"));
				typeDeBiereRepository.save(new TypeDeBiere(10,"Pills"));
				typeDeBiereRepository.save(new TypeDeBiere(11,"Fruitées"));
				panierRepository.save(new Panier());
//				historiqueDAchat.save(new HistoriqueDAchat());
				imageBiereRepository.save(new ImageBiere(1,"Leffe-blonde.jpeg"));
				imageBiereRepository.save(new ImageBiere(2,"Kasteel-Rouge.jpeg"));
				imageBiereRepository.save(new ImageBiere(3,"Karmeliet.jpeg"));
				imageBiereRepository.save(new ImageBiere(4,"Guinness.jpeg"));
				imageBiereRepository.save(new ImageBiere(5,"Chimay.jpeg"));
				imageBiereRepository.save(new ImageBiere(6,"Logo-Leffe.jpeg"));
				imageBiereRepository.save(new ImageBiere(7,"Logo-Kasteel.jpeg"));
				imageBiereRepository.save(new ImageBiere(8,"Logo-Karmeliet.jpeg"));
				imageBiereRepository.save(new ImageBiere(9,"Logo-Guinness.jpeg"));
				imageBiereRepository.save(new ImageBiere(10,"Logo-Chimay.jpeg"));

				List <ImageBiere>leffe=new ArrayList<ImageBiere>();
				leffe.add(imageBiereRepository.findById(1).get());
				leffe.add(imageBiereRepository.findById(6).get());

				List <ImageBiere>kasteel=new ArrayList<ImageBiere>();
				kasteel.add(imageBiereRepository.findById(2).get());
				kasteel.add(imageBiereRepository.findById(7).get());

				List <ImageBiere>karmeliet=new ArrayList<ImageBiere>();
				karmeliet.add(imageBiereRepository.findById(3).get());
				karmeliet.add(imageBiereRepository.findById(8).get());

				List <ImageBiere>guinness=new ArrayList<ImageBiere>();
				guinness.add(imageBiereRepository.findById(4).get());
				guinness.add(imageBiereRepository.findById(9).get());

				List <ImageBiere>chimay=new ArrayList<ImageBiere>();
				chimay.add(imageBiereRepository.findById(5).get());
				chimay.add(imageBiereRepository.findById(10).get());

				biereRepository.save(new Biere(1, "Leffe", "leffe toi", 5.0, 2.0, 50000,0,true, typeDeBiereRepository.findById(2).get(), null, null, leffe));
				biereRepository.save(new Biere(2, "Kasteel Rouge", "La bière du chateau", 7.0, 2.5, 10,0, true, typeDeBiereRepository.findById(11).get(), null, null,kasteel));
				biereRepository.save(new Biere(3, "Karmeliet", "Karmeliet", 8.5, 2.5, 50000,0, true, typeDeBiereRepository.findById(4).get(), null, null,karmeliet));
				biereRepository.save(new Biere(4, "Guinness", "l'irlandaise", 5.0, 3.0, 0,0,true, typeDeBiereRepository.findById(7).get(), null, null,guinness));
				biereRepository.save(new Biere(5, "Chimay", "bière des moines", 8.6, 2.3, 50000,0,true, typeDeBiereRepository.findById(8).get(), null, null, chimay));
				commentaireRepository.save(new Commentaire(1, "Jean-Philippe", "C'est une bonne bière.", new Date(), biereRepository.findById(1).get()));
				commentaireRepository.save(new Commentaire(2, "Anthony", "Je ne suis pas fan de cette bière.", new Date(), biereRepository.findById(1).get()));
				commentaireRepository.save(new Commentaire(3, "Clement", "J'aime pas.", new Date(), biereRepository.findById(1).get()));

			}




			// ne pas oublier de bloquer la création d utilisateur avec le nom ou prenom admin
			Optional<Utilisateur> adminE = utilisateurRepository.findByUsername(adminUserConf.getUsername());
			if(! adminE.isPresent()) {
				Panier p=panierRepository.findById(1).get();
				p.setTotalPanier(0.0);
				panierRepository.save(p);
				roleRepository.save(new Role(0, "Tampon"));
				roleRepository.save(new Role(1, "User"));
				roleRepository.save(new Role(2, "Admin"));
				utilisateurRepository.save(Utilisateur.builder()
						.nom(adminUserConf.getNom())
						.prenom(adminUserConf.getPrenom())
						.username(adminUserConf.getUsername())
						.adresseMail("clement.leuridon@gmail.com")
						.dateAge(new Date())
						.reservation(false)
						.actif(true)
						.activated(true)
						.heureActivation(new Date().getTime())
						.panier(panierRepository.findById(1).get())
						.listHistoriqueAchat(null)
						.password(passwordEncoder.encode(adminUserConf.getPassword()))
						.role(roleRepository.findById(2).get())
						.build());
			}
		};
	}
	
	@Bean
	  public Executor taskExecutor() {
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    executor.setCorePoolSize(2);
	    executor.setMaxPoolSize(2);
	    executor.setQueueCapacity(500);
	    executor.setThreadNamePrefix("cleanDataBase");
	    executor.initialize();
	    return executor;
	  }


	
}